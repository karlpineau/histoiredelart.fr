<?php

namespace HOME\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route(
     *     "/",
     *     name="home_home_home_index",
     *     options={
     *          "sitemap"={
     *              "priority"="1",
     *              "changefreq"="monthly"
     *          }
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        //return $this->render('HOMEHomeBundle:Home:index.html.twig');
        return $this->redirectToRoute('cliches_home_home_index');
    }

    /**
     * @Route(
     *     "/mentions-légales",
     *     name="home_home_home_mentionsLegales",
     *     options={
     *          "sitemap"={
     *              "priority"=0.3,
     *              "changefreq"="monthly"
     *          },
     *          "utf8"=true
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mentionsLegalesAction()
    {
        return $this->render('HOMEHomeBundle:Home:mentionsLegales.html.twig');
    }
}

<?php

namespace TB\TestedGameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TB\ModelBundle\Entity\TestedGame;
use TB\ModelBundle\Form\TestedGameAddItemsType;
use TB\ModelBundle\Form\TestedGameEditType;
use TB\ModelBundle\Form\TestedGameIconEditType;
use TB\ModelBundle\Form\TestedGameType;
use TB\ModelBundle\Form\TestedItemEditType;
use Symfony\Component\Routing\Annotation\Route;

class TestedGameController extends Controller
{
    /**
     * @Route(
     *     "/testedGame/create",
     *     name="tb_testedgame_testedgame_create"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $testedGame = new TestedGame();
        $testedGame->setIsRandomized(false);
        $testedGame->setIsPrivate(false);
        if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $testedGame->setIsOnline(false);
            $testedGame->setIsOfficial(true);
        } else {
            $testedGame->setIsOnline(true);
            $testedGame->setIsOfficial(false);
        }
        $testedGame->setCreateUser($this->getUser());

        $form = $this->createForm(TestedGameType::class, $testedGame);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($testedGame->getTestedItems() as $testedItem) {
                $testedItem->setTestedGame($testedGame);
                $em->persist($testedItem);
            }
            $em->persist($testedGame);
            $em->flush();

            if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                $this->get('session')->getFlashBag()->add('notice', 'Félicitations, votre partie a bien été éditée.' );
                return $this->redirect($this->generateUrl('tb_testedgame_testedgame_view', array('testedGame_id' => $testedGame->getId())));
            } else {
                return $this->redirect($this->generateUrl('tb_testedgame_testedgame_create_end', array('testedGame_id' => $testedGame->getId())));
            }
        }
        return $this->render('TBTestedGameBundle:TestedGame:Create/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(
     *     "/profile/testedGame/create/end/{testedGame_id}",
     *     name="tb_testedgame_testedgame_create_end",
     *     requirements={
     *          "testedGame_id"="\d+"
     *     }
     * )
     * @param $testedGame_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createEndAction($testedGame_id)
    {
        $em = $this->getDoctrine()->getManager();
        $testedGame = $em->getRepository('TBModelBundle:TestedGame')->findOneById($testedGame_id);

        if($testedGame === null OR $testedGame->getCreateUser() != $this->getUser()) {throw $this->createNotFoundException('TestedGame non trouvé :'.$testedGame_id);}

        $urlToPlay = $this->generateUrl('tb_player_player_index', array('testedGame_id' => $testedGame->getId()), true);
        $image = $testedGame->getIcon();
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->addMeta('property', 'og:description', 'Teste tes connaissances en histoire de l\'art sur Clichés! - Viens participer au jeu conçu par '.$testedGame->getCreateUser()->getUsername().' et défis tes amis!')
            ->addMeta('property', 'og:image', $this->get('assets.packages')->getUrl('uploads/gallery/'.$image->getMediumFileName()))
            ->addMeta('property', 'og:url', $urlToPlay)->addMeta('property', 'og:title', $testedGame->getTitle().' - Clichés!')
            ->addMeta('property', 'twitter:title', $testedGame->getTitle().' - Clichés!')
            ->addMeta('property', 'twitter:description', 'Teste tes connaissances en histoire de l\'art sur Clichés! - Viens participer au jeu conçu par '.$testedGame->getCreateUser()->getUsername().' et défis tes amis!')
            ->addMeta('property', 'twitter:image', $this->get('assets.packages')->getUrl('uploads/gallery/'.$image->getMediumFileName()))
            ->addMeta('property', 'twitter:url', $urlToPlay)
            ->addMeta('property', 'twitter:card', 'summary_large_image')
        ;

        if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $seoPage->addMeta('name', 'author', "Histoiredelart.fr");
        } else {
            $seoPage->addMeta('name', 'author', $testedGame->getCreateUser()->getUsername());
        }

        return $this->render('TBTestedGameBundle:TestedGame:Create/end.html.twig', array(
            'testedGame' => $testedGame,
            'urlToPlay' => $urlToPlay
        ));
    }

    /**
     * @Route(
     *     "/testedGame/add-items/{testedGame_id}",
     *     name="tb_testedgame_testedgame_addItems",
     *     requirements={
     *          "testedGame_id"="\d+"
     *     }
     * )
     * @param $testedGame_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addItemsAction($testedGame_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $testedGame = $em->getRepository('TBModelBundle:TestedGame')->findOneById($testedGame_id);
        if($testedGame === null) {throw $this->createNotFoundException('No TestedGame for this id : '.$testedGame_id);}

        $form = $this->createForm(TestedGameAddItemsType::class, $testedGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($testedGame->getTestedItems() as $testedItem) {
                $testedItem->setTestedGame($testedGame);
                $testedItem->setCreateUser($this->getUser());
                $em->persist($testedItem);
            }
            $testedGame->setUpdateUser($this->getUser());
            $em->persist($testedGame);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, les modifications ont bien été enregistrées.' );
            return $this->redirect($this->generateUrl('tb_testedgame_testedgame_view', array('testedGame_id' => $testedGame->getId())));
        }
        return $this->render('TBTestedGameBundle:TestedGame:AddItems/addItems.html.twig', array(
            'testedGame' => $testedGame,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(
     *     "/testedGame/edit-item/{testedGame_id}/{testedItem_id}",
     *     name="tb_testedgame_testedgame_editItem",
     *     requirements={
     *          "testedGame_id"="\d+",
     *          "testedItem_id"="\d+"
     *     }
     * )
     * @param $testedGame_id
     * @param $testedItem_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editItemAction($testedGame_id, $testedItem_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $testedGame = $em->getRepository('TBModelBundle:TestedGame')->findOneById($testedGame_id);
        if($testedGame === null) {throw $this->createNotFoundException('No TestedGame for this id : '.$testedGame_id);}
        $testedItem = $em->getRepository('TBModelBundle:TestedItem')->findOneById($testedItem_id);
        if($testedItem === null) {throw $this->createNotFoundException('No TestedItem for this id : '.$testedItem_id);}
        if($testedItem->getTestedGame() != $testedGame) {throw $this->createNotFoundException('Error! Unmatch TestedGame <> TestedItem');}

        $form = $this->createForm(TestedItemEditType::class, $testedItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $testedItem->setUpdateUser($this->getUser());
            $em->persist($testedItem);
            $testedGame->setUpdateUser($this->getUser());
            $em->persist($testedGame);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, les modifications ont bien été enregistrées.' );
            return $this->redirect($this->generateUrl('tb_testedgame_testedgame_view', array('testedGame_id' => $testedGame->getId())));
        }
        return $this->render('TBTestedGameBundle:TestedGame:EditItem/editItem.html.twig', array(
            'testedGame' => $testedGame,
            'testedItem' => $testedItem,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(
     *     "/testedGame/remove-item/{testedGame_id}/{testedItem_id}",
     *     name="tb_testedgame_testedgame_removeItem",
     *     requirements={
     *          "testedGame_id"="\d+",
     *          "testedItem_id"="\d+"
     *     }
     * )
     * @param $testedGame_id
     * @param $testedItem_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeItemAction($testedGame_id, $testedItem_id)
    {
        $em = $this->getDoctrine()->getManager();
        $testedGame = $em->getRepository('TBModelBundle:TestedGame')->findOneById($testedGame_id);
        if($testedGame === null) {throw $this->createNotFoundException('No TestedGame for this id : '.$testedGame_id);}
        $testedItem = $em->getRepository('TBModelBundle:TestedItem')->findOneById($testedItem_id);
        if($testedItem === null) {throw $this->createNotFoundException('No TestedItem for this id : '.$testedItem_id);}
        if($testedItem->getTestedGame() != $testedGame) {throw $this->createNotFoundException('Error! Unmatch TestedGame <> TestedItem');}

        foreach($em->getRepository('TBModelBundle:TestedItemResult')->findByTestedItem($testedItem) as $testedItemResult) {
            $em->remove($testedItemResult);
        }
        $testedGame->removeTestedItem($testedItem);
        $testedGame->setUpdateUser($this->getUser());
        $em->persist($testedGame);
        $em->remove($testedItem);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'Félicitations, les modifications ont bien été enregistrées.' );
        return $this->redirect($this->generateUrl('tb_testedgame_testedgame_view', array('testedGame_id' => $testedGame->getId())));
    }

    /**
     * @Route(
     *     "/testedGame/edit/{testedGame_id}",
     *     name="tb_testedgame_testedgame_edit",
     *     requirements={
     *          "testedGame_id"="\d+"
     *     }
     * )
     * @param $testedGame_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($testedGame_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $testedGame = $em->getRepository('TBModelBundle:TestedGame')->findOneById($testedGame_id);
        if($testedGame === null) {throw $this->createNotFoundException('No TestedGame for this id : '.$testedGame_id);}

        $form = $this->createForm(TestedGameEditType::class, $testedGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $testedGame->setUpdateUser($this->getUser());
            $em->persist($testedGame);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, votre partie a bien été éditée.' );
            return $this->redirect($this->generateUrl('tb_testedgame_testedgame_view', array('testedGame_id' => $testedGame->getId())));
        }
        return $this->render('TBTestedGameBundle:TestedGame:Edit/edit.html.twig', array(
            'testedGame' => $testedGame,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(
     *     "/testedGame/edit-icon/{testedGame_id}",
     *     name="tb_testedgame_testedgame_editIcon",
     *     requirements={
     *          "testedGame_id"="\d+"
     *     }
     * )
     * @param $testedGame_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editIconAction($testedGame_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $testedGame = $em->getRepository('TBModelBundle:TestedGame')->findOneById($testedGame_id);
        if($testedGame === null) {throw $this->createNotFoundException('No TestedGame for this id : '.$testedGame_id);}

        $form = $this->createForm(TestedGameIconEditType::class, $testedGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $testedGame->setUpdateUser($this->getUser());
            $em->persist($testedGame);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, votre partie a bien été éditée.' );
            return $this->redirect($this->generateUrl('tb_testedgame_testedgame_view', array('testedGame_id' => $testedGame->getId())));
        }
        return $this->render('TBTestedGameBundle:TestedGame:EditIcon/editIcon.html.twig', array(
            'testedGame' => $testedGame,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(
     *     "/testedGame/{testedGame_id}",
     *     name="tb_testedgame_testedgame_view",
     *     requirements={
     *          "testedGame_id"="\d+"
     *     }
     * )
     * @param $testedGame_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($testedGame_id)
    {
        $em = $this->getDoctrine()->getManager();
        $testedGame = $em->getRepository('TBModelBundle:TestedGame')->findOneById($testedGame_id);

        if($testedGame === null) {throw $this->createNotFoundException('TestedGame non trouvé :'.$testedGame_id);}

        return $this->render('TBTestedGameBundle:TestedGame:View/view.html.twig', array(
            'testedGame' => $testedGame,
        ));
    }

    /**
     * @Route(
     *     "/testedGame/updateOnlineStatus/{testedGame_id}",
     *     name="tb_testedgame_testedgame_updateOnlineStatus",
     *     requirements={
     *          "testedGame_id"="\d+"
     *     }
     * )
     * @param $testedGame_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateOnlineStatusAction($testedGame_id)
    {
        $em = $this->getDoctrine()->getManager();
        $testedGame = $em->getRepository('TBModelBundle:TestedGame')->findOneById($testedGame_id);
        if($testedGame === null) {throw $this->createNotFoundException('TestedGame non trouvé :'.$testedGame_id);}

        if($testedGame->getIsOnline() == true) {
            $testedGame->setIsOnline(false);
        } else {
            $testedGame->setIsOnline(true);
        }

        $testedGame->setUpdateUser($this->getUser());

        $em->persist($testedGame);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'Félicitations, le statut de votre partie a bien été modifié.' );
        return $this->redirectToRoute('tb_testedgame_testedgame_view', array('testedGame_id' => $testedGame_id));
    }

    /**
     * @Route(
     *     "/testedGame/updateOfficialStatus/{testedGame_id}",
     *     name="tb_testedgame_testedgame_updateOfficialStatus",
     *     requirements={
     *          "testedGame_id"="\d+"
     *     }
     * )
     * @param $testedGame_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateOfficialStatusAction($testedGame_id)
    {
        $em = $this->getDoctrine()->getManager();
        $testedGame = $em->getRepository('TBModelBundle:TestedGame')->findOneById($testedGame_id);
        if($testedGame === null) {throw $this->createNotFoundException('TestedGame non trouvé :'.$testedGame_id);}

        if($testedGame->getIsOfficial() == true) {
            $testedGame->setIsOfficial(false);
        } else {
            $testedGame->setIsOfficial(true);
        }

        $testedGame->setUpdateUser($this->getUser());

        $em->persist($testedGame);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'Félicitations, le statut de votre partie a bien été modifié.' );
        return $this->redirectToRoute('tb_testedgame_testedgame_view', array('testedGame_id' => $testedGame_id));
    }

    /**
     * @Route(
     *     "/testedGame/remove/{testedGame_id}",
     *     name="tb_testedgame_testedgame_remove",
     *     requirements={
     *          "testedGame_id"="\d+"
     *     }
     * )
     * @param $testedGame_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction($testedGame_id)
    {
        $em = $this->getDoctrine()->getManager();
        $testedGame = $em->getRepository('TBModelBundle:TestedGame')->findOneById($testedGame_id);
        if($testedGame === null) {throw $this->createNotFoundException('TestedGame non trouvé :'.$testedGame_id);}

        $this->get('tb_model.testedgame')->remove($testedGame);

        $this->get('session')->getFlashBag()->add('notice', 'Félicitations, votre partie a bien été supprimée.' );
        if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('tb_administration_home_index');
        } else {
            return $this->redirectToRoute('tb_personalplace_home_index');
        }
    }
}

<?php

namespace TB\ModelBundle\Form;

use DATA\ImageBundle\Form\ImageRegisterType;
use DATA\ImageBundle\Form\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TestedGameType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',          TextType::class,                 array('required' => true))
            ->add('isRandomized',   CheckboxType::class,    array('label'    => 'Ordre aléatoire autorisé (les images seront mélangées)',
                                                                  'required' => false))
            ->add('isPrivate',      CheckboxType::class,    array('label'    => 'Partie privée (non référencée sur le site)',
                                                                  'required' => false))
            ->add('testedItems',    CollectionType::class,  array('entry_type' => TestedItemType::class,
                                                                  'allow_add' => true,
                                                                  'allow_delete' => true))
            ->add('icon',           ImageType::class,  array('required' => true))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TB\ModelBundle\Entity\TestedGame'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tb_modelbundle_testedgame';
    }
}

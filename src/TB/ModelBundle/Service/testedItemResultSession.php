<?php

namespace TB\ModelBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;

class testedItemResultSession
{
    protected $em;

    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------------------------------------------- */
    /* -------------------------------------------- GET                 --------------------------------------------- */
    public function getByTestedSession($testedSession) {
        return $this->em->getRepository('TBModelBundle:TestedItemResultSession')->findBy(array('testedSession' => $testedSession));
    }
    /* -------------------------------------------------------------------------------------------------------------- */
}

<?php

namespace TB\ModelBundle\Service;

use Doctrine\ORM\EntityManager;

class testedGame
{
    protected $em;
    protected $security;

    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------------------------------------------- */
    /* -------------------------------------------- Actions             --------------------------------------------- */
    public function remove($testedGame) {
        foreach($this->em->getRepository('TBModelBundle:TestedItem')->findByTestedGame($testedGame) as $testedItem) {
            foreach($this->em->getRepository('TBModelBundle:TestedItemResult')->findByTestedItem($testedItem) as $testedItemResult) {
                $this->em->remove($testedItemResult);
            }

            $this->em->remove($testedItem);
        }

        foreach($this->em->getRepository('TBModelBundle:TestedSession')->findByTestedGame($testedGame) as $testedSession) {
            $this->em->remove($testedSession);
        }

        $this->em->remove($testedGame);
        $this->em->flush();
    }
    /* -------------------------------------------------------------------------------------------------------------- */
}

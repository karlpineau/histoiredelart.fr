<?php

namespace TB\ModelBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;

class testedSession
{
    protected $em;

    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------------------------------------------- */
    /* -------------------------------------------- GET                 --------------------------------------------- */
    public function getByTestedGame($testedGame) {
        return $this->em->getRepository('TBModelBundle:TestedSession')->findBy(array('testedGame' => $testedGame));
    }
    /* -------------------------------------------------------------------------------------------------------------- */
}

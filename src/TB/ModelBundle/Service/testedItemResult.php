<?php

namespace TB\ModelBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;

class testedItemResult
{
    protected $em;

    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------------------------------------------- */
    /* -------------------------------------------- GET                 --------------------------------------------- */
    public function getByTestedItemResultSession($testedItemResultSession) {
        return $this->em->getRepository('TBModelBundle:TestedItemResult')->findBy(array('testedItemResultSession' => $testedItemResultSession));
    }
    /* -------------------------------------------------------------------------------------------------------------- */
}

<?php

namespace TB\ModelBundle\Service;

use Doctrine\ORM\EntityManager;

class testedItem
{
    protected $em;
    protected $security;

    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------------------------------------------- */
    /* -------------------------------------------- GET                 --------------------------------------------- */
    public function getByTestedGame($testedGame) {
        return $this->em->getRepository('TBModelBundle:TestedItem')->findBy(array('testedGame' => $testedGame));
    }
    /* -------------------------------------------------------------------------------------------------------------- */
}

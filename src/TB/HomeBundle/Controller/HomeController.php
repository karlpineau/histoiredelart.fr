<?php

namespace TB\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route(
     *     "/",
     *     name="tb_home_home_index",
     *     options={
     *          "sitemap"={
     *              "priority"="1",
     *              "changefreq"="weekly"
     *          }
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        /*$em = $this->getDoctrine()->getManager();
        $testedGames = $em->getRepository('TBModelBundle:TestedGame')->findBy(array('isOnline' => true, 'isPrivate' => false), array('createDate' => 'DESC'), 10);

        return $this->render('TBHomeBundle:Home:index.html.twig', array('testedGames' => $testedGames));*/
        return $this->redirectToRoute('cliches_home_home_index');
    }

    /**
     * @Route(
     *     "/pourquoi-s'inscrire",
     *     name="tb_home_home_registrationArg",
     *     options={
     *          "sitemap"={
     *              "priority"="0.6",
     *              "changefreq"="weekly"
     *          }
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registrationArgAction()
    {
        return $this->render('TBHomeBundle:Home:registrationArg.html.twig');
    }

    /**
     * @Route(
     *     "/parties",
     *     name="tb_home_home_getAllTestedGames",
     *     options={
     *          "sitemap"={
     *              "priority"="0.6",
     *              "changefreq"="weekly"
     *          }
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllTestedGamesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $testedGames = $em->getRepository('TBModelBundle:TestedGame')->findBy(array('isOnline' => true, 'isPrivate' => false), array('createDate' => 'DESC'));

        return $this->render('TBHomeBundle:Home:getAllTestedGame.html.twig', array('testedGames' => $testedGames));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllOfficialAction()
    {
        $em = $this->getDoctrine()->getManager();
        $testedGames = $em->getRepository('TBModelBundle:TestedGame')->findBy(array('isOfficial' => true, 'isPrivate' => false, 'isOnline' => true), array('createDate' => 'DESC'));

        return $this->render('TBHomeBundle:Home:getAllTestedGame.html.twig', array('testedGames' => $testedGames));
    }
}

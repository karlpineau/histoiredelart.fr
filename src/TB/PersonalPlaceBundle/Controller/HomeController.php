<?php

namespace TB\PersonalPlaceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route(
     *     "/profile/testedGame",
     *     name="tb_personalplace_home_index"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $testedGames = $em->getRepository('TBModelBundle:TestedGame')->findBy(array('createUser' => $this->getUser()));

        return $this->render('TBPersonalPlaceBundle:Home:index.html.twig', array('testedGames' => $testedGames));
    }
}

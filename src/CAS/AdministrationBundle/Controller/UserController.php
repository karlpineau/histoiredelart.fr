<?php

namespace CAS\AdministrationBundle\Controller;

use CAS\UserBundle\Entity\UserPreferences;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{
    /**
     * @Route(
     *     "/utilisateurs",
     *     name="cas_administration_user_index"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $repositoryUser = $this->getDoctrine()->getManager()->getRepository('CASUserBundle:User');
        $users = $repositoryUser->findAll();

        $paginator  = $this->get('knp_paginator');
        $listUsers = $paginator->paginate(
            $users,
            $request->query->get('page', 1)/*page number*/,
            100/*limit per page*/
        );

        return $this->render('CASAdministrationBundle:User:index.html.twig', array(
            'users' => $listUsers
        ));
    }

    /**
     * @Route(
     *     "/administrateurs",
     *     name="cas_administration_user_getadmin"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAdminAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb->select('u')
            ->from('CASUserBundle:User', 'u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"ROLE_ADMIN"%');
        $users = $qb->getQuery()->getResult();

        $paginator  = $this->get('knp_paginator');
        $listUsers = $paginator->paginate(
            $users,
            $request->query->get('page', 1)/*page number*/,
            100/*limit per page*/
        );

        return $this->render('CASAdministrationBundle:User:index.html.twig', array(
            'users' => $listUsers
        ));
    }

    /**
     * @Route(
     *     "/utilisateur/voir/{id}",
     *     name="cas_administration_user_view",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param $id int
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryUser = $em->getRepository('CASUserBundle:User');
        $repositoryImportSession = $em->getRepository('DATAImportBundle:ImportSession');
        $repositoryDataReporting = $em->getRepository('DATAPublicBundle:Reporting');
        $repositoryPlayerSession = $em->getRepository('CLICHESPlayerBundle:PlayerSession');
        $user = $repositoryUser->findOneBy(['id' => $id]);

        if ($user === null) { throw $this->createNotFoundException('User : [id='.$id.'] inexistant.'); }

        $lastImportSessions = $repositoryImportSession->findBy(array('createUser' => $user), array('createDate' => 'DESC'), 10);
        $lastDataReportings = $repositoryDataReporting->findBy(array('createUser' => $user), array('createDate' => 'DESC'), 10);
        $lastPlayerSessions = $repositoryPlayerSession->findBy(array('createUser' => $user), array('createDate' => 'DESC'), 10);

        return $this->render('CASAdministrationBundle:User:view.html.twig', array(
            'user' => $user,
            'lastImportSessions' => $lastImportSessions,
            'lastDataReportings' => $lastDataReportings,
            'lastPlayerSessions' => $lastPlayerSessions
        ));
    }
}

<?php

namespace CAS\UserBundle\Controller;

use CAS\UserBundle\Form\UserPreferencesClichesType;
use CAS\UserBundle\Form\UserPreferencesMailType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserPreferencesController extends Controller
{
    /**
     * @Route(
     *     "/preferences/mail",
     *     name="cas_user_userpreferences_mail"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function mailAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $userPreferences = $em->getRepository('CASUserBundle:UserPreferences')->findOneBy(['user' => $this->getUser()]);
        if ($userPreferences === null) {throw $this->createNotFoundException('Oups ... Problème interne, revenez plus tard :)');}
        
        $form = $this->createForm(UserPreferencesMailType::class, $userPreferences);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($userPreferences);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, vos préférences mail ont bien été mises à jour.' );
            return $this->redirectToRoute('fos_user_profile_show');
        }
        
        return $this->render('CASUserBundle:UserPreferences:Mail/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route(
     *     "/preferences/cliches",
     *     name="cas_user_userpreferences_cliches"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function clichesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $userPreferences = $em->getRepository('CASUserBundle:UserPreferences')->findOneBy(['user' => $this->getUser()]);
        if ($userPreferences === null) {throw $this->createNotFoundException('Oups ... Problème interne, revenez plus tard :)');}

        $form = $this->createForm(UserPreferencesClichesType::class, $userPreferences);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($userPreferences);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, vos préférences Clichés! ont bien été mises à jour.' );
            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render('CASUserBundle:UserPreferences:Cliches/edit.html.twig', ['form' => $form->createView()]);
    }
}

<?php

namespace API\DataBundle\Controller;

use DATA\DataBundle\Entity\Entity;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializationContext;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class EntityDerivationController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/updateDerivation", name="_api_data_items_derivation")
     *
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ImagickException
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');
        /** @var Entity $entity */
        $entity = $entityService->find('one', ['id' => $request->get('id')]);
        if($entity === null) {return new JsonResponse(['Item : [id='.$request->get('id').'] inexistant.']);}
        if($entity->getEnrichmentStatus() == 0) {
            return new JsonResponse(['Dérivation inexistante pour '.$request->get('id').'.']);
        }

        foreach ($entityService->getProperties($entity, "wd") as $property) {
            $em->remove($property);
        }
        $em->flush();

        $entityService->derivation($entity, $entity->getWikidataSameAs(), null, $entityService->getTeachings($entity), true);

        return new JsonResponse(['Update '.$request->get('id').'.']);
    }
}

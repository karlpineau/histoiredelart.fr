<?php

namespace API\DataBundle\Controller;

use DATA\DataBundle\Entity\Entity;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializationContext;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class EntityController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/items", name="_api_data_items")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns all the items"
     * )
     * @SWG\Tag(name="items")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getEntitiesAction(Request $request)
    {
        $response = array();
        /** @var Entity $entity */
        $entities = $this->get('data_data.entity')->find('all');

        $response["nbResults"] = count($entities);

        foreach ($entities as $entity) {
            $response["entities"][] = $this->get('api_data.entity')->buildEntity($entity);
        }

        return new JsonResponse($response);
    }

    /**
     * @Rest\Get("/items/{id}", name="api_data_item")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns one item"
     * )
     * @SWG\Tag(name="items")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');
        /** @var Entity $entity */
        $entity = $entityService->find('one', ['id' => $request->get('id')]);
        $entityResponse = null;

        if($entity != null) {
            $entityResponse = $this->get('api_data.entity')->buildEntity($entity);
        }

        return new JsonResponse($entityResponse);
    }
}

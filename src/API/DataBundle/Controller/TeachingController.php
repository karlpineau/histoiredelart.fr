<?php

namespace API\DataBundle\Controller;

use DATA\TeachingBundle\Entity\Teaching;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializationContext;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class TeachingController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/teachings", name="_api_data_teachings")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns all the teachings"
     * )
     * @SWG\Tag(name="items")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getTeachingsAction(Request $request)
    {
        $response = array();
        $teachings = $this->getDoctrine()->getRepository('DATATeachingBundle:Teaching')->findAll();
        $response["nbResults"] = count($teachings);
        /** @var Teaching $teaching */
        foreach ($teachings as $teaching) {
            $response["teachings"][] = $this->get('api_data.teaching')->buildTeaching($teaching);
        }

        return new JsonResponse($response);
    }

    /**
     * @Rest\Get("/teachings/{id}", name="_api_data_teaching")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns one teaching"
     * )
     * @SWG\Tag(name="items")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Teaching $teaching */
        $teaching = $em->getRepository('DATATeachingBundle:Teaching')->find($request->get('id'));
        $teachingResponse = null;

        if($teaching != null) {
            $teachingResponse = $this->get('api_data.teaching')->buildTeaching($teaching);
        }

        return new JsonResponse($teachingResponse);
    }
}

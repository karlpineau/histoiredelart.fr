<?php

namespace API\DataBundle\Service;

use CAS\UserBundle\Entity\Favorite;
use CLICHES\PlayerBundle\Entity\PlayerProposalChoiceValue;
use DATA\DataBundle\Entity\EntityProperty;
use DATA\DataBundle\Entity\Pad;
use DATA\DataBundle\Entity\Source;
use DATA\ImageBundle\Entity\View;
use DATA\PublicBundle\Entity\Reporting;
use DATA\PublicBundle\Entity\Visit;
use DATA\SearchBundle\Entity\SearchIndex;
use DATA\TeachingBundle\Entity\Teaching;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Psr\Log\Test\LoggerInterfaceTest;

class entity
{
    protected $em;
    protected $entity;
    protected $view;
    protected $logger;

    public function __construct(EntityManager $EntityManager, \DATA\DataBundle\Service\entity $entity, \DATA\ImageBundle\Service\view $view, LoggerInterface $logger)
    {
        $this->em = $EntityManager;
        $this->entity = $entity;
        $this->view = $view;
        $this->logger = $logger;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * @param \DATA\DataBundle\Entity\Entity $entity
     * @return mixed
     */
    public function buildEntity($entity) {
        $entityResponse["id"] = $entity->getId();
        $entityResponse["wikidataSameAs"] = $entity->getWikidataSameAs();

        /* EntityProperties */
        $properties = $this->em->getRepository('DATADataBundle:EntityProperty')->findBy(['entity' => $entity]);
        $entityResponse["properties"] = array();
        /** @var EntityProperty $property */
        foreach ($properties as $property) {
            $entityResponse["properties"][] = [
                "property" => $property->getProperty(),
                "value" => json_decode($property->getValue())
            ];
        }

        /* Views */
        $views = $this->entity->getViews($entity);
        $entityResponse["views"] = array();
        /** @var View $view */
        foreach ($views as $view) {
            $viewArray = array();

            $viewArray["teachings"] = array();
            /** @var Teaching $teaching */
            foreach ($view->getTeachings() as $teaching) {
                $viewArray["teachings"][] = [
                    "id" => $teaching->getId(),
                    "name" => $teaching->getName()
                ];
            }

            $viewArray["imageLocation"] = $this->view->getThumbnail($view);

            $entityResponse["views"][] = $viewArray;
        }

        /* SameAs */
        $entityResponse["sameAs"] = $entity->getWikidataSameAs();

        /* Sources */
        $sources = $this->entity->getSources($entity);
        $entityResponse["sources"] = array();
        /** @var Source $source */
        foreach ($sources as $source) {
            $entityResponse["sources"][] = [
                "url" => $source->getUrl(),
                "title" => $source->getTitle(),
                "isAuthority" => $source->getAuthorityControl()
            ];
        }
        
        return $entityResponse;
    }

}

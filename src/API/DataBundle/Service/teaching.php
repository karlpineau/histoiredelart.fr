<?php

namespace API\DataBundle\Service;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Psr\Log\Test\LoggerInterfaceTest;

class teaching
{
    protected $em;
    protected $entityApi;
    protected $logger;

    public function __construct(EntityManager $EntityManager, \API\DataBundle\Service\entity $entity, LoggerInterface $logger)
    {
        $this->em = $EntityManager;
        $this->entityApi = $entity;
        $this->logger = $logger;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * @param \DATA\TeachingBundle\Entity\Teaching $teaching
     * @return mixed
     */
    public function buildTeaching($teaching) {
        $teachingResponse["id"] = $teaching->getId();
        $teachingResponse["name"] = $teaching->getName();
        $teachingResponse["isOnline"] = $teaching->getOnLine();


        $teachingResponse["items"] = array();

        /** @var View $view */
        foreach ($this->em->getRepository("DATAImageBundle:View")->findAll() as $view) {
            foreach ($view->getTeachings() as $teachingV) {
                if($teaching == $teachingV) {
                    $teachingResponse["items"][] = $this->entityApi->buildEntity($view->getEntity());
                }
            }
        }
        
        return $teachingResponse;
    }

}

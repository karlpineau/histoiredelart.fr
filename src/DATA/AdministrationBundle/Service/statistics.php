<?php

namespace DATA\AdministrationBundle\Service;

use Doctrine\ORM\EntityManager;
use DATA\DataBundle\Service\entity;

class statistics
{
    protected $em;
    protected $entity;

    public function __construct(EntityManager $EntityManager, entity $entity)
    {
        $this->em = $EntityManager;
        $this->entity = $entity;
    }

    public function getStats($entityRepository, $field)
    {
        $repository = $this->em->getRepository($entityRepository);
        $resultsGlob = $repository->findAll();
        $resultsEmpty = $repository->findBy(array($field => null));
        $result = (count($resultsEmpty)*100)/count($resultsGlob);

        return $result;
    }
        
}

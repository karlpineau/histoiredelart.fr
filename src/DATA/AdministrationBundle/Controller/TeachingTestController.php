<?php

namespace DATA\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TeachingTestController extends Controller
{
    /**
     * @Route(
     *     "/teaching-test",
     *     name="data_administration_teachingtest_index"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryTeachingTest = $em->getRepository('DATATeachingBundle:TeachingTest');
        $repositoryTeachingTestVote = $em->getRepository('DATATeachingBundle:TeachingTestVote');
        $teachingTests = $repositoryTeachingTest->findAll();

        $teachingTestsArray = array();
        foreach($teachingTests as $teachingTest) {
            $teachingTestVotes = $repositoryTeachingTestVote->findBy(['teachingTest' => $teachingTest]);
            $teachingTestVotesOui = $repositoryTeachingTestVote->findBy(['teachingTest' => $teachingTest, 'vote' => true]);
            $teachingTestVotesNon = $repositoryTeachingTestVote->findBy(['teachingTest' => $teachingTest, 'vote' => false]);
            $teachingTestsArray[] = [
                'teachingTest' => $teachingTest,
                'teachingTestVotes' => $teachingTestVotes,
                'teachingTestVotesOui' => $teachingTestVotesOui,
                'teachingTestVotesNon' => $teachingTestVotesNon
            ];
        }

        return $this->render('DATAAdministrationBundle:TeachingTest:index.html.twig', ['teachingTests' => $teachingTestsArray]);
    }

    /**
     * @Route(
     *     "/teaching-test/derniers-votes",
     *     name="data_administration_teachingtest_lastvotes"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function lastVotesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryTeachingTestVote = $em->getRepository('DATATeachingBundle:TeachingTestVote');
        $votes = $repositoryTeachingTestVote->findBy([], ['createDate' => 'DESC']);

        $paginator  = $this->get('knp_paginator');
        $lastVotes = $paginator->paginate(
            $votes,
            $request->query->get('page', 1)/*page number*/,
            100/*limit per page*/
        );

        return $this->render('DATAAdministrationBundle:TeachingTest:lastVotes.html.twig', ['lastVotes' => $lastVotes]);
    }
}

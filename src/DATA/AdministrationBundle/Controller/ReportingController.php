<?php

namespace DATA\AdministrationBundle\Controller;

use DATA\PublicBundle\Entity\Reporting;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ReportingController extends Controller
{
    /**
     * @Route(
     *     "/signalements",
     *     name="data_administration_reporting_index"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryReporting = $em->getRepository('DATAPublicBundle:Reporting');
        /** @var Reporting[] $reportings */
        $reportings = $repositoryReporting->findBy(['traitement' => false]);

        return $this->render('DATAAdministrationBundle:Reporting:index.html.twig', ['reportings' => $reportings]);
    }

    /**
     * @Route(
     *     "/signalements/validation/{reporting_id}",
     *     name="data_administration_reporting_validate",
     *     requirements={
     *          "reporting_id"="\d+"
     *     }
     * )
     * @param int $reporting_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function validateAction($reporting_id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Reporting $reporting */
        $reporting = $em->getRepository('DATAPublicBundle:Reporting')->findOneBy(['id' => $reporting_id]);
        if ($reporting === null) {throw $this->createNotFoundException('Reporting : [id='.$reporting_id.'] inexistant.');}

        $reportingAction = $this->container->get('data_public.reporting');
        $reportingAction->validateReporting($reporting);

        if($reporting->getCreateUser() != null and $em->getRepository('CASUserBundle:UserPreferences')->findOneBy(['user' => $reporting->getCreateUser()])->getDataReportingConfirmation() == true) {
            /* -- l'utilisateur souhaite être notifié par mail du résultat : -- */
            $message = \Swift_Message::newInstance()
                ->setSubject('Merci d\'avoir contribué à DATA !')
                ->setFrom('cliches@histoiredelart.fr')
                ->setTo($reporting->getCreateUser()->getEmail())
                ->setBody(
                    $this->renderView(
                        'DATAAdministrationBundle:Reporting:mailInfoYes.html.twig',
                        array('reporting' => $reporting)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);
        }

        $this->get('session')->getFlashBag()->add('notice', 'Reporting a bien été notifié comme validé.' );
        return $this->forward('DATAAdministrationBundle:Reporting:index');
    }

    /**
     * @Route(
     *     "/signalements/refus/{reporting_id}",
     *     name="data_administration_reporting_refuse",
     *     requirements={
     *          "reporting_id"="\d+"
     *     }
     * )
     * @param int $reporting_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function refuseAction($reporting_id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Reporting $reporting */
        $reporting = $em->getRepository('DATAPublicBundle:Reporting')->findOneBy(['id' => $reporting_id]);
        if ($reporting === null) {throw $this->createNotFoundException('Reporting : [id='.$reporting_id.'] inexistant.');}

        $reportingAction = $this->container->get('data_public.reporting');
        $reportingAction->refuseReporting($reporting);

        if($reporting->getCreateUser() != null and $em->getRepository('CASUserBundle:UserPreferences')->findOneByUser($reporting->getCreateUser())->getDataReportingConfirmation() == true) {
            /* -- l'utilisateur souhaite être notifié par mail du résultat : -- */
            $message = \Swift_Message::newInstance()
                ->setSubject('Merci d\'avoir contribué à DATA !')
                ->setFrom('cliches@histoiredelart.fr')
                ->setTo($reporting->getCreateUser()->getEmail())
                ->setBody(
                    $this->renderView(
                        'DATAAdministrationBundle:Reporting:mailInfoNo.html.twig',
                        ['reporting' => $reporting]
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);
        }

        $this->get('session')->getFlashBag()->add('notice', 'Reporting a bien été notifié comme refusé.' );
        return $this->forward('DATAAdministrationBundle:Reporting:index');
    }
}

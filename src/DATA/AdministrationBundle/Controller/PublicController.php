<?php

namespace DATA\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PublicController extends Controller
{
    /**
     * @Route(
     *     "/visites",
     *     name="data_administration_public_visit"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function visitAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $visitsQuery = $em->getRepository('DATAPublicBundle:Visit')->findBy([], ['createDate' => 'DESC']);

        $paginator  = $this->get('knp_paginator');
        $visits = $paginator->paginate(
            $visitsQuery,
            $request->query->get('page', 1)/*page number*/,
            300/*limit per page*/
        );

        return $this->render('DATAAdministrationBundle:Public:visit.html.twig', ['visits' => $visits]);
    }
}

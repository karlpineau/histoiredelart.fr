<?php

namespace DATA\AdministrationBundle\Controller;

use DATA\DataBundle\Service\entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class TestDateController extends Controller
{
    /**
     * @Route(
     *     "/test-date",
     *     name="data_administration_testdate_index"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var entity $entityService */
        $entityService = $this->get('data_data.entity');
        $entityProperty = $em->getRepository('DATADataBundle:EntityProperty');
        $precisionService = $this->get('data_data.property');

        $entities = array();
        $entities[] = ['entity' => $entityService->getById('317'), 'trueValue' => '26e siècle av. J.⁠-⁠C.', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 317, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 317, 'property' => 'wd-P571'])->getValue()]; // Oies de Meidoum
        $entities[] = ['entity' => $entityService->getById('444'), 'trueValue' => '17e siècle', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 444, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 444, 'property' => 'wd-P571'])->getValue()]; // Les reines de Perse aux pieds d'Alexandre
        $entities[] = ['entity' => $entityService->getById('1523'), 'trueValue' => '17e siècle (entre 1650 et 1675)', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 1523, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 1523, 'property' => 'wd-P571'])->getValue()]; // Paysage rocheux avec un chasseur
        $entities[] = ['entity' => $entityService->getById('2621'), 'trueValue' => 'années 800', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 2621, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 2621, 'property' => 'wd-P571'])->getValue()]; // Évangéliaire de Saint-Médard de Soissons
        $entities[] = ['entity' => $entityService->getById('1535'), 'trueValue' => 'années 1750', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 1535, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 1535, 'property' => 'wd-P571'])->getValue()]; // Scènes de Carnaval
        $entities[] = ['entity' => $entityService->getById('751'), 'trueValue' => 'années 1900', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 751, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 751, 'property' => 'wd-P571'])->getValue()]; // Le Grand Nu
        $entities[] = ['entity' => $entityService->getById('1578'), 'trueValue' => 'années 1390 (de 1386 à 1400)', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 1578, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 1578, 'property' => 'wd-P571'])->getValue()]; // Psautier de Jean de Berry
        $entities[] = ['entity' => $entityService->getById('388'), 'trueValue' => '1449 / années 1440', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 388, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 388, 'property' => 'wd-P571'])->getValue()]; // Crucifixion du Parlement de Paris
        $entities[] = ['entity' => $entityService->getById('426'), 'trueValue' => '85 av. J.-C.', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 426, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 426, 'property' => 'wd-P571'])->getValue()]; // Général de Tivoli
        $entities[] = ['entity' => $entityService->getById('456'), 'trueValue' => '1000 av. J.-C.', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 456, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 456, 'property' => 'wd-P571'])->getValue()]; // Cône d'or
        $entities[] = ['entity' => $entityService->getById('2358'), 'trueValue' => 'janvier 1890', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 2358, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 2358, 'property' => 'wd-P571'])->getValue()]; // La Méridienne
        $entities[] = ['entity' => $entityService->getById('383'), 'trueValue' => '23 avril 1855', 'precision' => $precisionService->getTimePrecision($entityProperty->findOneBy(['entity' => 383, 'property' => 'wd-P571'])), 'dateJS' => $entityProperty->findOneBy(['entity' => 383, 'property' => 'wd-P571'])->getValue()]; // Vallée de l'ombre de la mort

        return $this->render('DATAAdministrationBundle:TestDate:index.html.twig', array(
            'entities' => $entities
        ));
    }
}

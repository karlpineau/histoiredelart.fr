<?php

namespace DATA\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends Controller
{
    /**
     * @Route(
     *     "/recherches/statistiques",
     *     name="data_administration_search_statistics"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function statisticsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $searchesResults = $em->getRepository('DATASearchBundle:SearchLog')->findBy([], ['createDate' => 'DESC']);

        $paginator  = $this->get('knp_paginator');
        $searches = $paginator->paginate(
            $searchesResults,
            $request->query->get('page', 1)/*page number*/,
            100/*limit per page*/
        );

        return $this->render('DATAAdministrationBundle:Search:statistics.html.twig', ['searches' => $searches]);
    }
}

<?php

namespace DATA\AdministrationBundle\Controller;

use DATA\DataBundle\Service\entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route(
     *     "/",
     *     name="data_administration_home_index"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var entity $entityService */
        $entityService = $this->get('data_data.entity');

        $repositoryReporting = $em->getRepository('DATAPublicBundle:Reporting');
        $reportings = count($repositoryReporting->findBy(['traitement' => false]));
        $entitiesCount = count($em->getRepository("DATADataBundle:Entity")->findAll());
        $viewsCount = count($em->getRepository("DATAImageBundle:View")->findAll());
        $propertiesCount = count($em->getRepository("DATADataBundle:EntityProperty")->findAll());
        $alignmentsCount = count($entityService->find('all', ['enrichmentStatus' => 1]));



        $import = count($entityService->find('all', ['importValidation' => false]));

        return $this->render('DATAAdministrationBundle:Home:index.html.twig', array(
            'reportings' => $reportings,
            'import' => $import,
            'entitiesCount' => $entitiesCount,
            'viewsCount' => $viewsCount,
            'propertiesCount' => $propertiesCount,
            'alignmentsCount' => $alignmentsCount
        ));
    }
}

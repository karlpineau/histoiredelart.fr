<?php

namespace DATA\DataBundle\Controller;

use DATA\DataBundle\Entity\Entity;
use DATA\DataBundle\Entity\EntitySources;
use DATA\DataBundle\Entity\Source;
use DATA\DataBundle\Form\EntitySourcesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EntitySourcesController extends Controller
{
    /**
     * @Route(
     *     "/source/edit/{id}",
     *     name="data_data_entity_edit_sources",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function sourcesForEntityAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entityService = $this->container->get('data_data.entity');
        /** @var Entity $entity */
        $entity = $entityService->getById($id);
        if ($entity === null) { throw $this->createNotFoundException('Item : [id='.$id.'] inexistant.'); }

        $entitySources = new EntitySources();
        /** @var Source[] $sources */
        $sources = $em->getRepository('DATADataBundle:Source')->findBy(['entity' => $entity]);
        foreach($sources as $source) {
            $entitySources->addSource($source);
        }
        
        $form = $this->createForm(EntitySourcesType::class, $entitySources);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entitySources);
            /** @var Source $source */
            foreach($entitySources->getSources() as $source) {
                $source->setEntity($entity);
                $em->persist($source);
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, les sources ont bien été modifiées.' );
            return $this->redirect($this->generateUrl('data_data_entity_view', ['id' => $entity->getId()]));
        }

        return $this->render('DATADataBundle:Entity:Sources/editSources.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }
}

<?php

namespace DATA\DataBundle\Controller;

use CLICHES\PlayerBundle\Entity\ExcludeView;
use CLICHES\PlayerBundle\Service\excludeViewService;
use DATA\DataBundle\Entity\EntityProperty;
use DATA\DataBundle\Form\MergeViewsType;
use DATA\DataBundle\Entity\Entity;
use DATA\ImageBundle\Entity\Image;
use DATA\ImageBundle\Entity\View;
use DATA\ImageBundle\Form\ImageRegisterType;
use DATA\ImportBundle\Entity\ImportSession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class EntityViewsController extends Controller
{
    /**
     * @Route(
     *     "/view/add/{id}",
     *     name="data_data_entity_addview",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addViewAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');

        /** @var Entity $entity */
        $entity = $entityService->getById($id);
        if ($entity === null) { throw $this->createNotFoundException('Item : [id='.$id.'] inexistant.'); }

        $view = new View();
        $view->setCreateUser($this->getUser());
        $view->setOrderView(count($entityService->getViews($entity))+1);

        $image = new Image();
        $image->setCreateUser($this->getUser());
        $image->setView($view);

        $view->setEntity($entity);
        
        $form = $this->createForm(ImageRegisterType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($image);
            $em->persist($view);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, la vue a bien été créée.' );
            return $this->redirect($this->generateUrl('data_data_entity_view', ['id' => $entity->getId()]));
        }

        return $this->render('DATAImageBundle:Image:Register/register.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(
     *     "/view/dislink/{id}",
     *     name="data_data_entity_view_unrelated",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function dislinkAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var View $view */
        $view = $em->getRepository('DATAImageBundle:View')->findOneBy(['id' => $id]);

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $entity = new Entity();
        $entity->setCreateUser($user);
        $entity->setEnrichmentStatus(0);
        $entity->setImportSession(null);
        $entity->setImportValidation(true);
        $view->setEntity($entity);

        $propertyName = new EntityProperty();
        $propertyName->setCreateUser($user);
        $propertyName->setEntity($entity);
        $propertyName->setProperty("hda-label");
        $propertyName->setValue(json_encode($this->get('data_data.property')->generatePropertyValue('label', ["À définir"], "fr")));

        $em->persist($propertyName);
        $em->persist($view);
        $em->persist($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'La vue a bien été déliée.' );
        return $this->redirectToRoute('data_data_entity_view', ['id' => $entity->getId()]);
    }

    /**
     * @Route(
     *     "/view/exclude/{view_id}",
     *     name="data_data_entity_view_exclusion",
     *     requirements={
     *          "view_id"="\d+"
     *     }
     * )
     * @param $view_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function exclusionFromClichesAction($view_id)
    {
        $em = $this->getDoctrine()->getManager();
        $viewRepository = $em->getRepository('DATAImageBundle:View');
        $excludeViewRepository = $em->getRepository('CLICHESPlayerBundle:ExcludeView');
        /** @var View $view */
        $view = $viewRepository->findOneBy(['id' => $view_id]);
        /** @var ExcludeView $excludeView */
        $excludeView = $excludeViewRepository->findOneBy(['view' => $view]);
        /** @var excludeViewService $excludeService */
        $excludeService = $this->container->get('cliches_player.player_exclude');

        if ($view === null) { throw $this->createNotFoundException('View : [id='.$view_id.'] inexistante.'); }

        if($excludeView === null) { 
            $excludeService->excludeView($view);
        } elseif($excludeView !== null) {
            $excludeService->includeView($excludeView);
        }
        
        return $this->redirectToRoute('data_data_entity_view', ['id' => $view->getEntity()->getId()]);

    }

    /**
     * @Route(
     *     "/view/sort/{view_id}/{currentOrder}/{way}",
     *     name="data_data_entity_view_reorder",
     *     requirements={
     *          "view_id"="\d+",
     *          "currentOrder"="\d+",
     *          "slug"="up|down"
     *     }
     * )
     * @param int $view_id
     * @param int $currentOrder
     * @param string $way
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function orderViewAction($view_id, $currentOrder, $way)
    {
        $em = $this->getDoctrine()->getManager();
        $viewRepository = $em->getRepository('DATAImageBundle:View');
        /** @var View $view */
        $view = $viewRepository->findOneBy(['id' => $view_id]);
        if ($view === null) { throw $this->createNotFoundException('View : [id='.$view_id.'] inexistante.'); }

        if($way == 'up' AND $currentOrder > 1) {$newOrder = $currentOrder - 1;}
        elseif($way == 'down') {$newOrder = $currentOrder + 1;}
        
        $view->setOrderView($newOrder);
        $em->persist($view);

        $otherView = $viewRepository->findOneBy(['entity' => $view->getEntity(), 'orderView' => $newOrder]);
        $otherView->setOrderView($currentOrder);
        $em->persist($otherView);

        $em->flush();

        return $this->redirectToRoute('data_data_entity_view', ['id' => $view->getEntity()->getId()]);
    }

    /**
     * @Route(
     *     "/view/sort-all",
     *     name="data_data_entity_view_initialOrderViews"
     * )
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function initialOrderViewsAction()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->container->get('data_data.entity');

        $count = 0;
        /** @var Entity $entity */
        foreach($entityService->find('all', null, 'large') as $entity) {
            $views = $entityService->getViews($entity);

            /**
             * @var int $key
             * @var View $view
             */
            foreach($views as $key => $view) {
                $view->setOrderView($key+1);
                $em->persist($view);
                ++$count;
            }
        }
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', $count.' vues ont été ordonnées.' );
        return $this->redirectToRoute('data_administration_home_index');
    }

    /**
     * @Route(
     *     "/view/merge/{view_id}",
     *     name="data_data_entity_view_mergeViews",
     *     requirements={
     *          "view_id"="\d+"
     *     }
     * )
     * @param int $view_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function mergeViewsAction($view_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var View $view */
        $view = $em->getRepository('DATAImageBundle:View')->findOneBy(['id' => $view_id]);
        if ($view === null) { throw $this->createNotFoundException('View : [id='.$view_id.'] inexistante.'); }
        
        $form = $this->createForm(MergeViewsType::class, [], ['attr' => ['entity' => $view->getEntity()->getId(), 'currentView_id' => $view->getId()]]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->container->get('data_image.view')->mergeViews($form->get('view')->getData(), $view);
                
            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, les vues ont bien été fusionnées.' );
            return $this->redirect($this->generateUrl('data_data_entity_view', ['id' => $view->getEntity()->getId()]));
        }
        
        return $this->render('DATADataBundle:Entity:View/Views/view-views-merge.html.twig', array(
            'form' => $form->createView(),
            'view' => $view
        ));
    }
}

<?php

namespace DATA\DataBundle\Controller;

use DATA\DataBundle\Entity\Source;
use DATA\DataBundle\Entity\SourceClick;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SourceController extends Controller
{
    /**
     * @Route(
     *     "/source/click/{source_id}/{context}",
     *     name="data_data_source_addClick",
     *     requirements={
     *          "source_id"="\d+",
     *          "context"="\S{0,255}"
     *     },
     *     options={
     *          "expose"=true
     *     }
     * )
     * @param int $source_id
     * @param string $context
     * @param Request $request
     * @return Response
     */
    public function addClickAction($source_id, $context, Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            $em = $this->getDoctrine()->getEntityManager();
            $repositorySource = $em->getRepository('DATADataBundle:Source');
            /** @var Source $source */
            $source = $repositorySource->findOneBy(['id' => $source_id]);
            
            $sourceClick = new SourceClick();
            $sourceClick->setContext($context);
            $sourceClick->setIpCreateUser($request->getClientIp());
            if($this->getUser() != null) {$sourceClick->setCreateUser($this->getUser());}
            $sourceClick->setSource($source);
            $em->persist($sourceClick);
            $em->flush();
            
            $response = new Response(json_encode("ok"));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }
}

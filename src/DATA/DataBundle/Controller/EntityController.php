<?php

namespace DATA\DataBundle\Controller;

use DATA\DataBundle\Entity\Entity;
use DATA\DataBundle\Entity\EntityProperty;
use DATA\DataBundle\Form\EntityPropertyType;
use DATA\DataBundle\Form\EntityType;
use DATA\ImageBundle\Service\view;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EntityController extends Controller
{
    /**
     * @Route(
     *     "/",
     *     name="data_data_entity_index"
     * )
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function indexAction(Request $request)
    {
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');
        /** @var Entity[] $entities */
        $entities = $this->get('data_data.entity')->find('all', null, 'large');
        
        $transi = array();
        foreach($entities as $entity) {
            if($entityService->countProperties($entity) == 0) {
                $entityService->removeEntity($entity);
            } else {
                $transi[$entity->getId()] = $entityService->get($entity, 'label');
            }
        }
        natsort($transi);

        $return = array();
        foreach($transi as $keyId => $name) {
            $return[] = $keyId;
        }

        $paginator  = $this->get('knp_paginator');
        $listEntities = $paginator->paginate(
            $return,
            $request->query->get('page', 1)/*page number*/,
            100/*limit per page*/
        );

        return $this->render('DATADataBundle:Entity:index.html.twig', ['listEntities' => $listEntities]);
    }

    /**
     * @Route(
     *     "/{id}",
     *     name="data_data_entity_view",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param int $id
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryPad = $em->getRepository('DATADataBundle:Pad');
        $repositoryVisit = $em->getRepository('DATAPublicBundle:Visit');
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->container->get('data_data.entity');
        /** @var view $viewService */
        $viewService = $this->container->get('data_image.view');

        /** @var Entity $entity */
        $entity = $entityService->find('one', array('id' => $id), 'large');
        if ($entity === null) { throw $this->createNotFoundException('Entité : [id='.$id.'] inexistante.'); }

        $visits = $repositoryVisit->findBy(['entity' => $entity]);
        $pad = $repositoryPad->findOneBy(['entity' => $entity]);
        $teachings = $entityService->getTeachings($entity);
        $relatedViews = $entityService->getViewsForEntityAdmin($entity);

        if(count($relatedViews) > 1) {$viewService->checkOrderViews($relatedViews);}

        $properties = null;
        if($entity->getEnrichmentStatus() == 1) {
            /** @var EntityProperty[] $properties */
            $properties = $entityService->getProperties($entity, "wd", true);
        } else {
            /** @var EntityProperty[] $properties */
            $properties = $entityService->getProperties($entity, "hda", true);
        }

        $qwd = null;
        $duplicates = null;
        if($entity->getWikidataSameAs() != null) {
            $qwd = $this->get('data_data.wikidata')->getQwd($entity->getWikidataSameAs());
            $duplicates = $entityService->getDuplicates($entity);
        }

        return $this->render('DATADataBundle:Entity:view.html.twig', array(
            'entity' => $entity,
            'properties' => $properties,
            'qwd' => $qwd,
            'relatedViews' => $relatedViews,
            'teachings' => $teachings,
            'visits' => $visits,
            'pad' => $pad,
            'duplicates' => $duplicates
        ));
    }

    /**
     * @Route(
     *     "/edit/{id}",
     *     name="data_data_entity_edit",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param int $id
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->container->get('data_data.entity');

        /** @var Entity $entity */
        $entity = $entityService->find('one', array('id' => $id), 'large');
        if ($entity === null) { throw $this->createNotFoundException('Entité : [id='.$id.'] inexistante.'); }

        $form = $this->createForm(EntityType::class, $entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var EntityProperty $property */
            foreach ($form->get('properties')->getData() as $property) {
                if($property->getProperty() != null and $property->getValue() != null) {
                    $property->setEntity($entity);
                    $em->persist($property);
                } else {
                    $em->remove($property);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, l\'entité a bien été modifiée.' );
            return $this->redirectToRoute('data_data_entity_view', array('id' => $entity->getId()));
        }

        return $this->render('DATADataBundle:Entity:Edit/edit.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity
        ));
    }

    /**
     * @Route(
     *     "/delete/{id}",
     *     name="data_data_entity_delete",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $entityService = $this->get('data_data.entity');
        /** @var Entity $entity */
        $entity = $entityService->getById($id);
        if($entity === null) {throw $this->createNotFoundException('Item : [id='.$id.'] inexistant.');}

        $entityService->removeEntity($entity);

        $this->get('session')->getFlashBag()->add('notice', 'Votre item a bien été supprimé.' );
        return $this->redirectToRoute('data_data_entity_index');
    }
}

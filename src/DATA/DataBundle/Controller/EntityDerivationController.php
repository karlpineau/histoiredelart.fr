<?php

namespace DATA\DataBundle\Controller;

use DATA\DataBundle\Entity\Entity;
use DATA\DataBundle\Entity\EntityProperty;
use DATA\DataBundle\Form\EntityPropertyType;
use DATA\DataBundle\Form\EntityType;
use DATA\ImageBundle\Service\view;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EntityDerivationController extends Controller
{
    /**
     * @Route(
     *     "/derivation/{entity_id}",
     *     name="data_data_entityderivation_derivation",
     *     requirements={
     *          "entity_id"="\d+"
     *     }
     * )
     * @param int $entity_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ImagickException
     */
    public function derivationAction($entity_id)
    {
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');
        /** @var Entity $entity */
        $entity = $entityService->getById($entity_id);
        if($entity === null) {throw $this->createNotFoundException('Item : [id='.$entity_id.'] inexistant.');}

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if($user == "anon.") {$user = null;}

        $entityService->derivation($entity, $entity->getWikidataSameAs(), $user, $entityService->getTeachings($entity));

        $this->get('session')->getFlashBag()->add('notice', 'Votre item a bien été dérivé.' );
        return $this->redirectToRoute('data_data_entity_view', ["id" => $entity_id]);
    }

    /**
     * @Route(
     *     "/updateDerivation/{entity_id}",
     *     name="data_data_entityderivation_update",
     *     requirements={
     *          "entity_id"="\d+"
     *     }
     * )
     * @param int $entity_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ImagickException
     */
    public function updateAction($entity_id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');
        /** @var Entity $entity */
        $entity = $entityService->getById($entity_id);
        if($entity === null) {throw $this->createNotFoundException('Item : [id='.$entity_id.'] inexistant.');}
        if($entity->getEnrichmentStatus() == 0) {
            $this->get('session')->getFlashBag()->add('notice', 'La dérivation ne peut pas être mise à jour car elle n\'existe pas.' );
            return $this->redirectToRoute('data_data_entity_view', ["id" => $entity_id]);
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if($user == "anon.") {$user = null;}

        foreach ($entityService->getProperties($entity, "wd") as $property) {
            $em->remove($property);
        }
        $em->flush();

        $entityService->derivation($entity, $entity->getWikidataSameAs(), $user, $entityService->getTeachings($entity), true);

        $this->get('session')->getFlashBag()->add('notice', 'La dérivation a bien été mise à jour.' );
        return $this->redirectToRoute('data_data_entity_view', ["id" => $entity_id]);
    }

    /**
     * @Route(
     *     "/derivation/remove/{entity_id}",
     *     name="data_data_entityderivation_removederivation",
     *     requirements={
     *          "entity_id"="\d+"
     *     }
     * )
     * @param string $entity_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function removeDerivationAction($entity_id) {
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');
        /** @var Entity $entity */
        $entity = $entityService->getById($entity_id);
        if($entity === null) {throw $this->createNotFoundException('Item : [id='.$entity_id.'] inexistant.');}

        $entityService->removeDerivation($entity);

        $this->get('session')->getFlashBag()->add('notice', 'Votre item a bien été désaligné.' );
        return $this->redirectToRoute('data_data_entity_view', ["id" => $entity_id]);
    }

    /**
     * @Route(
     *     "/toDerive",
     *     name="data_data_entityderivation_toDerive"
     * )
     * @param Request $request
     * @return Response
     */
    public function getToDeriveAction(Request $request)
    {
        /** @var Entity[] $entities */
        $entities = $this->get('data_data.entity')->find('array', ["enrichmentStatus" => 0], 'large');
        $return = [];

        foreach($entities as $entity) {
            if($entity->getWikidataSameAs() != null) {
                array_push($return, $entity);
            }
        }

        $paginator  = $this->get('knp_paginator');
        $listEntities = $paginator->paginate(
            $return,
            $request->query->get('page', 1)/*page number*/,
            100/*limit per page*/
        );

        return $this->render('DATADataBundle:Entity:index.html.twig', ['listEntities' => $listEntities]);
    }

    /**
     * @Route(
     *     "/derived",
     *     name="data_data_entityderivation_derived"
     * )
     * @param Request $request
     * @return Response
     */
    public function getDerivedAction(Request $request)
    {
        /** @var Entity[] $entities */
        $entities = $this->get('data_data.entity')->find('array', ["enrichmentStatus" => 1], 'large');
        $return = [];

        foreach($entities as $entity) {
            if($entity->getWikidataSameAs() != null) {
                array_push($return, $entity);
            }
        }

        $paginator  = $this->get('knp_paginator');
        $listEntities = $paginator->paginate(
            $return,
            $request->query->get('page', 1)/*page number*/,
            100/*limit per page*/
        );

        return $this->render('DATADataBundle:Entity:index.html.twig', ['listEntities' => $listEntities]);
    }



    /**
     * @Route(
     *     "/updateAllDerivation",
     *     name="data_data_entityderivation_all"
     * )
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ImagickException
     */
    public function updateAllAction()
    {
        set_time_limit(0);
        $em = $this->getDoctrine()->getManager();
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');
        /** @var Entity $entity */

        $entities = $entityService->find('all', ['enrichmentStatus' => 1], 'large');

        /** @var \DATA\DataBundle\Entity\Entity $entity */
        foreach($entities as $entity) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            if ($user == "anon.") {
                $user = null;
            }

            foreach ($entityService->getProperties($entity, "wd") as $property) {
                $em->remove($property);
            }
            $em->flush();

            $entityService->derivation($entity, $entity->getWikidataSameAs(), $user, $entityService->getTeachings($entity), true);
        }

        $this->get('session')->getFlashBag()->add('notice', 'Les dérivations ont bien été mises à jour.' );
        return $this->redirectToRoute('data_administration_home_index');
    }
}

<?php

namespace DATA\DataBundle\Controller;

use DATA\DataBundle\Entity\Entity;
use DATA\DataBundle\Form\MergeType;
use DATA\DataBundle\Service\merge;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MergeController extends Controller
{
    /**
     * @Route(
     *     "/merge",
     *     name="data_entity_merge_merge"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function mergeAction(Request $request)
    {
        $entityService = $this->container->get('data_data.entity');

        $form = $this->createForm(MergeType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Entity $entityLeft */
            $entityLeft = $entityService->getById($form->get('entityLeft')->getData());
            if ($entityLeft === null) { throw $this->createNotFoundException('ItemLeft : [id='.$form->getData()->get('entityLeft').'] inexistant.'); }

            /** @var Entity $entityRight */
            $entityRight = $entityService->getById($form->get('entityRight')->getData());
            if ($entityRight === null) { throw $this->createNotFoundException('ItemRight : [id='.$form->getData()->get('entityRight').'] inexistant.'); }

            //EntityRight est fusionnée dans EntityLeft
            /** @var merge $mergeService */
            $mergeService = $this->container->get('data_data.merge');
            $left = $mergeService->mergeDuplicates($entityLeft->getId(), $entityRight->getId());

            $this->get('session')->getFlashBag()->add('notice', 'Doublons fusionnés' );
            return $this->redirect($this->generateUrl('data_administration_home_index'));
        }

        return $this->render('DATADataBundle:Merge:merge.html.twig', array(
            'form' => $form->createView(),
        ));
    }


}

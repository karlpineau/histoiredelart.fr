<?php

namespace DATA\DataBundle\Command;

use DATA\DataBundle\Service\entity;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Routing\Annotation\Route;

class AlignmentCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('align:properties')

            // the short description shown while running "php bin/console list"
            ->setDescription('Align intern properties with Wikidata Properties')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'Set alignments',
            '============',
        ]);

        /** @var $entityService entity */
        $entityService = $this->getApplication()->getKernel()->getContainer()->get('data_data.entity');

        $entities = $entityService->find('all', null, 'large');

        foreach($entities as $entity) {
            $entityService->alignSemanticProperties($entity);
            $output->writeln([
                $entity->getId()
            ]);
        }

        $output->writeln([
            '<info>It\'s done!</info>'
        ]);
    }
}
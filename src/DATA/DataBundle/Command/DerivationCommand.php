<?php

namespace DATA\DataBundle\Command;

use DATA\DataBundle\Service\entity;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Routing\Annotation\Route;

class DerivationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('derivation:update')

            // the short description shown while running "php bin/console list"
            ->setDescription('Update derivation of all derived items')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'UPDATE DERIVATION',
            '============',
        ]);

        $entities = $this->getContainer()->get('data_data.entity')->find('all', ['enrichmentStatus' => 1], 'large');

        /** @var \DATA\DataBundle\Entity\Entity $entity */
        foreach($entities as $entity) {
            $output->writeln([
                $entity->getId()." > ".$entity->getWikidataSameAs()
            ]);
            foreach ($this->getContainer()->get('data_data.entity')->getProperties($entity, "wd") as $property) {
                $this->getContainer()->get('doctrine.orm.entity_manager')->remove($property);
            }
            $this->getContainer()->get('doctrine.orm.entity_manager')->flush();

            $this->getContainer()->get('data_data.entity')->derivation($entity, $entity->getWikidataSameAs(), null, $this->getContainer()->get('data_data.entity')->getTeachings($entity), true);
            $output->writeln([
                $entity->getId()
            ]);
        }

        $output->writeln([
            '<info>It\'s done!</info>'
        ]);
    }
}
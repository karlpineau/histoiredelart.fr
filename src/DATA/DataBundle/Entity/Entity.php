<?php

namespace DATA\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="DATA\ImportBundle\Entity\ImportSession", inversedBy="entities")
     * @ORM\JoinColumn(nullable=true)
     */
    private $importSession;

    /**
     * @var string
     *
     * @ORM\Column(name="importValidation", type="boolean", nullable=true)
     */
    private $importValidation;

    /**
     * @var \stdClass
     *
     * @ORM\OneToMany(targetEntity="DATA\DataBundle\Entity\EntityProperty", mappedBy="entity", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $properties;

    /**
     * Les valeurs vont de 0 à 1 :
     *  0 = Pas d'alignement fait avec Wikidata
     *  1 = Alignement Wikidata fait
     *
     * @var integer
     *
     * @ORM\Column(name="enrichmentStatus", type="integer", nullable=true)
     */
    protected $enrichmentStatus;

    /**
     * @var string
     * @Assert\Url()
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "Le champ 'wikidataSameAs' ne peut pas dépasser {{ limit }} caractères"
     * )
     *
     * @ORM\Column(name="wikidataSameAs", type="string", length=255, nullable=true)
     */
    protected $wikidataSameAs;

    /**
     * @ORM\ManyToOne(targetEntity="CAS\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $createUser;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="createDate", type="datetime", nullable=false)
     */
    protected $createDate;

    /**
     * @ORM\ManyToOne(targetEntity="CAS\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $updateUser;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updateDate", type="datetime", nullable=true)
     */
    protected $updateDate;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->properties = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     * @return Entity
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Entity
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set importSession
     *
     * @param \DATA\ImportBundle\Entity\ImportSession $importSession
     * @return Entity
     */
    public function setImportSession(\DATA\ImportBundle\Entity\ImportSession $importSession = null)
    {
        $this->importSession = $importSession;

        return $this;
    }

    /**
     * Get importSession
     *
     * @return \DATA\ImportBundle\Entity\ImportSession
     */
    public function getImportSession()
    {
        return $this->importSession;
    }

    /**
     * Set createUser
     *
     * @param \CAS\UserBundle\Entity\User $createUser
     * @return Entity
     */
    public function setCreateUser(\CAS\UserBundle\Entity\User $createUser = null)
    {
        $this->createUser = $createUser;

        return $this;
    }

    /**
     * Get createUser
     *
     * @return \CAS\UserBundle\Entity\User
     */
    public function getCreateUser()
    {
        return $this->createUser;
    }

    /**
     * Set updateUser
     *
     * @param \CAS\UserBundle\Entity\User $updateUser
     * @return Entity
     */
    public function setUpdateUser(\CAS\UserBundle\Entity\User $updateUser = null)
    {
        $this->updateUser = $updateUser;

        return $this;
    }

    /**
     * Get updateUser
     *
     * @return \CAS\UserBundle\Entity\User
     */
    public function getUpdateUser()
    {
        return $this->updateUser;
    }

    /**
     * Set importValidation
     *
     * @param boolean $importValidation
     * @return Entity
     */
    public function setImportValidation($importValidation)
    {
        $this->importValidation = $importValidation;

        return $this;
    }

    /**
     * Get importValidation
     *
     * @return boolean 
     */
    public function getImportValidation()
    {
        return $this->importValidation;
    }

    /**
     * Add properties
     *
     * @param \DATA\DataBundle\Entity\EntityProperty $properties
     * @return Entity
     */
    public function addProperty(\DATA\DataBundle\Entity\EntityProperty $properties)
    {
        $this->properties[] = $properties;

        return $this;
    }

    /**
     * Remove properties
     *
     * @param \DATA\DataBundle\Entity\EntityProperty $properties
     */
    public function removeProperty(\DATA\DataBundle\Entity\EntityProperty $properties)
    {
        $this->properties->removeElement($properties);
    }

    /**
     * Get properties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Set enrichmentStatus
     *
     * @param integer $enrichmentStatus
     * @return Entity
     */
    public function setEnrichmentStatus($enrichmentStatus)
    {
        $this->enrichmentStatus = $enrichmentStatus;

        return $this;
    }

    /**
     * Get enrichmentStatus
     *
     * @return string
     */
    public function getEnrichmentStatus()
    {
        return $this->enrichmentStatus;
    }

    /**
     * Set wikidataSameAs
     *
     * @param integer $wikidataSameAs
     * @return Entity
     */
    public function setWikidataSameAs($wikidataSameAs)
    {
        $this->wikidataSameAs = $wikidataSameAs;

        return $this;
    }

    /**
     * Get wikidataSameAs
     *
     * @return string
     */
    public function getWikidataSameAs()
    {
        return $this->wikidataSameAs;
    }
}

# Objectif :   
    {
        "values": [
            {
                "mainsnak": {
                    "value": "Q296955", 
                    "labels": {
                        "fr": {"language": "fr", "value": "peinture \u00e0 l'huile"}, 
                        "en": {"language": "en", "value": "oil paint"}}
                    }, 
                "qualifiers": null
            }, 
            {
                "mainsnak": {
                    "value": "Q4259259", 
                    "labels": {
                        "fr": {"language": "fr", "value": "toile"}, 
                        "en": {"language": "en", "value": "canvas"}
                    }
                }, 
                "qualifiers": {
                    "P518": [
                        {"hash": "741c2f726bfd42522b6ee31f850ec9ff7d4b7c41", "snaktype": "value", "property": "P518", "datatype": "wikibase-item", "datavalue": {"type": "wikibase-entityid", "value": {"id": "Q861259", "entity-type": "item", "numeric-id": 861259}}}
                    ]
                }
            }
        ], 
        "labels": {
            "fr": {"language": "fr", "value": "mat\u00e9riau"}, 
            "en": {"language": "en", "value": "material used"}
        }
    }
    
  
# Ce qu'on a au début :    
    [
        {
            "mainsnak":
            {
                "snaktype":"value",
                "property":"P186",
                "hash":"71b8c3a9aada5161bd30692f732bc347daba35cf",
                "datavalue": {
                    "value": {
                        "entity-type":"item",
                        "numeric-id":296955,
                        "id":"Q296955"
                    },
                    "type":"wikibase-entityid"
                },
                "datatype":"wikibase-item"
            },
            "type":"statement",
            "id":"q12418$B76F63CF-7E3D-435F-8694-7F743F494B71",
            "rank":"preferred"
        },{
            "mainsnak":{
                "snaktype":"value",
                "property":"P186",
                "hash":"bb839707f4056b5d4abcbac2d313de5ca8869a94",
                "datavalue":{
                    "value":{
                        "entity-type":"item",
                        "numeric-id":291034,
                        "id":"Q291034"
                    },
                    "type":"wikibase-entityid"
                },
                "datatype":"wikibase-item"
            },
            "type":"statement",
            "qualifiers":{
                "P518":[
                    {"snaktype":"value","property":"P518","hash":"741c2f726bfd42522b6ee31f850ec9ff7d4b7c41","datavalue":{"value":{"entity-type":"item","numeric-id":861259,"id":"Q861259"},"type":"wikibase-entityid"},"datatype":"wikibase-item"}
                ]
            },
            "qualifiers-order":["P518"],
            "id":"Q12418$053f412b-4541-92f8-ebba-f73c568f5c9b",
            "rank":"preferred"
        }
    ]

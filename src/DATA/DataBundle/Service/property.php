<?php

namespace DATA\DataBundle\Service;

use DATA\DataBundle\Entity\EntityProperty;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Yaml\Yaml;

class property
{
    protected $em;
    protected $logger;

    public function __construct(EntityManager $EntityManager, LoggerInterface $logger)
    {
        $this->em = $EntityManager;
        $this->logger = $logger;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Fonction systeme
     *
     * @param $array array
     * @return bool
     */
    protected function has_string_keys($array) {
        if(gettype($array) == "object") {return true;}
        elseif(gettype($array) == "array") {return count(array_filter(array_keys($array), 'is_string')) > 0;}
        else {return false;}
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : retourne le label d'une EntityProperty en utilisant la config ou le contenu
     *           (e.g. le label de P31 -> "Nature de l'élément")
     * METHODE :
     *  1. Regarde dans le fichier de config si la propriété est présente et si un label lui a été attribué
     *      1.b -> Si oui, elle retourne cette valeur
     *  2. Si la clé "labels" existe dans EntityProperty->getValue(), elle retourne la valeur depuis cet endroit
     *  3. Sinon elle retourne null
     *
     * @param $property EntityProperty
     * @param null|string $defaultLanguage
     * @return array|string|null
     */
    public function getPropertyLabelFromEntityProperty($property, $defaultLanguage=null) {
        /* Label management */
        $propertyValue = json_decode($property->getValue());
        $label = null;

        if(strpos($property->getProperty(), '-') !== false and array_key_exists(explode('-', $property->getProperty())[1], $this->getProperties())) {
            $label = $this->getProperties()[explode('-', $property->getProperty())[1]]["label"];
        } elseif(property_exists($propertyValue, "labels")) {
            if($defaultLanguage != null and property_exists($propertyValue->labels, $defaultLanguage)) {
                $label = $propertyValue->labels->{$defaultLanguage}->value;
            }
        }

        return $label;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Extrait les littéraux depuis $entityProperty->getValue()
     *
     * @param $property EntityProperty
     * @param null|string $defaultLanguage
     * @return string|null
     *
     */
    public function getLitteralFromPropertyValue($property, $defaultLanguage=null) {
        $propertyValue = json_decode($property->getValue());

        if(key_exists("values", $propertyValue)) {
            foreach($propertyValue->values as $claim) {
                if ($defaultLanguage != null and property_exists($claim, 'mainsnak')) {
                    if(gettype($claim->mainsnak) == 'string') {
                        return $claim->mainsnak;
                    } elseif (key_exists("labels", $claim->mainsnak) and key_exists($defaultLanguage, $claim->mainsnak->labels)) {
                        return $claim->mainsnak->labels->{$defaultLanguage}->value;
                    } elseif (key_exists("text", $claim->mainsnak)) {
                        return $claim->mainsnak->text;
                    }
                }
            }
        } else {
            foreach($propertyValue as $claim) {
                if($defaultLanguage != null and $claim->language == $defaultLanguage) {
                    return $claim->value;
                }
            }
        }

        return null;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Liste les propriétés compatibles avec Clichés! ainsi que tous les attributs nécessaires
     *
     * @return array
     */
    public function getProperties() {
        $configProperties = Yaml::parseFile(__DIR__ . "/../../../../web/properties.yml");
        return $configProperties['properties'];
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Liste les propriétés compatibles avec Clichés! ainsi que tous les attributs nécessaires
     *
     * @return array
     */
    public function getGroupProperties() {
        $configProperties = Yaml::parseFile(__DIR__ . "/../../../../web/properties.yml");
        return $configProperties['groups'];
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Génère une "value" correctement structurée pour EntityProperty
     *
     * Pour le modèle de données, voir README.md
     *
     * @param string $property Doit être exprimé sans préfixe (hda ou wd)
     * @param array $values
     * @param string $language
     * @return array
     */
    public function generatePropertyValue($property, $values, $language="fr") {
        if($property == "label") {return $this->generatePropertyLabelValue($values, $language);}
        $propertyInfo = $this->getProperties()[$property];

        $valuesI = [];
        foreach ($values as $value) {
            $valuesI[] = [
                "mainsnak" => $value
            ];
        }

        return [
            "values" => $valuesI,
            "labels" => [
                "fr" => ["language" => $language, "value" => $propertyInfo["label"]]
            ],
            "type" => "string"
        ];
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Génère une "value" correctement structurée pour EntityProperty, dans le cas où c'est un label
     *
     *
     * @param array $values
     * @param string $language
     * @return array
     */
    public function generatePropertyLabelValue($values, $language="fr") {
        $mainArray = [];
        foreach ($values as $value) {
            $mainArray[] = ["language" => $language, "value" => $value];
        }
        return $mainArray;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Concatène les values pour indexation
     *
     * @param EntityProperty $property
     * @return string
     */
    public function aggregatePropertyValue($property) {
        $propertyValue = json_decode($property->getValue());
        $returnedString = "";

        if(array_key_exists("values", $propertyValue)) {
            foreach($propertyValue->values as $claim) {
                if(array_key_exists("mainsnak", $claim) and $claim->mainsnak != null) {
                    if (array_key_exists("mainsnak", $claim) and gettype($claim->mainsnak) == 'string') {
                        $returnedString .= " ".$claim->mainsnak;
                    } elseif (array_key_exists("mainsnak", $claim) and array_key_exists("labels", $claim->mainsnak)) {
                        foreach ($claim->mainsnak->labels as $lg) {
                            $returnedString .= " ".$lg->value;
                        }
                    } elseif (array_key_exists("mainsnak", $claim) and array_key_exists("text", $claim->mainsnak)) {
                        $returnedString .= " ".$claim->mainsnak->text;
                    } elseif (array_key_exists("mainsnak", $claim) and array_key_exists("time", $claim->mainsnak)) {
                        $returnedString .= " ".$claim->mainsnak->time;
                    }
                }
            }
        } else if(strpos($property->getProperty(), "label") != false or strpos($property->getProperty(), "description") != false or strpos($property->getProperty(), "aliases") != false) {
            foreach($propertyValue as $claim) {
                $returnedString .= " ".$claim->value;
            }
        }

        return $returnedString;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne la précision d'une date
     *
     * @param EntityProperty $entityProperty
     * @return mixed
     */
    public function getTimePrecision($entityProperty) {
        $precisions = [];

        foreach(json_decode($entityProperty->getValue())->values as $claim) {
            if (array_key_exists("mainsnak", $claim) and array_key_exists("time", $claim->mainsnak)) {
                $precisions[] = " ".$claim->mainsnak->precision;
            }
        }
        return $precisions;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne la précision d'une quantité au format chaine de caractère
     *
     * QUESTION : Est-ce qu'il faut aussi le modifier en JS quelque part ?
     *
     * @param string $unit
     * @return string
     */
    public function getQuantityPrecision($unit) {
        switch ($unit) {
            case "http://www.wikidata.org/entity/Q174789":
                return "millimètre(s)";
            case "http://www.wikidata.org/entity/Q174728":
                return "centimètre(s)";
            case "http://www.wikidata.org/entity/Q11573":
                return "mètre(s)";
            case "http://www.wikidata.org/entity/Q11570":
                return "kilo(s)";
            case "http://www.wikidata.org/entity/Q191118":
                return "tonne(s)";
            case "http://www.wikidata.org/entity/Q218593":
                return "pouce(s)";
            default:
                return $unit;
        }
    }
}
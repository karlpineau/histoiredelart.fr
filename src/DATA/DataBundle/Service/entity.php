<?php

namespace DATA\DataBundle\Service;

use CAS\UserBundle\Entity\Favorite;
use CLICHES\PlayerBundle\Entity\PlayerProposalChoiceValue;
use DATA\DataBundle\Entity\EntityProperty;
use DATA\DataBundle\Entity\Pad;
use DATA\DataBundle\Entity\Source;
use DATA\ImageBundle\Entity\Image;
use DATA\ImageBundle\Entity\View;
use DATA\ImageBundle\Entity\ViewProperty;
use DATA\PublicBundle\Entity\Reporting;
use DATA\PublicBundle\Entity\Visit;
use DATA\SearchBundle\Entity\SearchIndex;
use DATA\TeachingBundle\Entity\Teaching;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Psr\Log\Test\LoggerInterfaceTest;

class entity 
{
    protected $em;
    protected $view;
    protected $property;
    protected $wikidata;
    protected $image;
    protected $logger;

    public function __construct(EntityManager $EntityManager, \DATA\ImageBundle\Service\view $view, property $property, wikidata $wikidata, \DATA\ImageBundle\Service\image $image, LoggerInterface $logger)
    {
        $this->em = $EntityManager;
        $this->view = $view;
        $this->property = $property;
        $this->wikidata = $wikidata;
        $this->image = $image;
        $this->logger = $logger;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------------------------------------------- */
    /* -------------------------------------------- Find                --------------------------------------------- */
    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Finder de View
     *
     * @param null|string $number
     * @param null|array $properties
     * @param null|string $scope
     * @return array|bool|null|object
     *
     *
     * $number -> "one"|"all"|"array"
     * $scope :
     *  restrict = uniquement les entités validées
     */
    public function find($number=null, $properties=null, $scope=null) {
        $repositoryEntity = $this->em->getRepository('DATADataBundle:Entity');

        if($scope == 'restrict') {$properties['importValidation'] = true;}
        if($properties == null) {$properties = [];}

        if($number == 'all' OR $number == 'array') {
            return $repositoryEntity->findBy($properties);
        }

        elseif($number == 'one') {
            return $repositoryEntity->findOneBy($properties);
        }

        else {
            return false;
        }
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------------------------------------------- */
    /* -------------------------------------------- GET BY              --------------------------------------------- */
    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne une Entity via sa View
     *
     * @param $view View
     * @return mixed
     */
    public function getByView($view) {
        if(!empty($view->getEntity())) {return $view->getEntity();}
        else {return null;}
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne les Entity d'un Teaching
     *
     * @param Teaching $teaching
     * @param null|bool $addNotValidatedEntities
     * @param null|bool $addExcludedViews
     * @return array|Collection
     */
    public function getByTeaching($teaching, $addNotValidatedEntities=null, $addExcludedViews=null) {
        $views = $teaching->getViews();

        $entities = [];
        /** @var View $view */
        foreach($views as $view) {
            $addBool = true;

            if($addNotValidatedEntities == false and $view->getEntity()->getImportValidation() == false) {
                $addBool = false;
            }

            if($addExcludedViews == false and $this->view->isExcludedView($view) == true) {
                $addBool = false;
            }

            if($addBool == true and !in_array($view->getEntity(), $entities)) {
                $entities[] = $view->getEntity();
            }
        }

        return $entities;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne une Entity par son ID
     *
     * @param $entityId integer
     * @return array|bool|null|object
     */
    public function getById($entityId) {
        return $this->find('one', ['id' => $entityId], 'large');
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------------------------------------------- */
    /* -------------------------------------------- GET INFORMATION     --------------------------------------------- */
    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne le label d'un EntityProperty pour une Entity
     *
     * @param \DATA\DataBundle\Entity\Entity $entity
     * @param string $property
     * @param string $language
     * @return mixed|string|null
     *
     * @throws \Exception
     */
    public function get($entity, $property, $language='fr') {
        if($property == 'P571') {return $this->getDateOfCreation($entity);}
        elseif($property == 'creator') {return $this->getCreator($entity, $language);}

        if($entity->getEnrichmentStatus() == 1) {
            /** @var EntityProperty $entityProperty */
            $entityProperty = $this->em->getRepository('DATADataBundle:EntityProperty')->findOneBy(['entity' => $entity, 'property' => 'wd-'.$property]);
        } else {
            /** @var EntityProperty $entityProperty */
            $entityProperty = $this->em->getRepository('DATADataBundle:EntityProperty')->findOneBy(['entity' => $entity, 'property' => 'hda-'.$property]);
        }

        if($entityProperty != null) {
            return $this->property->getLitteralFromPropertyValue($entityProperty, $language);
        }

        return null;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne l'auteur d'une entité. Distinct de GET() car choisit entre des propriétés
     *
     * @param \DATA\DataBundle\Entity\Entity $entity
     * @param string $language
     * @return mixed|string|null
     *
     * @throws \Exception
     */
    public function getCreator($entity, $language='fr') {
        $entityProperty = null;
        $prefix = "hda";
        if($entity->getEnrichmentStatus() == 1) {
            $prefix = "wd";
        }

        if($this->isPropertyExist($entity, $prefix.'-P170')) {
            /** @var EntityProperty $entityProperty */
            $entityProperty = $this->em->getRepository('DATADataBundle:EntityProperty')->findOneBy(['entity' => $entity, 'property' => $prefix.'-P170']);
        } elseif($this->isPropertyExist($entity, $prefix.'-P84')) {
            /** @var EntityProperty $entityProperty */
            $entityProperty = $this->em->getRepository('DATADataBundle:EntityProperty')->findOneBy(['entity' => $entity, 'property' => $prefix.'-84']);
        } elseif($this->isPropertyExist($entity, $prefix.'-P50')) {
            /** @var EntityProperty $entityProperty */
            $entityProperty = $this->em->getRepository('DATADataBundle:EntityProperty')->findOneBy(['entity' => $entity, 'property' => $prefix.'-50']);
        }

        if($entityProperty != null) {
            return $this->property->getLitteralFromPropertyValue($entityProperty, $language);
        }

        return null;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne la date de création
     *
     * @param $entity \DATA\DataBundle\Entity\Entity
     * @return mixed|null
     * @throws \Exception
     */
    public function getDateOfCreation($entity) {
        if($entity->getEnrichmentStatus() == 1) {
            $property = 'wd-P571';
        } else {
            $property = 'hda-P571';
        }

        /** @var EntityProperty $date_entityProperty */
        $date_entityProperty = $this->em->getRepository('DATADataBundle:EntityProperty')->findOneBy(['entity' => $entity, 'property' => $property]);

        $stringToReturn = "";
        if($date_entityProperty != null) {
            $date_entityPropertyValue = json_decode($date_entityProperty->getValue());
            foreach($date_entityPropertyValue->values as $value) {
                if(key_exists('mainsnak', $value) and key_exists('type', $date_entityPropertyValue) and $date_entityPropertyValue->type == "time") {
                    if($stringToReturn != "") {$stringToReturn .= ", ";}
                    $stringToReturn .= $this->wikidata->computeTime($value->mainsnak);
                } elseif(key_exists('mainsnak', $value) and key_exists('type', $date_entityPropertyValue) and $date_entityPropertyValue->type == "string") {
                    if($stringToReturn != "") {$stringToReturn .= ", ";}
                    $stringToReturn .= $value->mainsnak;
                }
            }
            return $stringToReturn;
        }

        return null;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Requête les excerpt Wikipedia au sein de DATA
     *
     * @param $entity
     * @param string $language
     * @return object|null
     */
    public function getWikipedia($entity, $language='fr') {
        if($language=='fr') {
            /** @var EntityProperty $date_entityProperty */
            return $this->em->getRepository('DATADataBundle:EntityProperty')->findOneBy(['entity' => $entity, 'property' => 'wd-wikipedia-frwiki']);
        } elseif($language=='en') {
            /** @var EntityProperty $date_entityProperty */
            return $this->em->getRepository('DATADataBundle:EntityProperty')->findOneBy(['entity' => $entity, 'property' => 'wd-wikipedia-enwiki']);
        } else {
            return null;
        }

    }

    /**
     * @param \DATA\DataBundle\Entity\Entity $entity
     * @return array
     */
    public function getDuplicates($entity) {
        $duplicates = array();
        if($entity->getWikidataSameAs() != null) {
            $duplicatesI = $this->find('all', ["wikidataSameAs" => $entity->getWikidataSameAs()]);

            foreach ($duplicatesI as $duplicateI) {
                if($duplicateI != $entity) {
                    $duplicates[] = $duplicateI;
                }
            }
        }

        return $duplicates;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------------------------------------------- */
    /* -------------------------------------------- GET MODULES         --------------------------------------------- */
    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne les Views d'une Entity
     *
     * @param $entity \DATA\DataBundle\Entity\Entity
     * @return array
     */
    public function getViews($entity) {
        return $this->em->getRepository('DATAImageBundle:View')->findBy(['entity' => $entity], ['orderView' => 'ASC']);
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne la vue la plus haute de la liste des vues
     *
     * @param $entity \DATA\DataBundle\Entity\Entity
     * @return array
     */
    public function getMainView($entity) {
        $views = $this->em->getRepository('DATAImageBundle:View')->findBy(['entity' => $entity], ['orderView' => 'ASC']);

        if(count($views) > 0) {
            return $views[0];
        }

        return null;
    }


    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne les Views d'une Entity, avec leurs proposals
     *
     * @param $entity \DATA\DataBundle\Entity\Entity
     * @return array
     */
    public function getViewsForEntityAdmin($entity) {
        $views = $this->getViews($entity);
        $viewArray = array();
        foreach ($views as $view) {
            $viewArray[] = ['view' => $view,
                'teachings' => $this->view->getTeachingsAdminForView($view),
                'proposals' => $this->em->getRepository('CLICHESPlayerBundle:PlayerOeuvre')->findBy(['view' => $view])];
        }

        return $viewArray;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne les Teachings associés aux Views d'une Entity
     *
     * @param $entity \DATA\DataBundle\Entity\Entity
     * @return Teaching[]
     */
    public function getTeachings($entity) {
        $teachings = array();
        $views = $this->getViews($entity);

        /** @var View $view */
        foreach($views as $view) {
            foreach ($view->getTeachings() as $teaching) {
                if(!in_array($teaching, $teachings)) {
                    $teachings[] = $teaching;
                }
            }
        }

        return $teachings;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne les Sources d'une Entity
     *
     * @param $entity \DATA\DataBundle\Entity\Entity
     * @return mixed
     */
    public function getSources($entity) {
        return $this->em->getRepository('DATADataBundle:Source')->findBy(['entity' => $entity]);
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Compte le nombre de EntityProperty associées à une Entity
     *
     * @param \DATA\DataBundle\Entity\Entity $entity
     * @return int
     */
    public function countProperties($entity) {
        return count($this->em->getRepository('DATADataBundle:EntityProperty')->findBy(['entity' => $entity]));
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Sert à retourner les EntityProperty d'une entity
     *
     * @param $entity \DATA\DataBundle\Entity\Entity
     * @param null|string $prefix
     * @param null|bool $sort Si la valeur est "true", on trie les propriétés dans l'ordre le plus naturel pour l'utilisateur
     * @return EntityProperty[]
     */
    public function getProperties($entity, $prefix=null, $sort=null) {
        $properties = $this->em->getRepository('DATADataBundle:EntityProperty')->findBy(['entity' => $entity]);
        $propertiesInfo = $this->property->getProperties();
        $groupGropertiesInfo = $this->property->getGroupProperties();

        if($prefix != null) {
            $selection = [];
            /** @var $property EntityProperty */
            foreach ($properties as $key => $property) {
                if (strpos($property->getProperty(), $prefix) !== false) {
                    $orderProperty = null;
                    $propertyId = explode('-', $property->getProperty())[1];

                    if(key_exists($propertyId, $propertiesInfo)) {
                        if($propertiesInfo[$propertyId]['displayOrder'] == "false") {
                            $orderProperty = null;
                        } else {
                            $orderProperty = intval(strval($groupGropertiesInfo[$propertiesInfo[$propertyId]["displayGroup"]]).strval($propertiesInfo[$propertyId]['displayOrder']));
                        }
                    } else {
                        $orderProperty = count($properties)+$key;
                    }

                    if($orderProperty !== null) {
                        $selection[$orderProperty] = $property;
                    }
                }
            }
            if($sort == true) {
                ksort($selection);
            }
            return $selection;
        }

        return $properties;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Formatte selon les besoins de Cliches
     *
     * @param \DATA\DataBundle\Entity\Entity $entity
     * @return array
     */
    public function getPropertiesForCliches($entity) {
        $propertyPrefix = 'hda';
        if($entity->getEnrichmentStatus() == 1) {$propertyPrefix = 'wd';}

        $properties = $this->getProperties($entity, $propertyPrefix, true);
        $propertiesForCliches = $this->property->getProperties();

        $data = array();

        foreach ($properties as $property) {
            if(key_exists(explode('-', $property->getProperty())[1], $propertiesForCliches) and $propertiesForCliches[explode('-', $property->getProperty())[1]]['displayCliches'] == "true") {
                array_push($data, [
                    'field' => $property->getProperty(),
                    'label' => $propertiesForCliches[explode('-', $property->getProperty())[1]]["label"],
                    'value' => $property->getValue()]);
            }
        }

        return $data;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Test l'existence d'un couple Entity / Property
     *
     * @param \DATA\DataBundle\Entity\Entity $entity
     * @param string $property
     * @return bool|mixed
     */
    public function isPropertyExist($entity, $property)
    {
        if($this->em->getRepository('DATADataBundle:EntityProperty')->findOneBy(['entity' => $entity, 'property' => $property]) != null) return true;
        else return false;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------------------------------------------- */
    /* -------------------------------------------- ENTITY ACTIONS      --------------------------------------------- */
    /**
     * @param $entity \DATA\DataBundle\Entity\Entity
     * @param null|\DATA\DataBundle\Entity\Entity $left
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeEntity($entity, $left=null) {
        /*
         * La présence d'un left entraine une fusion et non une suppression
         *
         * Liste des modules à supprimer pour une entité :
         *      - Entity <- Check
         *      - Views <- Check
         *      - Sources <- Chech
         *      - Pad <- Check
         *      - Visit <- Check
         *      - Reporting <- Check
         *      - Data favorite <- Check
         *      - PlayerProposalChoiceValue <- Check
         *      - SearchIndex
         */

        /** @var PlayerProposalChoiceValue $playerProposalChoiceValue */
        foreach($this->em->getRepository('CLICHESPlayerBundle:PlayerProposalChoiceValue')->findBy(['entity' => $entity]) as $playerProposalChoiceValue) {
            if($left != null) {$playerProposalChoiceValue->setEntity($left); $this->em->persist($playerProposalChoiceValue);}
            else {$playerProposalChoiceValue->setEntity(null); $this->em->persist($playerProposalChoiceValue);}
        }
        /** @var View $view */
        foreach ($this->getViews($entity) as $view) {
            if($left != null) {$view->setEntity($left); $this->em->persist($view);}
            else {$this->view->deleteView($view);}
        }
        /** @var Source $source */
        foreach($this->getSources($entity) as $source) {
            if($left != null) {$source->setEntity($left); $this->em->persist($source);}
            else {$this->em->remove($source);}
        }
        /** @var Reporting $reporting */
        foreach ($this->em->getRepository('DATAPublicBundle:Reporting')->findBy(['entity' => $entity]) as $reporting) {
            if($left != null) {$reporting->setEntity($left); $this->em->persist($reporting);}
            else {$this->em->remove($reporting);}
        }
        /** @var Visit $visit */
        foreach ($this->em->getRepository('DATAPublicBundle:Visit')->findBy(['entity' => $entity]) as $visit) {
            if($left != null) {$visit->setEntity($left); $this->em->persist($visit);}
            $this->em->remove($visit);
        }
        /** @var Pad $pad */
        foreach ($this->em->getRepository('DATADataBundle:Pad')->findBy(['entity' => $entity]) as $pad) {
            if($left != null and $this->em->getRepository('DATADataBundle:Pad')->findOneBy(['entity' => $left]) == null) {
                //Si LEFT a déjà un pad, on supprime celui de right, sinon on fusionne
                $pad->setEntity($left);$this->em->persist($pad);
            } else {
                $this->em->remove($pad);
            }
        }
        /** @var SearchIndex $searchIndex */
        foreach ($this->em->getRepository('DATASearchBundle:SearchIndex')->findBy(['entity' => $entity]) as $searchIndex) {
            if($left != null and $this->em->getRepository('DATASearchBundle:SearchIndex')->findOneBy(['entity' => $left]) == null) {
                //Si LEFT a déjà un pad, on supprime celui de right, sinon on fusionne
                $searchIndex->setEntity($left);$this->em->persist($searchIndex);
            } else {
                $this->em->remove($searchIndex);
            }
        }
        /** @var Favorite $favorite */
        foreach ($this->em->getRepository('CASUserBundle:Favorite')->findBy(['entity' => $entity]) as $favorite) {
            if($left != null and $this->em->getRepository('CASUserBundle:Favorite')->findOneBy(['entity' => $entity, 'user' => $favorite->getUser()]) == null) {
                $favorite->setEntity($left);
                $this->em->persist($favorite);
            } else {
                $this->em->remove($favorite);
            }
        }

        $this->em->remove($entity);
        $this->em->flush();
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne un titre pour une Image (calculé en fonction de l'Entity)
     *
     * @param \DATA\ImageBundle\Entity\Image $image
     * @return mixed|string|null
     */
    public function getTitleForImage($image)
    {
        $title = "";

        if($image->getView() != null) {
            if (!empty($this->get($this->getByView($image->getView()), 'label'))) {
                $title = $this->get($this->getByView($image->getView()), 'label');
            }
        }

        return $title;
    }

    /**
     * @param \DATA\DataBundle\Entity\Entity $entity
     * @param string $uri
     * @param mixed $user
     * @param Teaching[] $teachings
     * @param bool $ignoreImage
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ImagickException
     */
    public function derivation($entity, $uri, $user, $teachings, $ignoreImage=false) {
        $requiredProperties = $this->property->getProperties();
        $requiredLanguages = $this->wikidata->getListOfLanguages();
        $uri = preg_replace('/\s/', '', $uri);
        $wikidataEntity = $this->wikidata->getEntity($uri);

        $entity->setEnrichmentStatus(1);

        /* Claims management */
        foreach($wikidataEntity->claims as $claim_id => $claim_content) {
            if(key_exists($claim_id, $requiredProperties) and key_exists("isImage", $requiredProperties[$claim_id]) and $requiredProperties[$claim_id]['isImage'] == "true" and $ignoreImage == false) {
                foreach($claim_content as $claim_iteration) {
                    $url = $this->wikidata->getImageFromClaim($claim_iteration);
                    $imageSet = $this->image->setImage($url, 'download');

                    $view = new View();
                    $view->setEntity($entity);
                    $view->setCreateUser($user);

                    $viewProperty = new ViewProperty();
                    $viewProperty->setValue($claim_id);
                    $viewProperty->setProperty("hda-imageType");
                    $viewProperty->setView($view);
                    $viewProperty->setCreateUser($user);
                    $this->em->persist($viewProperty);

                    $image = new Image();
                    $image->setOriginalFileName($imageSet[0]);
                    $image->setSmallFileName($imageSet[1] . "_small.jpg");
                    $image->setMediumFileName($imageSet[1] . "_medium.jpg");
                    $image->setLargeFileName($imageSet[1] . "_large.jpg");
                    $image->setCopyright('Wikimedia Commons');
                    $image->setCreateUser($user);
                    $image->setView($view);

                    /* -- Ajout de l'enseignement-- */
                    foreach ($teachings as $teaching) {
                        $view->addTeaching($teaching);
                    }

                    $this->em->persist($image);
                    $this->em->persist($view);
                }
            } elseif(key_exists($claim_id, $requiredProperties) and (!key_exists("isImage", $requiredProperties[$claim_id]) or (key_exists("isImage", $requiredProperties[$claim_id]) and $requiredProperties[$claim_id]['isImage'] != "true"))) {
                $entityProperty = new EntityProperty();
                $entityProperty->setValue(json_encode($this->wikidata->cleanClaim($claim_content)));
                $entityProperty->setProperty("wd-".$claim_id);
                $entityProperty->setEntity($entity);
                $entityProperty->setCreateUser($user);
                $this->em->persist($entityProperty);
            }
        }

        /* Label management */
        $labelArray = [];
        foreach($wikidataEntity->labels as $label_lg => $label_content) {
            if(in_array($label_lg, $requiredLanguages)) {
                $labelArray[] = $label_content;
            }
        }
        if(count($labelArray) > 0) {
            $entityProperty = new EntityProperty();
            $entityProperty->setValue(json_encode($labelArray));
            $entityProperty->setProperty("wd-label");
            $entityProperty->setEntity($entity);
            $entityProperty->setCreateUser($user);
            $this->em->persist($entityProperty);
        }

        /* Description management */
        $descriptionArray = [];
        foreach($wikidataEntity->descriptions as $description_lg => $description_content) {
            if(in_array($description_lg, $requiredLanguages)) {
                $descriptionArray[] = $description_content;
            }
        }
        if(count($descriptionArray) > 0) {
            $entityProperty = new EntityProperty();
            $entityProperty->setValue(json_encode($descriptionArray));
            $entityProperty->setProperty("wd-description");
            $entityProperty->setEntity($entity);
            $entityProperty->setCreateUser($user);
            $this->em->persist($entityProperty);
        }

        /* Aliases management */
        $aliasesArray = [];
        foreach($wikidataEntity->aliases as $aliases_lg => $aliases_content) {
            if(in_array($aliases_lg, $requiredLanguages)) {
                $aliasesArray[] = $aliases_content;
            }
        }

        $aliasesArrayM = array();
        foreach ($aliasesArray as $arraySub) {
            $aliasesArrayM = array_merge($aliasesArrayM, $arraySub);
        }

        if(count($aliasesArrayM) > 0) {
            $entityProperty = new EntityProperty();
            $entityProperty->setValue(json_encode($aliasesArrayM));
            $entityProperty->setProperty("wd-aliases");
            $entityProperty->setEntity($entity);
            $entityProperty->setCreateUser($user);
            $this->em->persist($entityProperty);
        }

        /* Wikipedia management */
        $wpArray = [];
        foreach($wikidataEntity->sitelinks as $site_name => $site_meta) {
            if(in_array($site_name, ["enwiki", "frwiki"])) {
                $wpArray[] = $site_meta;
            }
        }
        if(count($wpArray) > 0) {
            $entityProperty = new EntityProperty();
            $entityProperty->setValue(json_encode($wpArray));
            $entityProperty->setProperty("wd-sitelinks");
            $entityProperty->setEntity($entity);
            $entityProperty->setCreateUser($user);
            $this->em->persist($entityProperty);

            foreach($wpArray as $siteLink) {
                $wpContent = $this->wikidata->getWikipedia($siteLink);

                if($wpContent != null) {
                    $entityProperty = new EntityProperty();
                    $entityProperty->setValue(json_encode($wpContent));
                    $entityProperty->setProperty("wd-wikipedia-".$siteLink->site);
                    $entityProperty->setEntity($entity);
                    $entityProperty->setCreateUser($user);
                    $this->em->persist($entityProperty);
                }
            }
        }

        $result[] = ["entity" => $entity->getId(), "status" => "insert"];
        $this->em->flush();

        return $result;
    }

    /**
     * @param \DATA\DataBundle\Entity\Entity $entity
     * @throws \Doctrine\ORM\ORMException
     */
    public function removeDerivation($entity) {
        foreach ($this->getProperties($entity, "wd") as $property) {
            $this->em->remove($property);
        }
        $entity->setEnrichmentStatus(0);
        $this->em->flush();
    }

}

<?php

namespace DATA\DataBundle\Service;

use Doctrine\ORM\EntityManager;

class entityRouting
{
    protected $em;

    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }

    /**
     * @return string
     */
    public function getRoutingView() {return 'data_data_entity_view';}

    /**
     * @return string
     */
    public function getRoutingPublicView() {return 'data_public_entity_view';}

    /**
     * @return string
     */
    public function getRoutingDelete() {return 'data_data_entity_delete';}
}

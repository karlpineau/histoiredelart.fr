<?php

namespace DATA\DataBundle\Service;

use Doctrine\ORM\EntityManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class twig extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('json_decode', [$this, 'jsonDecode']),
            new TwigFilter('shuffle', [$this, 'shuffle']),
        ];
    }

    public function jsonDecode($string)
    {
        return json_decode($string);
    }

    public function shuffle($array)
    {
        $toShuffle = (array) $array;
        return shuffle($toShuffle);
    }
}
<?php

namespace DATA\DataBundle\Service;

use DATA\DataBundle\Service\entity;
use DATA\TeachingBundle\Service\teaching;
use Doctrine\ORM\EntityManager;

class merge
{
    protected $em;
    protected $teaching;
    protected $entity;

    public function __construct(EntityManager $EntityManager, teaching $teaching, entity $entity)
    {
        $this->em = $EntityManager;
        $this->teaching = $teaching;
        $this->entity = $entity;
    }

    /**
     * @param $entityLeft_id int
     * @param $entityRight_id int
     * @return array|bool|null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function mergeDuplicates($entityLeft_id, $entityRight_id)
    {
        /** @var \DATA\DataBundle\Entity\Entity $right */
        $right = $this->entity->getById($entityRight_id);
        if ($right === null) { throw $this->createNotFoundException('Erreur : au moins une entité inexistante.'); }

        /** @var \DATA\DataBundle\Entity\Entity $left */
        $left = $this->entity->getById($entityLeft_id);
        if ($left === null) { throw $this->createNotFoundException('Erreur : au moins une entité inexistante.'); }

        $this->entity->removeEntity($right, $left);

        return $left;
    }
}

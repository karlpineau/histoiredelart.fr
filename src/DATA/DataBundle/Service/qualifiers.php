<?php

namespace DATA\DataBundle\Service;

use DATA\DataBundle\Entity\EntityProperty;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Yaml\Yaml;

class qualifiers
{
    protected $em;
    protected $logger;

    public function __construct(EntityManager $EntityManager, LoggerInterface $logger)
    {
        $this->em = $EntityManager;
        $this->logger = $logger;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Fonction systeme
     *
     * @param $array array
     * @return bool
     */
    protected function has_string_keys($array) {
        if(gettype($array) == "object") {return true;}
        elseif(gettype($array) == "array") {return count(array_filter(array_keys($array), 'is_string')) > 0;}
        else {return false;}
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Liste les qualificatifs compatibles avec Clichés! ainsi que tous les attributs nécessaires
     *
     * @return array
     */
    public function getQualifiers() {
        $configQualifiers = Yaml::parseFile(__DIR__ . "/../../../../web/qualifiers.yml");
        return $configQualifiers['qualifiers'];
    }
}
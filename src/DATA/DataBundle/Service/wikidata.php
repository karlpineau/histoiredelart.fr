<?php

namespace DATA\DataBundle\Service;

use Doctrine\ORM\EntityManager;
use phpDocumentor\Reflection\Types\Integer;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

class wikidata
{
    protected $em;
    protected $request;
    protected $logger;

    public function __construct(EntityManager $EntityManager, LoggerInterface $logger)
    {
        $this->em = $EntityManager;
        $this->logger = $logger;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne le QWD d'une URL ou URI Wikidata
     *
     * @param $url string
     * @return mixed|null
     */
    public function getQwd($url) {
        $qwd = null;

        if(preg_match('/https:\/\/www.wikidata.org\/entity\//', $url)) {
            $qwd = str_replace('https://www.wikidata.org/entity/', "", $url);
        } elseif(preg_match('/http:\/\/www.wikidata.org\/entity\//', $url)) {
            $qwd = str_replace('http://www.wikidata.org/entity/', "", $url);
        } elseif(preg_match('/https:\/\/www.wikidata.org\/wiki\//', $url)) {
            $qwd = str_replace('https://www.wikidata.org/wiki/', "", $url);
        } elseif(preg_match('/http:\/\/www.wikidata.org\/wiki\//', $url)) {
            $qwd = str_replace("http://www.wikidata.org/wiki/", "", $url);
        }
        
        return $qwd;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Télécharge les données d'une entité Wikidata depuis son identifiant
     *
     * @param $qwd string
     * @return mixed
     */
    public function getEntity($qwd) {
        if(strpos($qwd, 'wikidata.org')) {
            $qwd = str_replace('https://www.wikidata.org/wiki/', '', $qwd);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.wikidata.org/w/api.php?action=wbgetentities&ids='.urlencode($qwd).'&format=json');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        return json_decode($response)->entities->{$qwd};
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne l'excerpt d'un article Wikipédia relié à une entité Wikidata
     *
     * URL d'API appelée : https://fr.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exsentences=10&titles=La%20Bella%20(Titien)
     *
     * @param $siteLink object
     * @return mixed
     */
    public function getWikipedia($siteLink) {
        if($siteLink->site == "enwiki") {
            $lg = 'en';
        } elseif($siteLink->site == "frwiki") {
            $lg = 'fr';
        }


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://'.urlencode($lg).'.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exsentences=5&titles='.urlencode($siteLink->title));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        // Normalement, le foreach ne contient qu'une page
        foreach (json_decode($response)->query->pages as $page) {
            return $page;
        }

        return null;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Indique la liste des langues pour lesquelles il faut télécharger les labels et les littéraux
     *
     * @return array
     */
    public function getListOfLanguages() {
        return [
            "fr",
            "en",
        ];
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Formate les DataValue selon leur type
     *
     * @param object $datavalue
     * @return array
     */
    public function formatDataValue($datavalue) {
        $mainsnak = null;
        $type = null;

        if($datavalue->type == 'wikibase-entityid') {
            $qwd = $datavalue->value->id;
            $qwd_item = $this->getEntity($qwd);
            $qwd_labels = $this->getLabelFromEntity($qwd_item);

            $mainsnak = [
                "value" => $qwd,
                "labels" => $qwd_labels
            ];
            $type = "entity";
        } elseif($datavalue->type == 'time') {
            $mainsnak = $datavalue->value;
            $type = "time";
        } elseif($datavalue->type == 'string') {
            $mainsnak = $datavalue->value;
            $type = "string";
        } elseif($datavalue->type == 'quantity') {
            $mainsnak = $datavalue->value;
            $type = "quantity";
        } elseif($datavalue->type == 'monolingualtext') {
            $mainsnak = $datavalue->value;
            $type = "monolingualtext";
        }

        return [$mainsnak, $type];
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Convertit les Claims de Wikidata au format du système
     *
     * @param object $claim
     * @return array
     */
    public function cleanClaim($claim) {

        $return = ["values" => [], "labels" => null, "type" => null];
        $prop_id = null;
        $type = null;

        foreach($claim as $claim_iteration) {
            $subArray = [];

            if(property_exists($claim_iteration, 'mainsnak')) {
                $prop_id = $claim_iteration->mainsnak->property;

                if(property_exists($claim_iteration->mainsnak, 'datavalue')) {
                    if(property_exists($claim_iteration->mainsnak->datavalue, 'value')) {
                        $dataValueContainer = $this->formatDataValue($claim_iteration->mainsnak->datavalue);
                        $subArray["mainsnak"] = $dataValueContainer[0];
                        $type = $dataValueContainer[1];
                    }
                }
            }
            if(property_exists($claim_iteration, 'qualifiers')) {
                $subArray["qualifiers"] = array();

                foreach ($claim_iteration->qualifiers as $qualifierPropoerty => $qualifierContents) {
                    $qualifierI = [
                        "property" => $qualifierPropoerty,
                        "values" => [],
                        "type" => null
                    ];

                    foreach ($qualifierContents as $qualifierContent) {
                        $dataValueContainer = $this->formatDataValue($qualifierContent->datavalue);
                        $qualifierI['values'][] = $dataValueContainer[0];
                        $qualifierI['type'] = $dataValueContainer[1];
                    }

                    $subArray["qualifiers"][] = $qualifierI;
                }
            } else { $subArray["qualifiers"] = null; }

            $return["values"][] = $subArray;
            $return["type"] = $type;
        }

        $qwd_prop = $this->getEntity($prop_id);
        $prop_labels = $this->getLabelFromEntity($qwd_prop);
        $return["labels"] = $prop_labels;

        return $return;

    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Sélectionne les labels d'une entité Wikidata dans les langues sélectionnées
     *
     * @param object $wikidataEntity
     * @param string $language
     * @return array|string|null
     */
    public function getLabelFromEntity($wikidataEntity, $language=null) {
        if($language == null) {
            /* Label management */
            $labelArray = [];
            foreach($wikidataEntity->labels as $label_lg => $label_content) {
                if(in_array($label_lg, $this->getListOfLanguages())) {
                    $labelArray[$label_lg] = $label_content;
                }
            }

            return $labelArray;
        } else {
            foreach($wikidataEntity->labels as $label_lg => $label_content) {
                if($label_lg == $language) {
                    return $label_content->value;
                }
            }

            return "Valeur inconnue";
        }
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Calcule l'URL de l'image à télécharger sur Commons
     *
     * @param $claim
     * @return string
     */
    public function getImageFromClaim($claim_iteration) {
        $url = "";

        if (property_exists($claim_iteration, 'mainsnak')) {
            if (property_exists($claim_iteration->mainsnak, 'datavalue')) {
                if (property_exists($claim_iteration->mainsnak->datavalue, 'value')) {
                    $filename = $claim_iteration->mainsnak->datavalue->value;

                    $encrypt = md5(str_replace(' ', '_', $filename));
                    $firstDir = substr($encrypt, 0, 1);
                    $secondDir = substr($encrypt, 0, 2);

                    $url = "https://upload.wikimedia.org/wikipedia/commons/" . $firstDir . '/' . $secondDir . '/' . str_replace(' ', '_', $filename);

                    if($this->getImageWeight(str_replace(' ', '_', $filename)) >= 1000000) { // 1MO
                        $url = "https://upload.wikimedia.org/wikipedia/commons/thumb/" . $firstDir . '/' . $secondDir . '/' . str_replace(' ', '_', $filename) . '/1280px-' . str_replace(' ', '_', $filename);
                    }
                }
            }
        }

        return $url;
    }


    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne le poids en méga d'une image
     *
     * @param string $fileName
     * @return string|null
     */
    private function getImageWeight($fileName) {
        $value = null;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://commons.wikimedia.org/w/api.php?action=query&format=json&prop=imageinfo&iiprop=size&titles=File:".$fileName);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        $content = json_decode($response)->query->pages;

        foreach ($content as $item) {
            foreach ($item->imageinfo as $info) {
                $value = $info->size;
                break;
            }
        }

        return $value;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Convertit les numéros de mois en français
     *
     * @param int $value
     * @return string
     */
    public function monthToString($value) {
        switch ($value) {
            case 1:
                return "janvier";
                break;
            case 2:
                return "février";
                break;
            case 3:
                return "mars";
                break;
            case 4:
                return "avril";
                break;
            case 5:
                return "mai";
                break;
            case 6:
                return "juin";
                break;
            case 7:
                return "juillet";
                break;
            case 8:
                return "août";
                break;
            case 9:
                return "septembre";
                break;
            case 10:
                return "octobre";
                break;
            case 11:
                return "novembre";
                break;
            case 12:
                return "décembre";
                break;
            default:
                return "inconnu";
        }

    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Convertit une date au format Wikidata en string
     *
     * @param array $mainsnak
     * @return \DateTime|string
     * @throws \Exception
     */
    public function computeTime($mainsnak) {
        $timeS = $mainsnak->time;
        $time = new \DateTime($timeS);
        $periodManagement = 'AD';

        $suffix = "";
        if(substr($timeS, 0, 1) == "-") {
            $periodManagement = 'BC';
            $suffix = " av. J.-C.";

            $noBCEYear = strval(intval(substr($time->format('Y'), 1))-1);
            if(substr($noBCEYear, 0, 3) == "000") {
                $prettyYear = substr($noBCEYear, 3);
            } elseif(substr($noBCEYear, 0, 2) == "00") {
                $prettyYear = substr($noBCEYear, 2);
            } elseif(substr($noBCEYear, 0, 1) == "0") {
                $prettyYear = substr($noBCEYear, 1);
            } else {
                $prettyYear = $noBCEYear;
            }
        }

        $year = "";
        if($periodManagement == 'BC') {
            $year = $prettyYear;
        } elseif($periodManagement == 'AD') {
            $year = $time->format('Y');
        }

        switch ($mainsnak->precision) {
            case 0:
                return "billion years";
                break;
            case 1:
                return "hundred million years";
                break;
            case 2:
                return "ten million years";
                break;
            case 3:
                return "million years";
                break;
            case 4:
                return "hundred thousand years";
                break;
            case 5:
                return "ten thousand years";
                break;
            case 6:
                // Millénaire
                return (intval(substr($year, 0, -3))+1)."e millénaire".$suffix;
                break;
            case 7:
                // Century
                return (intval(substr($year, 0, -2))+1)."e siècle".$suffix;
                break;
            case 8:
                // Decade
                if($periodManagement == 'BC') {
                    return "années ".substr($prettyYear, 0, -1)."0".$suffix;
                } elseif($periodManagement == 'AD') {
                    return "années ".substr(strval($time->format('Y')), 0, -1)."0".$suffix;
                }
                break;
            case 9:
                // Year
                if($periodManagement == 'BC') {
                    return $prettyYear.$suffix;
                } elseif($periodManagement == 'AD') {
                    return $time->format('Y').$suffix;
                }
                break;
            case 10:
                return $this->monthToString(intval($time->format('m'))).' '.$year.$suffix;
                break;
            case 11:
                return $time->format('d').' '.$this->monthToString(intval($time->format('m'))).' '.$year.$suffix;
                break;
            case 12:
                return $time->format('d').' '.$this->monthToString(intval($time->format('m'))).' '.$year.', '.$time->format('H').'h'.$suffix;
                break;
            case 13:
                return $time->format('d').' '.$this->monthToString(intval($time->format('m'))).' '.$year.', '.$time->format('H').'h'.$time->format('i').$suffix;
                break;
            case 14:
                return $time->format('d').' '.$this->monthToString(intval($time->format('m'))).' '.$year.', '.$time->format('H').'h'.$time->format('i:s').$suffix;
                break;
            default:
                return $time;
        }
    }
}

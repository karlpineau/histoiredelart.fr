<?php

namespace DATA\DataBundle\Form;

use DATA\DataBundle\Entity\EntityProperty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EntityType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('wikidataSameAs',         UrlType::class, array('required' => false,  'label' => 'SameAs'))
            ->add('properties',     CollectionType::class,   array(
                                                'entry_type' => EntityPropertyType::class,
                                                'allow_add'    => true,
                                                'allow_delete' => true))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DATA\DataBundle\Entity\Entity'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'data_databundle_entity';
    }
}

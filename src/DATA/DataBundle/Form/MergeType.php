<?php

namespace DATA\DataBundle\Form;

use Genemu\Bundle\FormBundle\Gd\Filter\Text;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;


class MergeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('entityLeft',     TextType::class,    array('required' => true, 'mapped' => false))
            ->add('entityRight',    TextType::class,    array('required' => true, 'mapped' => false))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'data_entity_merge';
    }
}

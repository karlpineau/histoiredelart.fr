<?php

namespace DATA\DataBundle\Form;

use DATA\DataBundle\Form\SourceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EntitySourcesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sources',     CollectionType::class,   array(
                'entry_type' => SourceType::class,
                'allow_add'    => true,
                'allow_delete' => true))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DATA\DataBundle\Entity\EntitySources'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'data_databundle_entitysources';
    }
}

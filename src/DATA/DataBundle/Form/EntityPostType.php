<?php

namespace DATA\DataBundle\Form;

use DATA\DataBundle\Entity\EntityProperty;
use DATA\ImageBundle\Form\ViewType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EntityPostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('properties',     CollectionType::class, array(
                                                'entry_type' => EntityPropertyType::class,
                                                'allow_add'    => true,
                                                'allow_delete' => true))
            ->add('wikidataSameAs',         UrlType::class, array('required' => false,  'label' => 'SameAs'))
            ->add('sources',        CollectionType::class, array(
                                                'entry_type' => SourceType::class,
                                                'allow_add'    => true,
                                                'allow_delete' => true,
                                                'mapped' => false))
            ->add('views',          CollectionType::class, array(
                                                'entry_type' => ViewType::class,
                                                'allow_add'    => true,
                                                'allow_delete' => true,
                                                'mapped' => false))
            ->add('importValidation',ChoiceType::class, array('choices'  => [
                                                                            'Yes' => true,
                                                                            'No' => false,
                                                                        ], "required" => true))
            ->add('enrichmentStatus',IntegerType::class, array("required" => true))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DATA\DataBundle\Entity\Entity'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'data_databundle_entity';
    }
}

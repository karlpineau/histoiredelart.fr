<?php

namespace DATA\SearchBundle\Service;

use DATA\DataBundle\Service\entity;
use DATA\SearchBundle\Entity\SearchIndex;
use DATA\TeachingBundle\Service\teaching;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

class search 
{
    protected $em;
    protected $teaching;
    protected $entity;
    protected $logger;

    public function __construct(EntityManager $EntityManager, teaching $teaching, entity $entity, LoggerInterface $logger)
    {
        $this->em = $EntityManager;
        $this->teaching = $teaching;
        $this->entity = $entity;
        $this->logger = $logger;
    }
    
    public function search($searchText, $teaching_slug)
    {
        $words = explode(' ', $searchText);
        $listMatchIndexes = array();
        $countEntities = array();

        foreach($words as $keyWord => $word) {
            $listMatchIndexes = array_merge($listMatchIndexes, $this->searchIndex($word));
        }

        $this->logger->error("----------- search2");
        $this->logger->error(json_encode($listMatchIndexes));

        /**
         * @var int $key
         * @var SearchIndex $index
         */
        foreach($listMatchIndexes as $key => $index) {
            if($index == null) {
                unset($listMatchIndexes[$key]);
            }
            elseif(array_key_exists($index->getEntity()->getId(), $countEntities)) {
                $nb = $countEntities[$index->getEntity()->getId()];
                $countEntities[$index->getEntity()->getId()] = $nb + 1;
            } else {
                $countEntities[$index->getEntity()->getId()] = 1;
            }
        }

        natsort($countEntities);

        $this->logger->error("----------- search3");
        $this->logger->error(json_encode($countEntities));

        if($teaching_slug != "null" and $teaching_slug != null) {
            $teaching = $this->em->getRepository('DATATeachingBundle:Teaching')->findOneBy(['slug' => $teaching_slug]);
            if($teaching !== null) {
                foreach ($countEntities as $idEntity => $count) {
                    /** @var \DATA\DataBundle\Entity\Entity $entity */
                    $entity = $this->entity->find('one', array('id' => $idEntity));
                    $concerned = false;
                    foreach ($this->entity->getTeachings($entity) as $teachingForEntity) {
                        if ($teachingForEntity == $teaching) {
                            $concerned = true;
                        }
                    }

                    if ($concerned == false) {
                        unset($countEntities[$idEntity]);
                    }
                }
            }
        }

        $returnArray = array();
        foreach($countEntities as $idEntity => $count) {
            $returnArray[] = $this->entity->find('one', array('id' => $idEntity));
        }
        return $returnArray;
    }

    public function searchIndex($searchText)
    {
        return $this->em->getRepository('DATASearchBundle:SearchIndex')
                        ->createQueryBuilder('a')
                        ->where('a.value LIKE :searchText')
                        ->setParameter('searchText', '%'.$searchText.'%')
                        ->getQuery()
                        ->getResult();
    }
}

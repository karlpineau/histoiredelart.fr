<?php

namespace DATA\SearchBundle\Entity;

use DATA\DataBundle\Entity\Entity;
use DATA\DataBundle\Entity\EntityProperty;
use Doctrine\ORM\Mapping as ORM;

/**
 * SearchIndex
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class SearchIndex
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="DATA\DataBundle\Entity\Entity", inversedBy="properties")
     * @ORM\JoinColumn(nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return SearchIndex
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set Entity
     *
     * @param Entity $entity
     * @return EntityProperty
     */
    public function setEntity(Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get Entity
     *
     * @return Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }
}

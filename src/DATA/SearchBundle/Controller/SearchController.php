<?php

namespace DATA\SearchBundle\Controller;

use DATA\SearchBundle\Entity\SearchLog;
use DATA\SearchBundle\Service\search;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use DATA\SearchBundle\Form\SearchType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends Controller
{
    /**
     * @Route(
     *     "/index",
     *     name="data_search_search_index"
     * )
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('DATASearchBundle:Search:index.html.twig');
    }

    /**
     * @Route(
     *     "/requete",
     *     name="data_search_search_search"
     * )
     *
     * @param null|string $teaching
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function searchAction($teaching=null, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* Définition des variables GET : */
        if($teaching == null) {
            if (isset($_GET['teaching'])) {
                $teaching = $_GET['teaching'];
            } elseif (isset($_POST['teaching'])) {
                $teaching = $_POST['teaching'];
            } else {$teaching = null;}
        }

        $query = null;
        if(isset($_GET['query'])) {
            $query = $_GET['query'];
        } elseif(isset($_GET['id'])) {
            /** @var SearchLog $searchLog */
            $searchLog = $em->getRepository('DATASearchBundle:SearchLog')->find($_GET['id']);
            if ($searchLog === null) {throw $this->createNotFoundException('You need to specify a correct id parameter');}
            $query = $searchLog->getSearchValue();
        }

        /* Création du formulaire de recherche : */
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $resultReturnedArray = array(
                'query' => $form->get('search')->getData(),
                'teaching' => $teaching
            );

            return $this->redirectToRoute('data_search_search_result', $resultReturnedArray);
        }

        $viewRetunedArray = array(
            'form' => $form->createView(),
            'teaching' => $teaching,
            'query' => $query
        );

        return $this->render('DATASearchBundle:Search:search.html.twig', $viewRetunedArray);
    }

    /**
     * @Route(
     *     "",
     *     name="data_search_search_result"
     * )
     * @param Request $request
     * @return Response
     */
    public function resultAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var search $serchService */
        $serchService = $this->get('data_search.search');

        if(isset($_GET['id'])) {
            /** @var SearchLog $searchLog */
            $searchLog = $em->getRepository('DATASearchBundle:SearchLog')->find($_GET['id']);
            if($searchLog === null) {throw $this->createNotFoundException('You need to specify a correct id parameter');}
            $query = $searchLog->getSearchValue();
        } elseif(!isset($_GET['query'])) {
            throw $this->createNotFoundException('You need to specify a query parameter');
        } else {
            $query = $_GET['query'];
        }

        /* Définition des variables GET : */
        if(isset($_GET['teaching'])) {
            $teaching = $_GET['teaching'];
        } elseif(isset($_POST['teaching'])) {
            $teaching = $_POST['teaching'];
        } else {
            $teaching = null;
        }

        $searchResults = $serchService->search($query, $teaching);

        /* -- Ajout de la recherche à la liste des logs -- */
        if(!isset($_GET['id'])) {
            $searchLog = new SearchLog();
            $searchLog->setSearchValue($query);
            $searchLog->setSearchNumberResults(count($searchResults));
            if ($this->getUser() != null) {
                $searchLog->setCreateUser($this->getUser());
            }
            $em->persist($searchLog);
            $em->flush();
        }

        $paginator = $this->get('knp_paginator');
        $results = $paginator->paginate(
            $searchResults,
            $request->query->get('page', 1)/*page number*/,
            100/*limit per page*/
        );

        $returnedArray = array(
            'searchResults' => $results,
            'teaching' => $teaching,
            'query' => $query,
            'searchLog' => $searchLog
        );


        if($teaching == null) {
            return $this->render('DATASearchBundle:Search:results.html.twig', $returnedArray);
        } else {
            $returnedArray['teaching'] = $em->getRepository('DATATeachingBundle:Teaching')->findOneBy(['slug' => $teaching]);
            $returnedArray['listEntities'] = $results;
            return $this->render('DATAPublicBundle:Teaching:view.html.twig', $returnedArray);
        }
    }
}

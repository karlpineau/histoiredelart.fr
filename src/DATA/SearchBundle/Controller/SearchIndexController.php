<?php

namespace DATA\SearchBundle\Controller;

use DATA\DataBundle\Entity\EntityProperty;
use DATA\DataBundle\Service\entity;
use DATA\SearchBundle\Entity\SearchIndex;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class SearchIndexController extends Controller
{
    /**
     * @Route(
     *     "/super-administration/indexation/construction",
     *     name="data_search_searchindex_buildindex"
     * )
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function buildIndexAction()
    {
        set_time_limit(0);
        $em = $this->getDoctrine()->getManager();
        $searchIndexRepository = $em->getRepository('DATASearchBundle:SearchIndex');

        foreach ($searchIndexRepository->findAll() as $index) {
            $em->remove($index);
        }

        $properties = $em->getRepository('DATADataBundle:EntityProperty')->findAll();
        /** @var EntityProperty $property */
        foreach($properties as $property) {
            $value = $this->get('data_data.property')->aggregatePropertyValue($property);
            if($value != "") {
                $searchIndex = new SearchIndex();
                $searchIndex->setValue($value);
                $searchIndex->setEntity($property->getEntity());
                $em->persist($searchIndex);
            }
        }

        $em->flush();

        $countIndex = count($searchIndexRepository->findAll());
        $this->get('session')->getFlashBag()->add('notice', 'L\'index de recherche comporte '.$countIndex.' entrées.' );
        return $this->redirectToRoute('data_administration_home_index');
    } 
}

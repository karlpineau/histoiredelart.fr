<?php

namespace DATA\ImageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Image
 *
 * @ORM\Table()
 * @Vich\Uploadable
 * @ORM\Entity()
 */
class Image
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Vich\UploadableField(mapping="original_file", fileNameProperty="originalFileName")
     * @var File
     */
    private $originalFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $originalFileName;

    /**
     * @Vich\UploadableField(mapping="original_file", fileNameProperty="smallFileName")
     * @var File
     */
    private $smallFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $smallFileName;

    /**
     * @Vich\UploadableField(mapping="original_file", fileNameProperty="mediumFileName")
     * @var File
     */
    private $mediumFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $mediumFileName;

    /**
     * @Vich\UploadableField(mapping="original_file", fileNameProperty="largeFileName")
     * @var File
     */
    private $largeFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $largeFileName;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message = "Le champ 'Copyright' ne peut pas être vide"
     * )
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "Le champ 'Copyright' ne peut pas dépasser {{ limit }} caractères"
     * )
     *
     * @ORM\Column(name="copyright", type="string", length=255, nullable=false)
     */
    private $copyright;

    /**
     * @ORM\ManyToOne(targetEntity="DATA\ImageBundle\Entity\View", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $view;

    /**
     * @ORM\ManyToOne(targetEntity="CAS\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $createUser;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="createDate", type="datetime", nullable=false)
     */
    protected $createDate;

    /**
     * @ORM\ManyToOne(targetEntity="CAS\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $updateUser;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updateDate", type="datetime", nullable=true)
     */
    protected $updateDate;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     * @return Image
     * @throws \Exception
     */
    public function setOriginalFile(File $image = null)
    {
        $this->originalFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getOriginalFile()
    {
        return $this->originalFile;
    }

    /**
     * @param string $originalFileName
     *
     * @return Image
     */
    public function setOriginalFileName($originalFileName)
    {
        $this->originalFileName = $originalFileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalFileName()
    {
        return $this->originalFileName;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     * @return Image
     * @throws \Exception
     */
    public function setSmallFile(File $image = null)
    {
        $this->smallFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getSmallFile()
    {
        return $this->smallFile;
    }

    /**
     * @param string $smallFileName
     *
     * @return Image
     */
    public function setSmallFileName($smallFileName)
    {
        $this->smallFileName = $smallFileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getSmallFileName()
    {
        return $this->smallFileName;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     * @return Image
     * @throws \Exception
     */
    public function setMediumFile(File $image = null)
    {
        $this->mediumFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getMediumFile()
    {
        return $this->mediumFile;
    }

    /**
     * @param string $mediumFileName
     *
     * @return Image
     */
    public function setMediumFileName($mediumFileName)
    {
        $this->mediumFileName = $mediumFileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getMediumFileName()
    {
        return $this->mediumFileName;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     * @return Image
     * @throws \Exception
     */
    public function setLargeFile(File $image = null)
    {
        $this->largeFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getLargeFile()
    {
        return $this->largeFile;
    }

    /**
     * @param string $largeFileName
     *
     * @return Image
     */
    public function setLargeFileName($largeFileName)
    {
        $this->largeFileName = $largeFileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLargeFileName()
    {
        return $this->largeFileName;
    }

    /**
     * Set copyright
     *
     * @param string $copyright
     * @return Image
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;

        return $this;
    }

    /**
     * Get copyright
     *
     * @return string
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     * @return Image
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Image
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set view
     *
     * @param \DATA\ImageBundle\Entity\View $view
     * @return Image
     */
    public function setView(\DATA\ImageBundle\Entity\View $view = null)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return \DATA\ImageBundle\Entity\View
     */
    public function getView()
    {
        return $this->view;
    }



    /**
     * Set createUser
     *
     * @param \CAS\UserBundle\Entity\User $createUser
     * @return Image
     */
    public function setCreateUser(\CAS\UserBundle\Entity\User $createUser = null)
    {
        $this->createUser = $createUser;

        return $this;
    }

    /**
     * Get createUser
     *
     * @return \CAS\UserBundle\Entity\User
     */
    public function getCreateUser()
    {
        return $this->createUser;
    }

    /**
     * Set updateUser
     *
     * @param \CAS\UserBundle\Entity\User $updateUser
     * @return Image
     */
    public function setUpdateUser(\CAS\UserBundle\Entity\User $updateUser = null)
    {
        $this->updateUser = $updateUser;

        return $this;
    }

    /**
     * Get updateUser
     *
     * @return \CAS\UserBundle\Entity\User
     */
    public function getUpdateUser()
    {
        return $this->updateUser;
    }
}

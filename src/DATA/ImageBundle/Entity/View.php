<?php

namespace DATA\ImageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * View
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DATA\ImageBundle\Repository\ViewRepository")
 */
class View
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * Champ permettant d'indiquer le positionnement de la vue au sein de l'oeuvre
     *
     * @ORM\Column(name="orderView", type="integer", nullable=true)
     */
    private $orderView;

    /**
     * @ORM\ManyToOne(targetEntity="DATA\DataBundle\Entity\Entity")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $entity;

    /**
     * @ORM\ManyToMany(targetEntity="DATA\TeachingBundle\Entity\Teaching", inversedBy="views")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $teachings;

    /**
     * @ORM\ManyToMany(targetEntity="CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer", mappedBy="views", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $privatePlayers;

    /**
     * @var \stdClass
     *
     * @ORM\OneToMany(targetEntity="DATA\ImageBundle\Entity\ViewProperty", mappedBy="view", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $properties;


    /**
     * @ORM\ManyToOne(targetEntity="CAS\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $createUser;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="createDate", type="datetime", nullable=false)
     */
    protected $createDate;

    /**
     * @ORM\ManyToOne(targetEntity="CAS\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $updateUser;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updateDate", type="datetime", nullable=true)
     */
    protected $updateDate;


    public function get($attribute)
    {
        return $this->{$attribute};
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teachings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->privatePlayers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->properties = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     * @return View
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return View
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set createUser
     *
     * @param \CAS\UserBundle\Entity\User $createUser
     * @return View
     */
    public function setCreateUser(\CAS\UserBundle\Entity\User $createUser = null)
    {
        $this->createUser = $createUser;

        return $this;
    }

    /**
     * Get createUser
     *
     * @return \CAS\UserBundle\Entity\User
     */
    public function getCreateUser()
    {
        return $this->createUser;
    }

    /**
     * Set updateUser
     *
     * @param \CAS\UserBundle\Entity\User $updateUser
     * @return View
     */
    public function setUpdateUser(\CAS\UserBundle\Entity\User $updateUser = null)
    {
        $this->updateUser = $updateUser;

        return $this;
    }

    /**
     * Get updateUser
     *
     * @return \CAS\UserBundle\Entity\User
     */
    public function getUpdateUser()
    {
        return $this->updateUser;
    }

    /**
     * Set orderView
     *
     * @param integer $orderView
     * @return View
     */
    public function setOrderView($orderView)
    {
        $this->orderView = $orderView;

        return $this;
    }

    /**
     * Get orderView
     *
     * @return integer 
     */
    public function getOrderView()
    {
        return $this->orderView;
    }

    /**
     * Set entity
     *
     * @param \DATA\DataBundle\Entity\Entity $entity
     * @return View
     */
    public function setEntity(\DATA\DataBundle\Entity\Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \DATA\DataBundle\Entity\Entity 
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Add teachings
     *
     * @param \DATA\TeachingBundle\Entity\Teaching $teachings
     * @return View
     */
    public function addTeaching(\DATA\TeachingBundle\Entity\Teaching $teachings)
    {
        $this->teachings[] = $teachings;

        return $this;
    }

    /**
     * Remove teachings
     *
     * @param \DATA\TeachingBundle\Entity\Teaching $teachings
     */
    public function removeTeaching(\DATA\TeachingBundle\Entity\Teaching $teachings)
    {
        $this->teachings->removeElement($teachings);
    }

    /**
     * Get teachings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeachings()
    {
        return $this->teachings;
    }

    /**
     * Add privatePlayers
     *
     * @param \CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer $privatePlayers
     * @return View
     */
    public function addPrivatePlayer(\CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer $privatePlayers)
    {
        $this->privatePlayers[] = $privatePlayers;

        return $this;
    }

    /**
     * Remove privatePlayers
     *
     * @param \CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer $privatePlayers
     */
    public function removePrivatePlayer(\CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer $privatePlayers)
    {
        $this->privatePlayers->removeElement($privatePlayers);
    }

    /**
     * Get privatePlayers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrivatePlayers()
    {
        return $this->privatePlayers;
    }

    /**
     * Add properties
     *
     * @param \DATA\ImageBundle\Entity\ViewProperty $properties
     * @return View
     */
    public function addProperty(\DATA\DataBundle\Entity\EntityProperty $properties)
    {
        $this->properties[] = $properties;

        return $this;
    }

    /**
     * Remove properties
     *
     * @param \DATA\ImageBundle\Entity\ViewProperty $properties
     */
    public function removeProperty(\DATA\DataBundle\Entity\EntityProperty $properties)
    {
        $this->properties->removeElement($properties);
    }

    /**
     * Get properties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProperties()
    {
        return $this->properties;
    }
}

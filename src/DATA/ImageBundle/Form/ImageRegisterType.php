<?php

namespace DATA\ImageBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;


class ImageRegisterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('copyright',   TextType::class,           array('required' => true,  'label' => 'Copyright'))
            ->add('imageFile',  VichImageType::class,       array(
                                                                        'required'      => true,
                                                                        'allow_delete'  => false, // not mandatory, default is true
                                                                        'download_link' => true, // not mandatory, default is true
            ))
            ->add('view',        ViewType::class)
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DATA\ImageBundle\Entity\Image'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'data_imagebundle_imageregister';
    }
}

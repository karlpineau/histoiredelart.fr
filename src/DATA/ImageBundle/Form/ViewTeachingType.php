<?php

namespace DATA\ImageBundle\Form;

use DATA\TeachingBundle\Entity\Teaching;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ViewTeachingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teachings', EntityType::class,   array(
                'class' => Teaching::class,
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true,
                'empty_data'  => null,
                'by_reference' => false))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'data_image_view_teaching';
    }
}

<?php

namespace DATA\ImageBundle\Controller;

use DATA\DataBundle\Entity\Entity;
use DATA\ImageBundle\Entity\Image;
use DATA\ImageBundle\Entity\View;
use DATA\ImageBundle\Form\ImageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ImageController extends Controller
{
    /**
     * @Route(
     *     "/image/voir/{view_id}/{class}/{id}/{clichesnumber}/{session}/{import}/{image_id}",
     *     name="data_image_image_renderimage",
     *     requirements={
     *          "view_id"="\d+",
     *          "class"=".{0,40}",
     *          "id"=".{0,20}",
     *          "clichesnumber"=".{0,40}",
     *          "session"="true|false",
     *          "image_id"="\d+"
     *     },
     *     defaults={
     *          "id"="0",
     *          "clichesnumber"=".{0,40}",
     *          "session"=".{0,40}",
     *          "import"=".{0,40}",
     *          "image_id"=".{0,40}"
     *     }
     * )
     * @param $view_id
     * @param $class
     * @param int $id
     * @param int $clichesnumber
     * @param int $session
     * @param int $image_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderImageAction($view_id, $class, $id=0, $clichesnumber=0, $session=0, $image_id=0)
    {
        $em = $this->getDoctrine()->getManager();

        if($view_id === null AND $image_id == 0) {
            throw $this->createNotFoundException('Erreur de chargement de l\'image : identifiant inexistant');
        }

        if($view_id !== null AND $view_id != 0) {
            /** @var View $view */
            $view = $em->getRepository('DATAImageBundle:View')->findOneBy(['id' => $view_id]);
            /** @var Image $image */
            $image = $em->getRepository('DATAImageBundle:Image')->findOneBy(['view' => $view]);
        } elseif($image_id != 0) {
            /** @var Image $image */
            $image = $em->getRepository('DATAImageBundle:Image')->findOneBy(['id' => $image_id]);
        } else {
            throw $this->createNotFoundException('Erreur de chargement de l\'image : identifiant inexistant');
        }

        if ($image === null) {throw $this->createNotFoundException('Image for view : [id=' . $view_id . '] inexistante.');}

        if($id == 0) { $id = null;}
        if($clichesnumber == 0) { $clichesnumber = null; }
        if($session == 0) { $session = null; }

        $isModal = false;
        $requestedImageSize = "medium";

        $arrayClass = explode(' ', $class);
        foreach ($arrayClass as $keyArrayClass => $classAlone) {
            if($classAlone == 'modal') {
                $isModal = true;
                unset($arrayClass[$keyArrayClass]);
            }

            if($classAlone == 'size-small') {
                $requestedImageSize = "small";
                unset($arrayClass[$keyArrayClass]);
            } elseif($classAlone == 'size-medium') {
                $requestedImageSize = "medium";
                unset($arrayClass[$keyArrayClass]);
            } elseif($classAlone == 'size-large') {
                $requestedImageSize = "large";
                unset($arrayClass[$keyArrayClass]);
            } elseif($classAlone == 'size-original') {
                $requestedImageSize = "original";
                unset($arrayClass[$keyArrayClass]);
            }
        }
        if($isModal == true) {
            $class = implode(' ', $arrayClass);
        }

        return $this->render('DATAImageBundle:Image:renderImage.html.twig', array(
                'image' => $image,
                'class' => $class,
                'isModal' => $isModal,
                'id' => $id,
                'clichesnumber' => $clichesnumber,
                'session' => $session,
                'requestedImageSize' => $requestedImageSize
        ));
    }

    /**
     * @Route(
     *     "/image/modifier/{view_id}",
     *     name="data_image_image_edit",
     *     requirements={
     *          "view_id"="\d+"
     *     }
     * )
     * @param $view_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($view_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryView = $em->getRepository('DATAImageBundle:View');
        $repositoryImage = $em->getRepository('DATAImageBundle:Image');

        /** @var View $view */
        $view = $repositoryView->findOneBy(['id' => $view_id]);
        if ($view === null) { throw $this->createNotFoundException('View : [id='.$view_id.'] inexistante.'); }
        /** @var Image $image */
        $image = $repositoryImage->findOneBy(['view' => $view]);
        if ($image === null) { throw $this->createNotFoundException('Image for view : [id='.$view_id.'] inexistante.'); }
        /** @var Entity $entity */
        $entity = $this->get('data_data.entity')->getByView($view);

        $form = $this->createForm(ImageType::class, $image);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $imageSet = $this->get("data_image.image")->setImage($image->getOriginalFileName(), 'replace');
            $em = $this->getDoctrine()->getManager();
            $em->persist($image);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, l\'item a bien été modifié.' );
            return $this->redirectToRoute('data_data_entity_view', array('id' => $view->getEntity()->getId()));
        }

        return $this->render('DATAImageBundle:Image:Edit/edit.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity
        ));
    }
}

<?php

namespace DATA\ImageBundle\Controller;

use DATA\DataBundle\Entity\Entity;
use DATA\ImageBundle\Form\ViewTeachingType;
use DATA\ImageBundle\Entity\View;
use DATA\TeachingBundle\Entity\Teaching;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ViewTeachingController extends Controller
{
    /**
     * @Route(
     *     "/teaching/edit/{id}",
     *     name="data_image_view_edit_teaching",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var View $view */
        $view = $em->getRepository('DATAImageBundle:View')->find($id);
        if ($view === null) {throw $this->createNotFoundException('Item : [id='.$id.'] inexistant.');}
        
        $form = $this->createForm(ViewTeachingType::class, $view);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($view->getTeachings() as $teaching) {
                $view->removeTeaching($teaching);
            }

            /** @var Teaching $teaching */
            foreach ($form->get('teachings')->getData() as $teaching) {
                $view->addTeaching($teaching);
            }
            $em->persist($view);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, l\'enseignement a bien été ajouté.' );
            return $this->redirect($this->generateUrl('data_data_entity_view', ['id' => $view->getEntity()->getId()]));
        }
        
        return $this->render('DATAImageBundle:ViewTeaching:editTeaching.html.twig', array(
            'view' => $view,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(
     *     "/teaching/delete/{id}/{slug_teaching}",
     *     name="data_image_view_delete_teaching",
     *     requirements={
     *          "id"="\d+",
     *          "slug_teaching"="\S{0,255}"
     *     }
     * )
     * @param int $id
     * @param string $slug_teaching
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id, $slug_teaching)
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryTeaching = $em->getRepository('DATATeachingBundle:Teaching');
        /** @var View $view */
        $view = $em->getRepository('DATAImageBundle:View')->find($id);
        /** @var Teaching $teaching */
        $teaching = $repositoryTeaching->findOneBy(['slug' => $slug_teaching]);

        if($view === null) {throw $this->createNotFoundException('Item : [id='.$id.'] inexistant.');}
        if($teaching === null) {throw $this->createNotFoundException('Teaching : [slug='.$slug_teaching.'] inexistant.');}

        $teaching->removeView($view);
        $em->persist($teaching);
        $em->flush();
        
        $this->get('session')->getFlashBag()->add('notice', 'Votre item a bien été supprimé de cet enseignement.' );
        return $this->redirectToRoute('data_data_entity_view', ['id' => $view->getEntity()->getId()]);
    }
}

<?php

namespace DATA\ImageBundle\Controller;

use DATA\DataBundle\Entity\Entity;
use DATA\ImageBundle\Entity\Image;
use DATA\ImageBundle\Entity\View;
use DATA\ImageBundle\Form\ViewType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ViewController extends Controller
{
    /**
     * @Route(
     *     "/vue/modifier/{view_id}",
     *     name="data_image_view_edit",
     *     requirements={
     *          "view_id"="\d+"
     *     }
     * )
     * @param $view_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($view_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryView = $em->getRepository('DATAImageBundle:View');
        $repositoryImage = $em->getRepository('DATAImageBundle:Image');

        /** @var View $view */
        $view = $repositoryView->findOneBy(['id' => $view_id]);
        if ($view === null) { throw $this->createNotFoundException('View : [id='.$view_id.'] inexistante.'); }
        /** @var Image $image */
        $image = $repositoryImage->findOneBy(['view' => $view]);
        if ($image === null) { throw $this->createNotFoundException('Image for view : [id='.$view_id.'] inexistante.'); }
        /** @var Entity $entity */
        $entity = $this->get('data_data.entity')->getByView($view);
        
        $form = $this->createForm(ViewType::class, $view);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($view);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, la vue a bien été modifiée.' );
            return $this->redirectToRoute('data_data_entity_view', array('id' => $view->getEntity()->getId()));
        }

        return $this->render('DATAImageBundle:View:register.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity
        ));
    }

    /**
     * @Route(
     *     "/vue/supprimer/{view_id}",
     *     name="data_image_view_delete",
     *     requirements={
     *          "view_id"="\d+"
     *     }
     * )
     * @param $view_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($view_id)
    {
        $em = $this->getDoctrine()->getManager();

        $repositoryView = $em->getRepository('DATAImageBundle:View');
        /** @var View $view */
        $view = $repositoryView->findOneBy(['id' => $view_id]);
        if ($view === null) { throw $this->createNotFoundException('View : [id='.$view_id.'] inexistante.'); }
        /** @var Entity $entity */
        $entity = $view->getEntity();
        $this->get('data_image.view')->deleteView($view);

        $this->get('session')->getFlashBag()->add('notice', 'Félicitations, la vue a bien été supprimée.' );
        return $this->redirectToRoute('data_data_entity_view', array('id' => $entity->getId()));
    }
}

<?php

namespace DATA\ImageBundle\Service;

use DATA\DataBundle\Service\entity;
use Doctrine\ORM\EntityManager;
use \Imagick;

/*
 * Service dont l'objectif est d'appliquer des méthodes globales à toutes les entités de DATA
 */
class image 
{
    protected $em;

    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }
    
    //Fonction retournant une oeuvre au hasard valable pour une session
    public function getOneByView($view)
    {
        $repositoryImage = $this->em->getRepository('DATAImageBundle:Image');
	    return $repositoryImage->findOneBy(["view" => $view]);
    }

    /**
     * @param $img
     * @param $filename
     * @param $width
     * @param $height
     * @param $extension
     * @param int $quality
     * @throws \ImagickException
     */
    function generateThumbnail($img, $filename, $width, $height, $extension, $quality = 90)
    {
        if (is_file($img)) {
            $imagick = new \Imagick(realpath($img));
            $imagick->setImageFormat('jpeg');
            $imagick->setImageCompression(\Imagick::COMPRESSION_JPEG);
            $imagick->setImageCompressionQuality($quality);
            $imagick->thumbnailImage($width, $height, false, false);
            file_put_contents(__DIR__. '/../../../../web/uploads/gallery/'. $filename . '_'. $extension . '.jpg', $imagick);
        }
    }

    /**
     * @param string $url
     * @return array
     * @throws \ImagickException
     */
    function setImage($url, $context) {
        $arrayUrl = explode('.', $url);
        $extension = $arrayUrl[count($arrayUrl)-1];
        $newFileName = uniqid() . rand();
        $newFileNameFull = $newFileName.'.'.$extension;
        if($context == "download") {
            file_put_contents(__DIR__ . '/../../../../web/uploads/gallery/' . $newFileNameFull, fopen($url, 'r'));
        } elseif ($context == "local") {
            rename(__DIR__ . '/../../../../web/' . $url, __DIR__ . "/../../../../web/uploads/gallery/" . $newFileNameFull);
        } elseif ($context == "replace") {
            rename(__DIR__ . '/../../../../web/web/uploads/gallery/' . $url, __DIR__ . "/../../../../web/uploads/gallery/" . $newFileNameFull);
        }

        $imageInfo = getimagesize(__DIR__ . '/../../../../web/uploads/gallery/' . $newFileNameFull);
        $imageInfoWidth = $imageInfo[0];
        $imageInfoHeight = $imageInfo[1];

        if($imageInfoWidth > $imageInfoHeight) {
            $widthLarge = 1000;
            $heightLarge = (1000*$imageInfoHeight)/$imageInfoWidth;
            $widthMedium = 500;
            $heightMedium = (500*$imageInfoHeight)/$imageInfoWidth;
            $widthSmall = 150;
            $heightSmall = (150*$imageInfoHeight)/$imageInfoWidth;
        } else if($imageInfoWidth < $imageInfoHeight) {
            $widthLarge = (1000*$imageInfoWidth)/$imageInfoHeight;
            $heightLarge = 1000;
            $widthMedium = (500*$imageInfoWidth)/$imageInfoHeight;
            $heightMedium = 500;
            $widthSmall = (150*$imageInfoWidth)/$imageInfoHeight;
            $heightSmall = 150;
        } else {
            $widthLarge = 1000;
            $heightLarge = 1000;
            $widthMedium = 500;
            $heightMedium = 500;
            $widthSmall = 150;
            $heightSmall = 150;
        }

        $this->generateThumbnail(__DIR__ . '/../../../../web/uploads/gallery/' . $newFileNameFull, $newFileName, $widthLarge, $heightLarge, "large", $quality = 90);
        $this->generateThumbnail(__DIR__ . '/../../../../web/uploads/gallery/' . $newFileNameFull, $newFileName, $widthMedium, $heightMedium, "medium", $quality = 90);
        $this->generateThumbnail(__DIR__ . '/../../../../web/uploads/gallery/' . $newFileNameFull, $newFileName, $widthSmall, $heightSmall, "small", $quality = 90);

        return [$newFileNameFull, $newFileName];
    }
}

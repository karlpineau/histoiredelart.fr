<?php

namespace DATA\ImageBundle\Service;

use CLICHES\PlayerBundle\Entity\PlayerSession;
use DATA\DataBundle\Service\entity;
use DATA\ImageBundle\Service\image;
use DATA\TeachingBundle\Entity\Teaching;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;

/*
 * Service dont l'objectif est d'appliquer des méthodes globales à toutes les entités de DATA
 */
class view 
{
    protected $em;
    protected $package;
    protected $image;
    protected $playerOeuvre;
    protected $request;

    public function __construct(EntityManager $EntityManager, image $image, \CLICHES\PlayerBundle\Service\playerOeuvreService $playerOeuvre, RequestStack $request)
    {
        $this->em = $EntityManager;
        $this->package = new Package(new EmptyVersionStrategy());;
        $this->image = $image;
        $this->playerOeuvre = $playerOeuvre;
        $this->request = $request->getCurrentRequest();
    }

    /**
     * @param $view \DATA\ImageBundle\Entity\View
     * @return array
     */
    public function getTeachingsAdminForView($view)
    {
        $repositoryTeachingTest = $this->em->getRepository('DATATeachingBundle:TeachingTest');
        $repositoryTeachingTestVote = $this->em->getRepository('DATATeachingBundle:TeachingTestVote');

        $teachingArray = array();
        foreach($view->getTeachings() as $teaching) {
            $teachingTests = $repositoryTeachingTest->findBy(array('view' => $view, 'teaching' => $teaching));

            $teachingTestsArray = array();
            foreach($teachingTests as $teachingTest) {
                $teachingTestsArray[] = [
                    'teachingTest' => $teachingTest,
                    'teachingTestVotes' => $repositoryTeachingTestVote->findBy(array('teachingTest' => $teachingTest)),
                    'teachingTestVotesOui' => $repositoryTeachingTestVote->findBy(array('teachingTest' => $teachingTest, 'vote' => true)),
                    'teachingTestVotesNon' => $repositoryTeachingTestVote->findBy(array('teachingTest' => $teachingTest, 'vote' => false))
                ];
            }
            $teachingArray[] = [
                'teaching' => $teaching,
                'teachingTests' => $teachingTestsArray
            ];
        }

        return $teachingArray;
    }

    public function isExcludedView($view) {
        return ($this->em->getRepository('CLICHESPlayerBundle:ExcludeView')->findOneBy(['view' => $view]) != null) ? true : false;
    }
    
    /**
     * Fonction retournant une oeuvre au hasard valable pour une session
     * @param $session PlayerSession
     * @return mixed
     */
    public function getRandView($session) 
    {
        $repositoryView = $this->em->getRepository('DATAImageBundle:View');
	    return $repositoryView->searchRandView($session);
    }

    public function getListFieldsForView() {
        return [['field' => 'isPlan', 'label' => 'Est-ce un plan ?'],
                ['field' => 'vue', 'label' => 'Vue'],
                ['field' => 'iconography', 'label' => 'Iconographie'],
                ['field' => 'title', 'label' => 'Titre'],
                ['field' => 'location', 'label' => 'Localisation']];
    }

    /**
     * @param $view \DATA\ImageBundle\Entity\View
     * @return array
     */
    public function getFieldsForView($view)
    {
        $data = array();
        if(!empty($view->getVue())) {array_push($data, ['field' => 'vue', 'label' => 'Vue', 'value' => $view->getVue()]);}
        if(!empty($view->getTitle())) {array_push($data, ['field' => 'title', 'label' => 'Vue', 'value' => $view->getTitle()]);}
        if(!empty($view->getIconography())) {array_push($data, ['field' => 'iconography', 'label' => 'Sujet iconographique', 'value' => $view->getIconography()]);}
        if(!empty($view->getLocation())) {array_push($data, ['field' => 'location', 'label' => 'Emplacement', 'value' => $view->getLocation()]);}

        return $data;
    }


    /**
     * @param \DATA\ImageBundle\Entity\View $viewHost
     * @param \DATA\ImageBundle\Entity\View $viewMerging
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function mergeViews($viewHost, $viewMerging) {
        /*
         * Liste des éléments à fusionner :
         *      - PlayerOeuvre Cliches
         *      - Votes
         *
         * Puis suppression de l'entité image correspondante et de l'entité view
         */

        $repositoryPlayerOeuvre = $this->em->getRepository('CLICHESPlayerBundle:PlayerOeuvre');
        foreach ($repositoryPlayerOeuvre->findByView($viewMerging) as $playerOeuvre) {
            $playerOeuvre->setView($viewHost);
            $this->em->persist($playerOeuvre);
        }

        $repositoryTeachingTest = $this->em->getRepository('DATATeachingBundle:TeachingTest');
        $teachingTests = $repositoryTeachingTest->findByView($viewMerging);
        foreach ($teachingTests as $teachingTest) {
            $teachingTest->setView($viewHost);
            $this->em->persist($teachingTest);
        }

        $this->em->flush();

        $this->deleteView($viewMerging);
    }

    /**
     * @param \DATA\ImageBundle\Entity\View $views
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function checkOrderViews($views)
    {
        foreach($views as $key => $container)
        {
            /** @var \DATA\ImageBundle\Entity\View $view */
            $view = $container['view'];
            if($view->getOrderView() != ($key+1)) {
                $view->setOrderView($key+1);
                $this->em->persist($view);
            }
        }
        $this->em->flush();
    }

    /**
     * @param $view \DATA\ImageBundle\Entity\View
     * @return null|string
     */
    public function getThumbnail($view) {
        $image = $this->image->getOneByView($view);
        if($image != null) {
            if ($this->request->getHttpHost() == 'localhost') {
                return $this->package->getUrl($this->request->getScheme() . '://' . $this->request->getHttpHost() . '/histoiredelart/web/uploads/gallery/' . $image->getOriginalFileName());
            } else {
                return $this->package->getUrl($this->request->getScheme() . '://' . $this->request->getHttpHost() . '/web/uploads/gallery/' . $image->getOriginalFileName());
            }
        } else {
            return null;
        }

    }

    /**
     * @param \DATA\ImageBundle\Entity\View $view
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteView($view)
    {
        /*
         * Liste des modules à supprimer pour une entité :
         *      - Image <- Check
         *      - ExcludeView <- Check
         *      - TeachingTest <- Check
         *      - TeachingTestVote <- Check
         *      - PlayerSuggest <- Check
         *      - PlayerOeuvre <- Check
         */

        $repositoryImage = $this->em->getRepository('DATAImageBundle:Image');
        $image = $repositoryImage->findOneBy(['view' => $view]);
        if($image != null) {$this->em->remove($image);}

        foreach ($this->em->getRepository('DATAImageBundle:ViewProperty')->findBy(['view' => $view]) as $viewProperty) {
            $this->em->remove($viewProperty);
        }

        foreach ($this->em->getRepository('CLICHESPlayerBundle:PlayerOeuvre')->findBy(['view' => $view]) as $playerOeuvre) {$this->playerOeuvre->deletePlayerOeuvre($playerOeuvre);}

        $repositoryExcludeView = $this->em->getRepository('CLICHESPlayerBundle:ExcludeView');
        $excludeView = $repositoryExcludeView->findOneBy(['view' => $view]);
        if($excludeView != null) {$this->em->remove($excludeView);}

        $repositoryTeachingTest = $this->em->getRepository('DATATeachingBundle:TeachingTest');
        $repositoryTeachingTestVote = $this->em->getRepository('DATATeachingBundle:TeachingTestVote');
        $teachingTests = $repositoryTeachingTest->findBy(['view' => $view]);
        foreach ($teachingTests as $teachingTest) {
            $votes = $repositoryTeachingTestVote->findBy(['teachingTest' => $teachingTest]);
            foreach($votes as $vote) {$this->em->remove($vote);}
            $this->em->remove($teachingTest);
        }

        $teachings = $this->em->getRepository('DATATeachingBundle:Teaching')->findAll();
        /** @var Teaching $teaching */
        foreach ($teachings as $teaching) {
            foreach ($teaching->getViews() as $viewInTeaching) {
                if($viewInTeaching == $view) {
                    $teaching->removeView($view);
                }
            }
            $this->em->persist($teaching);
        }

        $this->em->remove($view);
        $this->em->flush();
    }
}

<?php

namespace DATA\PublicBundle\Service;

use Doctrine\ORM\EntityManager;

class reporting
{
    protected $em;

    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }
    
    public function traitementReporting($reporting)
    {
        $reporting->setTraitement(true);
        $this->em->persist($reporting);
        $this->em->flush();
    }

    public function validateReporting($reporting)
    {
        $reporting->setValidate(true);
        $this->em->persist($reporting);

        $this->traitementReporting($reporting);
    }

    public function refuseReporting($reporting)
    {
        $this->traitementReporting($reporting);
    }
}

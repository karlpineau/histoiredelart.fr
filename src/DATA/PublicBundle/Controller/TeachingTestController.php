<?php

namespace DATA\PublicBundle\Controller;

use DATA\PublicBundle\Entity\Visit;
use DATA\TeachingBundle\Entity\TeachingTest;
use DATA\TeachingBundle\Entity\TeachingTestVote;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeachingTestController extends Controller
{
    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Enregistre un vote en AJAX
     *
     * @Route(
     *     "/enseignement/vote/{teachingTest_id}/{vote}",
     *     name="data_public_teachingTest_vote",
     *     options={
     *          "expose"=true,
     *          "utf8"=true
     *     }
     * )
     * @param int $teachingTest_id
     * @param bool $vote
     * @param Request $request
     * @return Response
     */
    public function voteAction($teachingTest_id, $vote, Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            //Définition des variables :
            $em = $this->getDoctrine()->getManager();
            /** @var TeachingTest $teachingTest */
            $teachingTest = $em->getRepository('DATATeachingBundle:TeachingTest')->findOneBy(['id' => $teachingTest_id]);

            if($teachingTest != null) {
                if($em->getRepository('DATATeachingBundle:TeachingTestVote')->findOneBy(array('teachingTest' => $teachingTest, 'createUser' => $this->getUser())) == null) {
                    $teachingTestVote = new TeachingTestVote();
                    $teachingTestVote->setTeachingTest($teachingTest);
                    $teachingTestVote->setVote($vote);
                    $teachingTestVote->setCreateUser($this->getUser());
                    $em->persist($teachingTestVote);
                    $em->flush();
                    return new Response(json_encode(true));
                } else {
                    return new Response(json_encode('already_exist'));
                }
            }

            return new Response(json_encode('wrong_id'));
        }
        return new Response(json_encode(false));
    }
}

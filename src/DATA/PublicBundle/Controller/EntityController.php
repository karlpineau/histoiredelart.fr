<?php

namespace DATA\PublicBundle\Controller;

use DATA\DataBundle\Entity\Entity;
use DATA\DataBundle\Entity\EntityProperty;
use DATA\PersonalPlaceBundle\Entity\UserSessions;
use DATA\PersonalPlaceBundle\Form\UserSessionsType;
use DATA\PublicBundle\Entity\Reporting;
use DATA\PublicBundle\Entity\Visit;
use DATA\PublicBundle\Form\ReportingType;
use CAS\UserBundle\Entity\Favorite;
use DATA\TeachingBundle\Entity\TeachingTest;
use DATA\TeachingBundle\Entity\TeachingTestVote;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EntityController extends Controller
{
    /**
     * @Route(
     *     "/items",
     *     name="data_public_entity_index",
     *     options={
     *          "sitemap"={
     *              "priority"="0.8",
     *              "changefreq"="monthly"
     *          }
     *     }
     * )
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');
        /** @var Entity[] $entities */
        $entities = $this->get('data_data.entity')->find('all', null, 'restrict');

        $transi = array();
        foreach($entities as $entity) {
            $transi[$entity->getId()] = $entityService->get($entity, 'label');
        }
        natsort($transi);

        $return = array();
        foreach($transi as $keyId => $name) {
            $return[] = $keyId;
        }

        $paginator  = $this->get('knp_paginator');
        $listEntities = $paginator->paginate(
            $return,
            $request->query->get('page', 1)/*page number*/,
            100/*limit per page*/
        );

        return $this->render('DATAPublicBundle:Entity:index.html.twig', array(
            'listEntities' => $listEntities
        ));
    }

    /**
     * @Route(
     *     "/items/{id}/{context}",
     *     name="data_public_entity_view",
     *     defaults={"context"="null"},
     *     requirements={
     *          "id"="\d+",
     *          "context"="\S{0,255}"
     *     }
     * )
     * @param $id
     * @param $context
     * @param Request $request
     * @return Response
     */
    public function viewAction($id, $context, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if($user == "anon.") {$user = null;}
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->container->get('data_data.entity');
        /** @var Entity $entity */
        $entity = $entityService->getById($id);
        if ($entity === null) { throw $this->createNotFoundException('Item : [id='.$id.'] inexistant.'); }

        $relatedViews = $entityService->getViews($entity);
        $teachings = $entityService->getTeachings($entity);

        $properties = null;
        if($entity->getEnrichmentStatus() == 1) {
            /** @var EntityProperty[] $properties */
            $properties = $entityService->getProperties($entity, "wd", true);
        } else {
            /** @var EntityProperty[] $properties */
            $properties = $entityService->getProperties($entity, "hda", true);
        }
        
        if($this->getUser() != null) {
            $isFavorite = $em->getRepository('CASUserBundle:Favorite')->findOneBy(array('user' => $this->getUser(), 'entity' => $entity));
        } else {
            $isFavorite = false;
        }

        $arrayReturn = array();
        if(isset($_GET['reportingBoolean']) AND $_GET['reportingBoolean'] == 'true') {
            $reporting = new Reporting();
            if($user != null) {$reporting->setCreateUser($user);}
            $reporting->setTraitement(false);
            $reporting->setEntity($entity);

            $form = $this->createForm(ReportingType::class, $reporting);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($reporting);
                $em->flush();

                $this->get('session')->getFlashBag()->add('notice', 'Votre signalement a bien été transmis. Merci pour votre contribution.' );
                return $this->redirectToRoute('data_public_entity_view', array('id' => $entity->getId()));
            }

            $arrayReturn['form'] = $form->createView();

        } else {
            $visit = new Visit();
            $visit->setEntity($entity);
            if($context == "null") {$context = null;} $visit->setContext($context);
            if($user != null) {$visit->setCreateUser($user);}
            $em->persist($visit);
            $em->flush();
        }

        $arrayReturn['entity'] = $entity;
        $arrayReturn['properties'] = $properties;
        $arrayReturn['relatedViews'] = $relatedViews;
        $arrayReturn['teachings'] = $teachings;
        $arrayReturn['isFavorite'] = $isFavorite;
        if(isset($_GET['search']) AND !empty($_GET['search'])) {$arrayReturn['search_id'] = $_GET['search'];}
        if(isset($_GET['ppid']) AND !empty($_GET['ppid'])) {$arrayReturn['ppid'] = $_GET['ppid'];}
        if($this->getUser() != null) {
            $arrayReturn['privateSessions'] = $em->getRepository('CLICHESPersonalPlaceBundle:PrivatePlayer')->findBy(array('createUser' => $this->getUser()), array('createDate' => 'DESC'));
        }

        return $this->render('DATAPublicBundle:Entity:view.html.twig', $arrayReturn);
    }


    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Enregistre un report en AJAX
     *
     * @Route(
     *     "/report/{entity_id}/{reportingText}",
     *     name="data_public_entity_report",
     *     options={
     *          "expose"=true,
     *          "utf8"=true
     *     }
     * )
     * @param int $entity_id
     * @param string $reportingText
     * @param bool $vote
     * @param Request $request
     * @return Response
     */
    public function reportAction($entity_id, $reportingText, Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            //Définition des variables :
            $em = $this->getDoctrine()->getManager();
            /** @var \DATA\DataBundle\Service\entity $entityService */
            $entityService = $this->container->get('data_data.entity');
            /** @var Entity $entity */
            $entity = $entityService->getById($entity_id);

            if($entity != null) {
                $reporting = new Reporting();
                $reporting->setEntity($entity);
                $reporting->setReporting($reportingText);
                $reporting->setTraitement(false);
                $reporting->setCreateUser($this->getUser());
                $em->persist($reporting);
                $em->flush();
                return new Response(json_encode(true));
            }
            return new Response(json_encode('wrong_id'));
        }
        return new Response(json_encode(false));
    }
}

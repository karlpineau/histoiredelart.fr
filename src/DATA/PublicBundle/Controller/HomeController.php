<?php

namespace DATA\PublicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route(
     *     "/",
     *     name="data_public_home_index",
     *     options={
     *          "sitemap"={
     *              "priority"="1",
     *              "changefreq"="monthly"
     *          }
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('DATATeachingBundle:Teaching');
        $teachings = $repository->findBy(array('onLine' => true));

        $array = ['teachings' => $teachings];
        if(isset($_GET['ppid']) AND !empty($_GET['ppid'])) {$array['ppid'] = $_GET['ppid'];}

        return $this->render('DATAPublicBundle:Home:index.html.twig', $array);
    }

    /**
     * @Route(
     *     "/contribuer",
     *     name="data_public_home_contribute",
     *     options={
     *          "sitemap"={
     *              "priority"="1",
     *              "changefreq"="monthly"
     *          }
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contributeAction()
    {
        return $this->render('DATAPublicBundle:Home:contribute.html.twig');
    }
}

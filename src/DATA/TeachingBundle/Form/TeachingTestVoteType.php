<?php

namespace DATA\TeachingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TeachingTestVoteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('vote', ChoiceType::class, array('choices'  => array(
                                                        "Oui" => true,
                                                        "Non" => false,
                                                    ),
                                                    'expanded' => true,
                                                    'multiple' => false,
                                                    'required' => true))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DATA\TeachingBundle\Entity\TeachingTestVote'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'data_teachingbundle_teachingtestvote';
    }
}

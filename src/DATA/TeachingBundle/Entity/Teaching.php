<?php

namespace DATA\TeachingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Teaching
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class Teaching
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "Le champ 'Nom' ne peut pas dépasser {{ limit }} caractères"
     * )
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="onLine", type="boolean", nullable=true, options="default: true;")
     */
    private $onLine;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug", type="string", length=255, nullable=false, unique=true)
     */
    private $slug;

    /**
     * @var string
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "Le champ 'Sous-Matière' ne peut pas dépasser {{ limit }} caractères"
     * )
     *
     * @ORM\Column(name="sous_matiere", type="string", length=255, nullable=true)
     */
    private $subTeaching;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * @ORM\ManyToOne(targetEntity="DATA\TeachingBundle\Entity\University")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $university;

    /**
     * @ORM\ManyToMany(targetEntity="DATA\ImageBundle\Entity\View", mappedBy="teachings")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $views;

    /**
     * @ORM\ManyToMany(targetEntity="DATA\MementoMoriBundle\Entity\MementoMori", mappedBy="teachings")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $mementomoris;

    /**
     * @var integer
     *
     * @ORM\Column(name="viewsNumber", type="integer", nullable=true)
     */
    private $viewsNumber;

    /**
    * @ORM\ManyToOne(targetEntity="CAS\UserBundle\Entity\User")
    * @ORM\JoinColumn(nullable=true)
    */
    protected $createUser;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="createDate", type="datetime", nullable=false)
     */
    protected $createDate;

    /**
    * @ORM\ManyToOne(targetEntity="CAS\UserBundle\Entity\User")
    * @ORM\JoinColumn(nullable=true)
    */
    protected $updateUser;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updateDate", type="datetime", nullable=true)
     */
    protected $updateDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->views = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mementomoris = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Teaching
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set onLine
     *
     * @param boolean $onLine
     * @return Teaching
     */
    public function setOnLine($onLine)
    {
        $this->onLine = $onLine;

        return $this;
    }

    /**
     * Get onLine
     *
     * @return boolean 
     */
    public function getOnLine()
    {
        return $this->onLine;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Teaching
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set subTeaching
     *
     * @param string $subTeaching
     * @return Teaching
     */
    public function setSubTeaching($subTeaching)
    {
        $this->subTeaching = $subTeaching;

        return $this;
    }

    /**
     * Get subTeaching
     *
     * @return string 
     */
    public function getSubTeaching()
    {
        return $this->subTeaching;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Teaching
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set viewsNumber
     *
     * @param integer $viewsNumber
     * @return Teaching
     */
    public function setViewsNumber($viewsNumber)
    {
        $this->viewsNumber = $viewsNumber;

        return $this;
    }

    /**
     * Get viewsNumber
     *
     * @return integer
     */
    public function getViewsNumber()
    {
        return $this->viewsNumber;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     * @return Teaching
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Teaching
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set university
     *
     * @param \DATA\TeachingBundle\Entity\University $university
     * @return Teaching
     */
    public function setUniversity(\DATA\TeachingBundle\Entity\University $university = null)
    {
        $this->university = $university;

        return $this;
    }

    /**
     * Get university
     *
     * @return \DATA\TeachingBundle\Entity\University 
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * Add views
     *
     * @param \DATA\ImageBundle\Entity\View $views
     * @return Teaching
     */
    public function addView(\DATA\ImageBundle\Entity\View $views)
    {
        $this->views[] = $views;

        return $this;
    }

    /**
     * Remove views
     *
     * @param \DATA\ImageBundle\Entity\View $views
     */
    public function removeView(\DATA\ImageBundle\Entity\View $views)
    {
        $this->views->removeElement($views);
    }

    /**
     * Get views
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set createUser
     *
     * @param \CAS\UserBundle\Entity\User $createUser
     * @return Teaching
     */
    public function setCreateUser(\CAS\UserBundle\Entity\User $createUser = null)
    {
        $this->createUser = $createUser;

        return $this;
    }

    /**
     * Get createUser
     *
     * @return \CAS\UserBundle\Entity\User
     */
    public function getCreateUser()
    {
        return $this->createUser;
    }

    /**
     * Set updateUser
     *
     * @param \CAS\UserBundle\Entity\User $updateUser
     * @return Teaching
     */
    public function setUpdateUser(\CAS\UserBundle\Entity\User $updateUser = null)
    {
        $this->updateUser = $updateUser;

        return $this;
    }

    /**
     * Get updateUser
     *
     * @return \CAS\UserBundle\Entity\User
     */
    public function getUpdateUser()
    {
        return $this->updateUser;
    }



    /**
     * Add mementoMoris
     *
     * @param \DATA\MementoMoriBundle\Entity\MementoMori $mementoMoris
     * @return Teaching
     */
    public function addMementoMori(\DATA\MementoMoriBundle\Entity\MementoMori $mementoMoris)
    {
        $this->mementomoris[] = $mementoMoris;

        return $this;
    }

    /**
     * Remove mementoMoris
     *
     * @param \DATA\MementoMoriBundle\Entity\MementoMori $mementoMoris
     */
    public function removeMementoMori(\DATA\MementoMoriBundle\Entity\MementoMori $mementoMoris)
    {
        $this->mementomoris->removeElement($mementoMoris);
    }

    /**
     * Get mementoMoris
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMementoMoris()
    {
        return $this->mementomoris;
    }
}

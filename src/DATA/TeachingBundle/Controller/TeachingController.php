<?php

namespace DATA\TeachingBundle\Controller;

use DATA\ImageBundle\Entity\View;
use DATA\TeachingBundle\Entity\TeachingTest;
use DATA\TeachingBundle\Entity\TeachingTestVote;
use DATA\TeachingBundle\Form\TeachingTestVoteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use DATA\TeachingBundle\Entity\Teaching;
use DATA\TeachingBundle\Form\TeachingEditType;
use DATA\TeachingBundle\Form\MementoMoriType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeachingController extends Controller
{

    /**
     * @Route(
     *     "/enseignement/accueil",
     *     name="data_teaching_teaching_index"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $teachings = $this->getDoctrine()->getManager()->getRepository('DATATeachingBundle:Teaching')->findAll();

        return $this->render('DATATeachingBundle:Teaching:index.html.twig', array(
            'teachings' => $teachings
        ));
    }

    /**
     * @Route(
     *     "/enseignement/setViewsNumber",
     *     name="data_teaching_teaching_setViewsNumber"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function setViewsNumberAction()
    {
        $em = $this->getDoctrine()->getManager();
        $teachings = $em->getRepository('DATATeachingBundle:Teaching')->findAll();
        $views = $em->getRepository('DATAImageBundle:View')->findAll();

        $teachingsCount = [];

        /** @var View $view */
        foreach ($views as $view) {
            foreach ($view->getTeachings() as $teaching) {
                if(array_key_exists($teaching->getId(), $teachingsCount)) {
                    $teachingsCount[$teaching->getId()] = $teachingsCount[$teaching->getId()] + 1;
                } else {
                    $teachingsCount[$teaching->getId()] = 1;
                }
            }
        }

        /** @var Teaching $teaching */
        foreach ($teachings as $teaching) {
            $teaching->setViewsNumber($teachingsCount[$teaching->getId()]);
        }
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'ViewsNumber MàJ.' );
        return $this->redirect($this->generateUrl('data_administration_home_index'));
    }

    /**
     * @Route(
     *     "/enseignement/liste",
     *     name="data_teaching_teaching_list"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $listTeachings = $this->getDoctrine()->getManager()->getRepository('DATATeachingBundle:Teaching')->findAll();
        
        $teachingAction = $this->container->get('cliches_player.playersessionaction');
        $total = array();
        foreach ($listTeachings as $teaching) {
            $number = $teachingAction->countOeuvreByTeaching($teaching);
            array_push($total, $number);
        }
             
        return $this->render('DATATeachingBundle:Teaching:list.html.twig', array('total' => $total));
    }

    /**
     * @Route(
     *     "/enseignement/voir/{slug}",
     *     name="data_teaching_teaching_view",
     *     requirements={
     *          "slug"="\S{0,255}"
     *     }
     * )
     * @param $slug
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($slug, Request $request)
    {
        $teaching = $this->getDoctrine()->getManager()->getRepository('DATATeachingBundle:Teaching')->findOneBySlug($slug);
        if ($teaching === null) { throw $this->createNotFoundException('Enseignement : [slug='.$slug.'] inexistant.'); }

        $entities = $this->get('data_data.entity')->getByTeaching($teaching);
        $paginator  = $this->get('knp_paginator');
        $listEntities = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            100/*limit per page*/
        );

        return $this->render('DATATeachingBundle:Teaching:view.html.twig', array(
            'teaching' => $teaching,
            'listEntities' => $listEntities
        ));
    }

    /**
     * @Route(
     *     "/enseignement/nouveau",
     *     name="data_teaching_teaching_register"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $teaching = new Teaching;
        
        $form = $this->createForm(MementoMoriType::class, $teaching);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $teaching->setCreateUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($teaching);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, l\'enseignement a bien été créé.' );
            return $this->redirect($this->generateUrl('data_teaching_teaching_view', array('slug' => $teaching->getSlug())));
        }

        return $this->render('DATATeachingBundle:Teaching:register.html.twig', array(
                                'form' => $form->createView(),
                            ));
    }

    /**
     * @Route(
     *     "/enseignement/editer/{slug}",
     *     name="data_teaching_teaching_edit",
     *     requirements={
     *          "slug"="\S{0,255}"
     *     }
     * )
     * @param $slug
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($slug, Request $request)
    {
        $teaching = $this->getDoctrine()->getManager()->getRepository('DATATeachingBundle:Teaching')->findOneBySlug($slug);
        if ($teaching === null) {throw $this->createNotFoundException('Enseignement : [slug='.$slug.'] inexistant.');}
        
        $form = $this->createForm(TeachingEditType::class, $teaching);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $teaching->setUpdateUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($teaching);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, votre enseignement a bien été édité.' );
            return $this->redirect($this->generateUrl('data_teaching_teaching_view', array('slug' => $teaching->getSlug())));
        }
        
        return $this->render('DATATeachingBundle:Teaching:edit.html.twig', array(
                                'teaching' => $teaching,
                                'form' => $form->createView(),
                            ));
    }

    /**
     * @Route(
     *     "/enseignement/supprimer/{slug}",
     *     name="data_teaching_teaching_delete",
     *     requirements={
     *          "slug"="\S{0,255}"
     *     }
     * )
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($slug)
    {
        $teaching = $this->getDoctrine()->getManager()->getRepository('DATATeachingBundle:Teaching')->findOneBySlug($slug);
        if($teaching === null) {throw $this->createNotFoundException('Enseignement : [slug='.$slug.'] inexistant.');}
        
        $teachingAction = $this->container->get('data_teaching.teaching');
        $teachingAction->deleteTeaching($teaching);
             
        $this->get('session')->getFlashBag()->add('notice', 'Votre enseignement a bien été supprimé.' );
        return $this->forward('DATATeachingBundle:Teaching:index');
    }
}

<?php

namespace DATA\TeachingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use DATA\TeachingBundle\Entity\University;
use DATA\TeachingBundle\Form\UniversityEditType;
use DATA\TeachingBundle\Form\UniversityRegisterType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UniversityController extends Controller
{
    /**
     * @Route(
     *     "/universite/accueil",
     *     name="data_teaching_university_index"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $universities = $this->getDoctrine()->getManager()->getRepository('DATATeachingBundle:University')->findAll();
        
        return $this->render('DATATeachingBundle:University:index.html.twig', array(
                'universities' => $universities
                ));
    }

    /**
     * @Route(
     *     "/universite/liste",
     *     name="data_teaching_university_list"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $listUniversities = $this->getDoctrine()->getManager()->getRepository('DATATeachingBundle:University')->findAll();

        return $this->render('DATATeachingBundle:University:list.html.twig', array('listUniversities' => $listUniversities));
    }

    /**
     * @Route(
     *     "/universite/voir/{slug}",
     *     name="data_teaching_university_view",
     *     requirements={
     *          "slug"="\S{0,255}"
     *     }
     * )
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($slug)
    {
        $university = $this->getDoctrine()->getManager()->getRepository('DATATeachingBundle:University')->findOneBySlug($slug);
        if ($university === null) {throw $this->createNotFoundException('Université : [slug='.$slug.'] inexistante.');}

        $universityAction = $this->container->get('data_teaching.university');
        $teachings = $universityAction->getTeachings($university);

        return $this->render('DATATeachingBundle:University:view.html.twig', array(
            'university' => $university,
            'teachings' => $teachings));
    }

    /**
     * @Route(
     *     "/universite/nouveau",
     *     name="data_teaching_university_register"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $university = new University;
        
        $form = $this->createForm(UniversityRegisterType::class, $university);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $university->setCreateUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($university);
            $em->flush();
                    
            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, l\'université a bien été créée.' );
            return $this->redirect($this->generateUrl('data_teaching_university_view', array('slug' => $university->getSlug())));
        }

        return $this->render('DATATeachingBundle:University:register.html.twig', array(
                                'form' => $form->createView(),
                            ));
    }

    /**
     * @Route(
     *     "/universite/editer/{slug}",
     *     name="data_teaching_university_edit",
     *     requirements={
     *          "slug"="\S{0,255}"
     *     }
     * )
     * @param $slug
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($slug, Request $request)
    {
        $university = $this->getDoctrine()->getManager()->getRepository('DATATeachingBundle:University')->findOneBySlug($slug);
        if ($university === null) {throw $this->createNotFoundException('Université : [slug='.$slug.'] inexistante.');}
        
        $form = $this->createForm( UniversityEditType::class, $university);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $university->setUpdateUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($university);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, votre université a bien été éditée.' );
            return $this->redirect($this->generateUrl('data_teaching_university_view', array('slug' => $university->getSlug())));
        }
        
        return $this->render('DATATeachingBundle:University:edit.html.twig', array(
                                'university' => $university,
                                'form' => $form->createView(),
                            ));
    }

    /**
     * @Route(
     *     "/universite/supprimer/{slug}",
     *     name="data_teaching_university_delete",
     *     requirements={
     *          "slug"="\S{0,255}"
     *     }
     * )
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($slug)
    {
        $university = $this->getDoctrine()->getManager()->getRepository('DATATeachingBundle:University')->findOneBySlug($slug);
        if ($university === null) {throw $this->createNotFoundException('Enseignement : [slug='.$slug.'] inexistant.');}
        
        $universityAction = $this->container->get('data_teaching.university');
        $universityAction->deleteUniversity($university);
             
        $this->get('session')->getFlashBag()->add('notice', 'Votre université a bien été supprimée.' );
        return $this->forward('DATATeachingBundle:University:index');
    }
}

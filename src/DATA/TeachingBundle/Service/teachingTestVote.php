<?php

namespace DATA\TeachingBundle\Service;

use CAS\UserBundle\Entity\User;
use DATA\DataBundle\Service\entity;
use DATA\ImageBundle\Entity\View;
use Doctrine\ORM\EntityManager;

class teachingTestVote
{
    protected $em;
    protected $entity;

    public function __construct(EntityManager $EntityManager, entity $entity)
    {
        $this->em = $EntityManager;
        $this->entityService = $entity;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Vérifie si un utilisateur a déjà exprimé un vote pour l'utilité d'un cliché au regard d'une matière
     *
     * @param \DATA\TeachingBundle\Entity\Teaching $teaching
     * @param View $view
     * @param User $user
     * @return object|null
     */
    public function checkVote($teaching, $view, $user)
    {
        $repositoryTeachingTestVote = $this->em->getRepository('DATATeachingBundle:TeachingTestVote');
        $repositoryTeachingTest = $this->em->getRepository('DATATeachingBundle:TeachingTest');
        $teachingTest = $repositoryTeachingTest->findOneBy(array('teaching' => $teaching, 'view' => $view));
        
        if($teachingTest != null) {
            return $repositoryTeachingTestVote->findOneBy(array('teachingTest' => $teachingTest, 'createUser' => $user));
        } else {
            return null;
        }
    }
}

<?php

namespace DATA\TeachingBundle\Service;

use CLICHES\PlayerBundle\Entity\PlayerSession;
use DATA\DataBundle\Service\entity;
use DATA\ImageBundle\Entity\View;
use Doctrine\ORM\EntityManager;

class teaching
{
    protected $em;

    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }

    /**
     * @param \DATA\TeachingBundle\Entity\Teaching $teaching
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteTeaching($teaching)
    {
        $repositoryPlayerSession = $this->em->getRepository('CLICHESPlayerBundle:PlayerSession');
        $players = $repositoryPlayerSession->findBy(['teaching' => $teaching]);
        /** @var PlayerSession $player */
        foreach($players as $player) {
            $player->setTeaching();
        }

        foreach($this->em->getRepository('DATAPublicBundle:Visit')->findBy(['teaching' => $teaching]) as $visit) {$this->em->remove($visit);}

        $views = $this->em->getRepository('DATAImageBundle:View')->findAll();
        /** @var View $view */
        foreach ($views as $view) {
            foreach ($view->getTeachings() as $teachingInView) {
                if($teachingInView == $teaching) {
                    $view->removeTeaching($teaching);
                }
            }
            $this->em->persist($view);
        }

        $this->em->remove($teaching);
        $this->em->flush();
    }
}

<?php

namespace DATA\ImportBundle\Controller;

use DATA\DataBundle\Entity\Entity;
use DATA\DataBundle\Entity\EntityProperty;
use DATA\DataBundle\Service\property;
use DATA\DataBundle\Service\wikidata;
use DATA\ImageBundle\Entity\Image;
use DATA\ImageBundle\Entity\View;
use DATA\ImageBundle\Entity\ViewProperty;
use DATA\ImportBundle\Entity\ImportSession;
use DATA\ImportBundle\Form\ImportType;
use DATA\ImportBundle\Form\WikidataImportType;
use DATA\TeachingBundle\Entity\Teaching;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WikidataController extends Controller
{
    /**
     * @Route(
     *     "/administration/wikidata",
     *     name="data_import_administration_wikidata"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ImagickException
     */
    public function indexAction(Request $request)
    {
        set_time_limit(0);
        $em = $this->getDoctrine()->getManager();
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');

        $form = $this->createForm(WikidataImportType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $uris = explode(',', $form->get('uris')->getData());
            /** @var wikidata $wikidataService */
            $wikidataService = $this->get('data_data.wikidata');
            /** @var property $propertyService */
            $propertyService = $this->get('data_data.property');
            $requiredProperties = $propertyService->getProperties();
            $requiredLanguages = $wikidataService->getListOfLanguages();

            $importSession = new ImportSession();
            $importSession->setCreateUser($user);
            $importSession->setImportValidation(false);
            $em->persist($importSession);

            $result = [];

            foreach($uris as $uri) {
                $uri = preg_replace('/\s/', '', $uri);
                $wikidataEntity = $wikidataService->getEntity($uri);

                /* Check if entity is already in database */
                /** @var Entity $testedEntity */
                $testedEntity = $em->getRepository('DATADataBundle:Entity')->findOneBy(['wikidataSameAs' => "https://www.wikidata.org/wiki/".$uri]);
                if($testedEntity != null) {
                    $result[] = ["entity" => $testedEntity->getId(), "status" => "alreadyInDB"];
                    continue;
                }

                /* Entity creation */
                $entity = new Entity();
                $entity->setImportValidation(false);
                $entity->setImportSession($importSession);
                $entity->setCreateUser($user);
                $entity->setEnrichmentStatus(1);
                $entity->setWikidataSameAs("https://www.wikidata.org/wiki/".$uri);
                $em->persist($entity);

                $entityService->derivation($entity, "https://www.wikidata.org/wiki/".$uri, $user, $form->get('teachings')->getData());

                $result[] = ["entity" => $entity->getId(), "status" => "insert"];
            }

            $em->flush();


            return $this->redirectToRoute("data_import_administration_index");
        }

        return $this->render('DATAImportBundle:Wikidata:index.html.twig', array(
            'form' => $form->createView()
        ));
    }
}

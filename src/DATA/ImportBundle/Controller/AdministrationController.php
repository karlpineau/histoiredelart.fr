<?php

namespace DATA\ImportBundle\Controller;

use DATA\DataBundle\Entity\Entity;
use DATA\DataBundle\Entity\EntityProperty;
use DATA\ImageBundle\Entity\Image;
use DATA\ImageBundle\Entity\View;
use DATA\ImportBundle\Entity\ImportSession;
use DATA\ImportBundle\Form\ImportType;
use DATA\TeachingBundle\Entity\Teaching;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdministrationController extends Controller
{
    /**
     * @Route(
     *     "/administration",
     *     name="data_import_administration_index"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Entity[] $entities */
        $entities = $this->get('data_data.entity')->find('all', array('importValidation' => false));

        if ($entities != null) {
            foreach ($entities as $entity) {
                if ($this->get('data_data.entity')->getViews($entity) == null) {
                    $this->get('data_data.entity')->removeEntity($entity);
                }
            }
            $em->flush();
            $entities = $this->get('data_data.entity')->find('all', array('importValidation' => false));
        }
        
        return $this->render('DATAImportBundle:Administration:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * @Route(
     *     "/administration/valider/{id}/{bool}",
     *     name="data_import_entity_validation",
     *     requirements={
     *          "id"="\d+",
     *          "bool"="true|false"
     *     }
     * )
     * @param $id
     * @param $bool
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function validationAction($id, $bool)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');

        /** @var Entity $entity */
        $entity = $this->get('data_data.entity')->getById($id);
        if ($entity === null) { throw $this->createNotFoundException('Entité : [id='.$id.'] inexistante.'); }
        $session = $entity->getImportSession();
        $user = $entity->getCreateUser();

        if($bool == 'true') {
            $entity->setImportValidation(true);
            $em->persist($entity);
            $em->flush();
        } elseif($bool == 'false') {
            $entityService->removeEntity($entity);
        }

        /* -- Dans le cas où c'est la dernière oeuvre validée de la session, on envoie un mail -- */
        $validBool = true;
        /** @var ImportSession $entitySession */
        foreach($entityService->find('all', array('importSession' => $session)) as $entitySession) {
            if($entitySession->getImportValidation() == false) {$validBool = false;}
        }
        if($session == null) {$validBool = false;}
        if($validBool == true) {
            $session->setImportValidation(true);
            $em->persist($session);
            $em->flush();

            if($em->getRepository('CASUserBundle:UserPreferences')->findOneByUser($user)->getDataDatasetConfirmation() == true) {
                /* -- Dans le cas où l'utilisateur souhaite être notifié des validations, on lui envoie un mail -- */
                $message = \Swift_Message::newInstance()
                    ->setSubject('Merci d\'avoir contribué à DATA !')
                    ->setFrom('cliches@histoiredelart.fr')
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView(
                            'DATAImportBundle:Administration:mailthanks.html.twig',
                            array('name' => $user->getUsername(), 'entities' => $entityService->find('all', array('importSession' => $session)))
                        ),
                        'text/html'
                    );
                $this->get('mailer')->send($message);
            }
        }

        return $this->redirectToRoute('data_import_administration_index');
    }

    /**
     * @Route(
     *     "/administration/mass-import",
     *     name="data_import_administration_massimport"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function massImportAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \DATA\DataBundle\Service\entity $entityService */
        $entityService = $this->get('data_data.entity');
        /** @var \DATA\DataBundle\Service\property $propertyService */
        $propertyService = $this->get('data_data.property');

        $arrayEntities = array();
        $toProceedFiles = scandir(__DIR__ . '/../../../../web/toProceed/');
        $row = 1;
        if (($handle = fopen(__DIR__ . '/../../../../web/toProceed/toImport.csv', "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                // echelle d'une ligne
                $num = count($data);
                $arrayEntities[$row] = array();
                for ($c=0; $c < $num; $c++) {
                    // echelle d'une cellule
                    $arrayEntities[$row][] = $data[$c];
                }

                //On va chercher l'image
                foreach ($toProceedFiles as $fileToProceed) {
                    $arrayNameFile = explode(".", $fileToProceed);
                    if($data[0] == $arrayNameFile[0]) {
                        $arrayEntities[$row][] = 'toProceed/' .$fileToProceed;
                    }
                }

                if(count($arrayEntities[$row]) == 11) {
                    $arrayEntities[$row][] = null;
                }

                $row++;
            }
            fclose($handle);
        }

        $form = $this->createForm(ImportType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entities = [];
            $user = $this->get('security.token_storage')->getToken()->getUser();
            
            foreach($arrayEntities as $key1 => $arrayEntity) {
                if(count($arrayEntity) == 12) {
                    $entity = new Entity();
                    $entity->setCreateUser($user);
                    $entity->setImportValidation(true);
                    $entity->setEnrichmentStatus(0);

                    foreach ($arrayEntity as $key => $field) {
                        $propertyCode = null;
                        if ($key == 1 and $field != "") {
                            $propertyCode = "label";
                        } elseif ($key == 2 and $field != "") {
                            $propertyCode = "P921";
                        } elseif ($key == 3 and $field != "") {
                            $propertyCode = "P170";
                        } elseif ($key == 4 and $field != "") {
                            $propertyCode = "P88";
                        } elseif ($key == 5 and $field != "") {
                            $propertyCode = "P495"; // Provenance, mais la propriété est "Pays d'origine"
                        } elseif ($key == 6 and $field != "") {
                            $propertyCode = "P571";
                        } elseif ($key == 7 and $field != "") {
                            $propertyCode = "P186";
                        } elseif ($key == 8 and $field != "") {
                            $propertyCode = "P4020";
                        } elseif ($key == 9 and $field != "") {
                            $propertyCode = "P135";
                        } elseif ($key == 10 and $field != "") {
                            $propertyCode = "P276";
                        }

                        if($propertyCode != null) {
                            $entityProperty = new EntityProperty();
                            $entityProperty->setValue(json_encode($propertyService->generatePropertyValue($propertyCode, [$field], "fr")));
                            $entityProperty->setProperty("hda-" . $propertyCode);
                            $entityProperty->setEntity($entity);
                            $entityProperty->setCreateUser($user);
                            $em->persist($entityProperty);
                        }
                    }

                    /* -- Ajout de la vue -- */
                    if($arrayEntity[11] != null) {
                        $view = new View();
                        $view->setEntity($entity);
                        $view->setCreateUser($user);

                        /* -- Ajout de l'enseignement-- */
                        /** @var Teaching $teaching */
                        foreach ($form->get('teachings')->getData() as $teaching) {
                            $view->addTeaching($teaching);
                        }

                        $imageSet = $this->get("data_image.image")->setImage($arrayEntity[11], 'local');

                        $image = new Image();
                        $image->setOriginalFileName($imageSet[0]);
                        $image->setSmallFileName($imageSet[1]."_small.jpg");
                        $image->setMediumFileName($imageSet[1]."_medium.jpg");
                        $image->setLargeFileName($imageSet[1] . "_large.jpg");
                        $image->setCopyright('Inconnu');
                        $image->setCreateUser($user);
                        $image->setView($view);

                        $em->persist($image);
                        $em->persist($view);
                    }
                    $em->persist($entity);
                    $entities[] = $entity;
                }
            }
            $em->flush();

            return $this->render('DATAImportBundle:Administration:mass/importResult.html.twig', array(
                'entities' => $entities
            ));
        }

        return $this->render('DATAImportBundle:Administration:mass/importForm.html.twig', array(
            'form' => $form->createView(),
            'file' => $arrayEntities
        ));
    }

    /**
     * @Route(
     *     "/administration/archives",
     *     name="data_import_administration_archives"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function archivesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryImportSession = $em->getRepository('DATAImportBundle:ImportSession');
        $importSessions = $repositoryImportSession->findBy(array('importValidation' => true));

        return $this->render('DATAImportBundle:Administration:archives.html.twig', array(
            'importSessions' => $importSessions,
        ));
    }
}

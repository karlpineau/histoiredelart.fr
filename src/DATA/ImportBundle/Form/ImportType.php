<?php

namespace DATA\ImportBundle\Form;

use DATA\DataBundle\Form\SourceType;
use DATA\ImageBundle\Form\ImageRegisterType;
use DATA\TeachingBundle\Entity\Teaching;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ImportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teachings', EntityType::class, [
                'class' => 'DATATeachingBundle:Teaching',
                'required' => true,
                'multiple' => true,
                'choice_label' => function($teaching) {
                    /** @var $teaching Teaching */
                    return $teaching->getName();
                },
                'group_by' => function($teaching) {
                    /** @var $teaching Teaching */
                    return $teaching->getYear().'e année';
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.onLine = :true')
                        ->setParameters(array('true' => true))
                        ->orderBy('t.year', 'ASC')
                        ->orderBy('t.name', 'ASC');

                },
                'empty_data' => 'Choisissez une matière ...',
                'mapped' => false
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'data_import_import';
    }
}

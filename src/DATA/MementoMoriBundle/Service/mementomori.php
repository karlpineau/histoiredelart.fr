<?php

namespace DATA\MementoMoriBundle\Service;

use CLICHES\PlayerBundle\Entity\PlayerSession;
use DATA\DataBundle\Service\entity;
use DATA\DataBundle\Service\property;
use DATA\ImageBundle\Entity\View;
use DATA\TeachingBundle\Entity\Teaching;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

class mementomori
{
    protected $em;
    protected $entityService;
    protected $propertyService;
    protected $logger;

    public function __construct(EntityManager $EntityManager, entity $entityService, property $propertyService, LoggerInterface $logger)
    {
        $this->em = $EntityManager;
        $this->entity = $entityService;
        $this->property = $propertyService;
        $this->logger = $logger;
    }

    /**
     * @param \DATA\MementoMoriBundle\Entity\MementoMori $mementomori
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete($mementomori)
    {
        $this->em->remove($mementomori);
        $this->em->flush();
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Génère les valeurs pour 1 mementomori
     *
     * @param \DATA\MementoMoriBundle\Entity\MementoMori $mementomori
     * @return array
     */
    public function computeContent($mementomori)
    {
        $arrayOfData = array();

        foreach ($mementomori->getTeachings() as $teaching) {
            $arrayOfData = array_merge($arrayOfData, $this->computeContentByTeaching($teaching));
        }

        return $arrayOfData;
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Génère les valeurs pour 1 teaching
     *
     * @param Teaching $teaching
     * @return array
     */
    public function computeContentByTeaching($teaching) {
        $entities = $this->entity->getByTeaching($teaching);

        $configProperties = $this->property->getProperties();

        $arrayOfData = array();

        /** @var \DATA\DataBundle\Entity\Entity $entity */
        foreach ($entities as $entity) {
            foreach ($this->entity->getProperties($entity) as $property) {

                if(array_key_exists(explode('-', $property->getProperty())[1], $configProperties) and
                    $configProperties[explode('-', $property->getProperty())[1]]['mementoMoriUsage'] != "false" and
                    explode('-', $property->getProperty())[0] == "wd" and
                    !in_array($this->property->getLitteralFromPropertyValue($property, 'fr'), $arrayOfData)
                ) {
                    $arrayOfData[] = $this->property->getLitteralFromPropertyValue($property, 'fr');
                }
            }
        }

        return $arrayOfData;
    }
}

<?php

namespace DATA\MementoMoriBundle\Controller;

use DATA\MementoMoriBundle\Entity\MementoMori;
use DATA\TeachingBundle\Entity\Teaching;
use Spipu\Html2Pdf\HTML2PDF;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use DATA\MementoMoriBundle\Form\MementoMoriType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MementoMoriController extends Controller
{

    /**
     * @Route(
     *     "/accueil",
     *     name="data_mementomori_mementomori_index"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $mementomoris = $this->getDoctrine()->getManager()->getRepository('DATAMementoMoriBundle:MementoMori')->findAll();

        return $this->render('DATAMementoMoriBundle:MementoMori:index.html.twig', array(
            'mementomoris' => $mementomoris
        ));
    }

    /**
     * @Route(
     *     "/voir/{id}",
     *     name="data_mementomori_mementomori_view",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id, Request $request)
    {
        $mementomori = $this->getDoctrine()->getManager()->getRepository('DATAMementoMoriBundle:MementoMori')->find($id);
        if ($mementomori === null) { throw $this->createNotFoundException('Memento mori : [id='.$id.'] inexistant.'); }

        return $this->render('DATAMementoMoriBundle:MementoMori:view.html.twig', array(
            'mementomori' => $mementomori
        ));
    }

    /**
     * @Route(
     *     "/nouveau",
     *     name="data_mementomori_mementomori_register"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $mementomori = new MementoMori();

        $form = $this->createForm(MementoMoriType::class, $mementomori);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $mementomori->setCreateUser($this->getUser());

            $em->persist($mementomori);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, le memento mori a bien été créé.' );
            return $this->redirect($this->generateUrl('data_mementomori_mementomori_view', array('id' => $mementomori->getId())));
        }

        return $this->render('DATAMementoMoriBundle:MementoMori:register.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(
     *     "/editer/{id}",
     *     name="data_mementomori_mementomori_edit",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request)
    {
        $mementomori = $this->getDoctrine()->getManager()->getRepository('DATAMementoMoriBundle:MementoMori')->find($id);
        if ($mementomori === null) {throw $this->createNotFoundException('Memento Mori : [slug='.$id.'] inexistant.');}
        
        $form = $this->createForm(MementoMoriType::class, $mementomori);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mementomori->setUpdateUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($mementomori);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, votre Memento Mori a bien été édité.' );
            return $this->redirect($this->generateUrl('data_mementomori_mementomori_view', array('id' => $mementomori->getId())));
        }
        
        return $this->render('DATAMementoMoriBundle:MementoMori:edit.html.twig', array(
            'mementomori' => $mementomori,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(
     *     "/supprimer/{id}",
     *     name="data_mementomori_mementomori_delete",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id)
    {
        $mementomori = $this->getDoctrine()->getManager()->getRepository('DATAMementoMoriBundle:MementoMori')->find($id);
        if($mementomori === null) {throw $this->createNotFoundException('MementoMori : [id='.$id.'] inexistant.');}
        
        $mementoService = $this->container->get('data_mementomori.mementomori');
        $mementoService->delete($mementomori);

        $this->get('session')->getFlashBag()->add('notice', 'Votre memento mori a bien été supprimé.' );
        return $this->redirectToRoute('data_mementomori_mementomori_index');
    }

    /**
     * @Route(
     *     "/generateContent/{id}",
     *     name="data_mementomori_mementomori_generateContent",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     */
    public function generateContentAction($id, Request $request)
    {
        set_time_limit(0);
        $em = $this->getDoctrine()->getManager();
        /** @var MementoMori $mementomori */
        $mementomori = $em->getRepository('DATAMementoMoriBundle:MementoMori')->find($id);
        if ($mementomori === null) {throw $this->createNotFoundException('Memento Mori : [slug='.$id.'] inexistant.');}

        $mementomori->setValues(json_encode($this->get('data_mementomori.mementomori')->computeContent($mementomori)));
        $em->flush();

        $template = $this->renderView('DATAMementoMoriBundle:PDF:pdf.html.twig', ['mementomori' => $mementomori]);

        //require_once(dirname(__DIR__).'/../../../vendor/autoload.php');
        //require_once(dirname( __DIR__ ).'/../../../vendor/spipu/html2pdf/src/Html2Pdf.php');

        //$html2pdf = new HTML2PDF('P', 'A4', 'fr');
        //$html2pdf->writeHTML($template);
        //$html2pdf->output('MA.pdf');

        $this->get('session')->getFlashBag()->add('notice', 'Félicitations, le contenu de votre Memento Mori a bien été mis à jour.' );
        return $this->redirect($this->generateUrl('data_mementomori_mementomori_view', array('id' => $mementomori->getId())));
    }
}

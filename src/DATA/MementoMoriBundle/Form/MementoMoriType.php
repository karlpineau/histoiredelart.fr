<?php

namespace DATA\MementoMoriBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DomCrawler\Field\TextareaFormField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class MementoMoriType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',              TextType::class,              array('required' => true))
            ->add('values',             TextareaType::class,    array('required' => false))
            ->add('pdfName',             TextType::class,    array('required' => false))
            ->add('teachings',          EntityType::class,   array(  'class' => 'DATATeachingBundle:Teaching',
                                                                    'choice_label' => 'name',
                                                                    'required' => true,
                                                                    'multiple' => true,
                                                                    'empty_data' => 'Enseignement'))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DATA\MementoMoriBundle\Entity\MementoMori'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'data_mementomori_mementomori_register';
    }
}

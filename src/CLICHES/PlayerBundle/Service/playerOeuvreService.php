<?php

namespace CLICHES\PlayerBundle\Service;

use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use CLICHES\PlayerBundle\Entity\PlayerSession;
use DATA\ImageBundle\Entity\View;
use Doctrine\ORM\EntityManager;

class playerOeuvreService
{
    protected $em;
    protected $playerProposal;

    public function __construct(EntityManager $EntityManager, playerProposalService $playerProposal)
    {
        $this->em = $EntityManager;
        $this->playerProposal = $playerProposal;
    }

    /**
     * @param $playerOeuvre PlayerOeuvre
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deletePlayerOeuvre($playerOeuvre)
    {
        $repositoryPlayerProposal = $this->em->getRepository('CLICHESPlayerBundle:PlayerProposal');
        $playersProposal = $repositoryPlayerProposal->findBy(["playerOeuvre" => $playerOeuvre]);
        foreach ($playersProposal as $playerProposal) {
            $this->playerProposal->deletePlayerProposal($playerProposal);
        }
        
        $this->em->remove($playerOeuvre);
        $this->em->flush();
    }

    /**
     * @param $view View
     * @return int
     */
    public function countByView($view)
    {
        $repositoryPlayerOeuvre = $this->em->getRepository('CLICHESPlayerBundle:PlayerOeuvre');
        $playerOeuvres = $repositoryPlayerOeuvre->findBy(["view" => $view]);
        
        return count($playerOeuvres);
    }

    /**
     * @param $view View
     * @return mixed
     */
    public function getByView($view) 
    {
        $repositoryPlayerOeuvre = $this->em->getRepository('CLICHESPlayerBundle:PlayerOeuvre');
        $playerOeuvres = $repositoryPlayerOeuvre->findBy(["view" => $view]);

        return $playerOeuvres;
    }

    /**
     * @param $playerSession PlayerSession
     * @return int
     */
    public function getIndexBySession($playerSession)
    {
        /** @var \CLICHES\PlayerBundle\Repository\PlayerOeuvreRepository $repository */
        $repository = $this->em->getRepository('CLICHESPlayerBundle:PlayerOeuvre');

        return count($repository->findBy(["playerSession" => $playerSession]));
    }
}

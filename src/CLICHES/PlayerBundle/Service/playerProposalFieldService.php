<?php

namespace CLICHES\PlayerBundle\Service;

use CLICHES\PlayerBundle\Entity\PlayerProposal;
use CLICHES\PlayerBundle\Entity\PlayerProposalField;
use DATA\ImageBundle\Service\view;
use Doctrine\ORM\EntityManager;

class playerProposalFieldService
{
    protected $em;
    protected $entity;
    protected $view;


    public function __construct(EntityManager $EntityManager, \DATA\DataBundle\Service\entity $entity, view $view)
    {
        $this->em = $EntityManager;
        $this->entity = $entity;
        $this->view = $view;
    }

    /**
     * @param $playerProposalField PlayerProposalField
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deletePlayerProposalField($playerProposalField)
    {
        $this->em->remove($playerProposalField);
        $this->em->flush();
    }

    /**
     * @param $idImg
     * @return string
     */
    public function foundProperties($idImg)
    {
        set_time_limit(0);
        $repositoryImage = $this->em->getRepository('DATAImageBundle:Image');
	    $image = $repositoryImage->findOneBy(['id' => $idImg]);
        $data = $this->entity->getPropertiesForCliches($image->getView()->getEntity());

        return json_encode($data);
    }

    /**
     * @param $playerProposal PlayerProposal
     * @return mixed
     */
    public function getByPlayerProposal($playerProposal)
    {
        set_time_limit(0);
        $repositoryPlayerProposalField = $this->em->getRepository('CLICHESPlayerBundle:PlayerProposalField');
        return $repositoryPlayerProposalField->findBy(['playerProposal' => $playerProposal]);
    }

    /**
     * @param null $field
     * @return float|int
     */
    public function getAverageFullByField($field=null)
    {
        set_time_limit(0);
        if($field == null) {
            $nullValue = $this->em->getRepository('CLICHESPlayerBundle:PlayerProposalField')->countNull();
            $totalValue = $this->em->getRepository('CLICHESPlayerBundle:PlayerProposalField')->countAll();
        } else {
            $nullValue = $this->em->getRepository('CLICHESPlayerBundle:PlayerProposalField')->countNullByField($field);
            $totalValue = $this->em->getRepository('CLICHESPlayerBundle:PlayerProposalField')->countAllByField($field);
        }
        return (($totalValue-$nullValue)*100)/$totalValue;
    }
}

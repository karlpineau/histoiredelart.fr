<?php

namespace CLICHES\PlayerBundle\Service;

use CLICHES\PlayerBundle\Entity\PlayerProposal;
use CLICHES\PlayerBundle\Entity\PlayerProposalField;
use DATA\DataBundle\Entity\Entity;
use DATA\DataBundle\Entity\EntityProperty;
use DATA\DataBundle\Service\property;
use Doctrine\ORM\EntityManager;

class playerResultService
{
    protected $em;
    protected $entity;
    protected $property;


    public function __construct(EntityManager $EntityManager, \DATA\DataBundle\Service\entity $entity, property $property)
    {
        $this->em = $EntityManager;
        $this->entity = $entity;
        $this->property = $property;
    }

    /**
     * @param $playerResult
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deletePlayerResult($playerResult)
    {
        $this->em->remove($playerResult);
        $this->em->flush();
    }

    /**
     * @param $playerProposal_id int
     * @param bool $jsonEncode
     * @return array|string
     */
    public function foundResults($playerProposal_id, $jsonEncode=true)
    {    
        $repositoryPlayerProposal = $this->em->getRepository('CLICHESPlayerBundle:PlayerProposal');
	    $repositoryPlayerProposalField = $this->em->getRepository('CLICHESPlayerBundle:PlayerProposalField');
	    /** @var $playerProposal PlayerProposal */
	    $playerProposal = $repositoryPlayerProposal->findOneBy(['id' => $playerProposal_id]);
        $playerProposalFields = $repositoryPlayerProposalField->findBy(['playerProposal' => $playerProposal]);

        /** @var Entity $entity */
        $entity = $playerProposal->getPlayerOeuvre()->getView()->getEntity();

        $data = array();
        /** @var $playerProposalField PlayerProposalField */
        foreach ($playerProposalFields as $playerProposalField) {
            $trueValue = null;
            if ($playerProposalField->getField() == "vue" OR $playerProposalField->getField() == "iconography" OR $playerProposalField->getField() == "title" OR $playerProposalField->getField() == "location") {
                $getter = "get" . ucfirst($playerProposalField->getField());
                $trueValue = $playerProposal->getPlayerOeuvre()->getView()->$getter();
                $label = $this->getLabel($playerProposalField->getField());
            } elseif(strpos($playerProposalField->getField(), '-') !== false) {
                /** @var EntityProperty $entityProperty */
                $entityProperty = $this->em->getRepository("DATADataBundle:EntityProperty")->findOneBy(array('entity' => $entity, 'property' => $playerProposalField->getField()));
                $trueValue = $entityProperty->getValue();
                $label = $this->property->getPropertyLabelFromEntityProperty($entityProperty, 'fr');
            } else {
                $trueValue = $this->entity->get($playerProposalField->getField(), $entity);
                $label = $this->getLabel($playerProposalField->getField());
            }

            if ($trueValue != null) {
                $arrayField = [
                    [
                        'field' => $playerProposalField->getField(),
                        'trueResult' => $trueValue,
                        'suggestResult' => $playerProposalField->getValue(),
                        'label' => $label
                    ]
                ];
                $data = array_merge($data, $arrayField);
            }
        }

        if($jsonEncode == true) {
            return json_encode( $data );
        } else {
            return $data;
        }
    }

    /**
     * @param $playerProposal PlayerProposal
     * @return bool
     */
    public function testFilled($playerProposal)
    {
        $repositoryPlayerProposalField = $this->em->getRepository('CLICHESPlayerBundle:PlayerProposalField');
        $playerProposalFields = $repositoryPlayerProposalField->findBy(['playerProposal' => $playerProposal]);

        $filled = false;
        foreach ($playerProposalFields as $playerProposalField) {
            /** @var $playerProposalField PlayerProposalField */
            if($playerProposalField->getValue() != null) {$filled = true;}
        }

        return $filled;
    }

    /**
     * @param $field string
     * @return string
     */
    public function getLabel($field)
    {
        switch ($field) {
            case 'name':
                return 'Nom';
                break;
            case 'sujet':
                return 'Sujet';
                break;
            case 'sujetIcono':
                return 'Sujet iconographique';
                break;
            case 'datation':
                return 'Datation';
                break;
            case 'auteur':
                return 'Auteur';
                break;
            case 'commanditaire':
                return 'Commanditaire';
                break;
            case 'lieuDeConservation':
                return 'Lieu de Conservation';
                break;
            case 'provenance':
                return 'Provenance';
                break;
            case 'mattech':
                return 'Matières & Techniques';
                break;
            case 'dimensions':
                return 'Dimensions';
                break;
            case 'style':
                return 'Style';
                break;
            case 'vue':
                return 'Vue';
                break;
            case 'iconography':
                return 'Iconographie de la vue';
                break;
            case 'title':
                return 'Titre de la vue';
                break;
            case 'location':
                return 'Emplacement de la vue';
                break;
        }
        return null;
    }
}

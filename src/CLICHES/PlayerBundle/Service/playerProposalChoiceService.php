<?php

namespace CLICHES\PlayerBundle\Service;

use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use CLICHES\PlayerBundle\Entity\PlayerProposal;
use DATA\DataBundle\Entity\Entity;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

class playerProposalChoiceService
{
    protected $em;
    protected $entity;
    protected $playerProposal;
    protected $logger;


    public function __construct(EntityManager $EntityManager, \DATA\DataBundle\Service\entity $entity, playerProposalService $playerProposal, LoggerInterface $logger)
    {
        $this->em = $EntityManager;
        $this->entity = $entity;
        $this->playerProposal = $playerProposal;
        $this->logger = $logger;
    }

    public static function compare_objects($obj_a, $obj_b) {
        return $obj_a->getId() - $obj_b->getId();
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne une réponse pour une question
     *
     * @param array $values : Liste des propositions déjà enregistrées
     * @param string $property : Propriété requise
     * @param Entity[] $usedEntities : liste des entités déjà utilisés dans la question
     * @param Entity[] $teachingEntities : liste des entités inscrites pour une matière
     * @param int $count : décompte du nombre de propositions restant à formuler
     * @param array $listValues : liste des valeurs utilisées dans la question
     * @return array|bool
     */
    private function getValueForEntity($values, $property, $usedEntities, $teachingEntities, $count, $listValues)
    {
        $listOfEntitiesUsabled = array_udiff($teachingEntities, $usedEntities, [$this, 'compare_objects']);

        if(count($listOfEntitiesUsabled) > 0) {
            $selectEntityId = array_rand($listOfEntitiesUsabled);
            $selectEntity = $listOfEntitiesUsabled[$selectEntityId];

            $wdP = $this->entity->isPropertyExist($selectEntity, "wd-".$property);
            $hdaP = $this->entity->isPropertyExist($selectEntity, "hda-".$property);
            if($wdP != false) {$isPropertyExist = $wdP;}
            elseif($hdaP != false) {$isPropertyExist = $hdaP;}
            else {$isPropertyExist = false;}

            $this->logger->error("------- getValueForEntity : if");
            $this->logger->error(json_encode($isPropertyExist));

            if($selectEntity != null and $isPropertyExist != false) {
                $valueSelected = $this->entity->get($selectEntity, $property);
                if ($valueSelected != null AND !in_array($valueSelected, $listValues)) {
                    $listValues[] = $valueSelected;
                    return ['values' => ['quizz' => false, 'value' => $valueSelected, 'entity' => $selectEntity], 'listValues' => $listValues];
                }
                $usedEntities[] = $selectEntity;
            }
        }

        $listOfEntitiesUsabledNew = array_udiff($teachingEntities, $usedEntities, [$this, 'compare_objects']);
        /*if(count($listOfEntitiesUsabledNew) > 0) {
            return $this->getValueForEntity($values, $property, $usedEntities, $teachingEntities, $count, $listValues);
        } else {*/
            return false;
        //}
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Retourne une question sur une EntityProperty avec plusieurs propositions
     *
     * @param PlayerOeuvre $playerOeuvre
     * @param string $property
     * @param int $choices_number
     * @return array
     */
    public function getValues($playerOeuvre, $property, $choices_number)
    {
        $view = $playerOeuvre->getView();
        $teaching = $playerOeuvre->getPlayerSession()->getTeaching();
        $entity = $this->entity->getByView($view);

        $teachingEntities = $this->entity->getByTeaching($teaching, false, false);
        $count = count($teachingEntities)-1;

        $this->logger->error("---> getValues : teachingEntities");
        $this->logger->error(json_encode($teachingEntities));

        $this->logger->error("---> getValues : count");
        $this->logger->error(json_encode($count));

        $listValues[] = $this->entity->get($entity, $property);
        $usedEntities = [$entity];
        $values = array();
        $values[] = ['quizz' => true, 'value' => $listValues[0], 'entity' => $entity];
        for($i = 0 ; $i < ($choices_number-1) ; $i++) {
            $returnFunctionValue = $this->getValueForEntity($values, $property, $usedEntities, $teachingEntities, $count, $listValues);
            if($returnFunctionValue != false) {
                $values[] = $returnFunctionValue['values'];
                $listValues = $returnFunctionValue['listValues'];
            } else {
                $values[] = false;
            }
        }

        return $values;
    }

    /**
     * @param $playerProposal PlayerProposal
     * @return int|null
     */
    public function getDifficultyLevel($playerProposal) {
        $difficultyLevelMax = 5;
        $difficultyLevelMin = 2;
        $difficultyLevelDefault = 3;
        $previousDifficultyLevel = null;
        $previousPlayerProposalChoice = null;

        $playerSession = $playerProposal->getPlayerOeuvre()->getPlayerSession();
        $previousPlayerOeuvres = $this->em->getRepository('CLICHESPlayerBundle:PlayerOeuvre')->findBy(['playerSession' => $playerSession], ['createDate' => 'DESC']);

        if(count($previousPlayerOeuvres) <= 1) {
            return $difficultyLevelDefault;
        } else {
            foreach($previousPlayerOeuvres as $key => $previousPlayerOeuvre) {
                $playerProposalChoice = $this->em->getRepository('CLICHESPlayerBundle:PlayerProposalChoice')->findOneBy(['playerProposal' => $this->playerProposal->getPlayerProposalByPlayerOeuvre($previousPlayerOeuvre)]);
                if($playerProposalChoice != null and $playerProposalChoice->getPlayerProposalChoiceValueSelected() != null) {
                    $previousDifficultyLevel = count($this->em->getRepository('CLICHESPlayerBundle:PlayerProposalChoiceValue')->findBy(['playerProposalChoice' => $playerProposalChoice]));
                    $previousPlayerProposalChoice = $playerProposalChoice;

                    break;
                }
            }

            if($previousPlayerProposalChoice != null) {
                if ($previousPlayerProposalChoice->getCorrectAnswer() == true and $previousDifficultyLevel < $difficultyLevelMax) {
                    return $previousDifficultyLevel + 1;
                } elseif ($previousPlayerProposalChoice->getCorrectAnswer() == false and $previousDifficultyLevel > $difficultyLevelMin) {
                    return $previousDifficultyLevel - 1;
                } else {
                    return $previousDifficultyLevel;
                }
            } else {
                return $previousDifficultyLevel;
            }
        }
    }
}

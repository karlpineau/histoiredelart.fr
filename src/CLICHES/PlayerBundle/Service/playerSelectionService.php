<?php

namespace CLICHES\PlayerBundle\Service;

use CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer;
use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use DATA\DataBundle\Service\entity;
use DATA\ImageBundle\Entity\View;
use DATA\TeachingBundle\Entity\Teaching;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

class playerSelectionService
{
    protected $em;
    protected $entity;
    protected $logger;

    public function __construct(EntityManager $EntityManager, entity $entity, LoggerInterface $logger)
    {
        $this->em = $EntityManager;
        $this->entity = $entity;
        $this->logger = $logger;
    }

    /**
     * Fonction retournant une oeuvre au hasard valable pour une session
     * @param $teaching Teaching
     * @param $passedPlayerOeuvres PlayerOeuvre[]
     * @return array
     */
    public function getRandViewForTeaching($teaching, $passedPlayerOeuvres)
    {
        $views = array();
        $selectedView = null;

        foreach($teaching->getViews() as $view) {
            if($this->em->getRepository('CLICHESPlayerBundle:ExcludeView')->findOneBy(['view' => $view]) == null) {
                $views[] = $view;
            }
        }

        if(count($views) > 0) {
            if (count($passedPlayerOeuvres) > 0) {
                foreach ($views as $kView => $view) {
                    foreach ($passedPlayerOeuvres as $passedPlayerOeuvre) {
                        if ($passedPlayerOeuvre->getView() == $view or $passedPlayerOeuvre->getView()->getEntity()->getId() == $view->getEntity()->getId()) {
                            unset($views[$kView]);
                        }
                    }
                }
            }

            //Réindexation du tableau pour supprimer les entrées vides
            /** @var View[] $views */
            $views = array_values(array_filter($views));

            if (count($views) == 1) {
                $selectedView = $views[0];
            } elseif (count($views) > 1) {
                $selectedView = $views[rand(0, (count($views) - 1))];
            }
        }

        if($selectedView == null) {
            return ['no_enough_entities', null];
        } else {
            return ['view', $selectedView];
        }
    }

    /**
     * Fonction retournant une oeuvre au hasard valable pour une session
     * @param $privatePlayer PrivatePlayer
     * @param $passedPlayerOeuvres PlayerOeuvre[]
     * @return array
     */
    public function getRandViewForPrivatePlayer($privatePlayer, $passedPlayerOeuvres)
    {
        $views = array();
        $selectedView = null;

        /** @var View $view */
        foreach ($privatePlayer->getViews() as $view) {
            if($this->em->getRepository('CLICHESPlayerBundle:ExcludeView')->findOneBy(['view' => $view]) == null) {
                $views[] = $view;
            }
        }

        if(count($views) > 0) {
            if (count($passedPlayerOeuvres) > 0) {
                foreach ($views as $kView => $view) {
                    foreach ($passedPlayerOeuvres as $passedPlayerOeuvre) {
                        if ($passedPlayerOeuvre->getView() == $view or $passedPlayerOeuvre->getView()->getEntity()->getId() == $view->getEntity()->getId()) {
                            unset($views[$kView]);
                        }
                    }
                }
            }

            //Réindexation du tableau pour supprimer les entrées vides
            /** @var View[] $views */
            $views = array_values(array_filter($views));

            if (count($views) == 1) {
                $selectedView = $views[0];
            } elseif (count($views) > 1) {
                $selectedView = $views[rand(0, (count($views) - 1))];
            }
        }

        if($selectedView == null) {
            return ['no_enough_entities', null];
        } else {
            return ['view', $selectedView];
        }
    }
}

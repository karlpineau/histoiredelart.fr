<?php

namespace CLICHES\PlayerBundle\Service;

use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use CLICHES\PlayerBundle\Entity\PlayerProposal;
use Doctrine\ORM\EntityManager;

class playerProposalService
{
    protected $em;
    
    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }

    /**
     * @param $playerProposal PlayerProposal
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deletePlayerProposal($playerProposal)
    {
        $this->em->remove($playerProposal);
        $this->em->flush();
    }

    /**
     * @param $playerOeuvre PlayerOeuvre
     * @return mixed
     */
    public function getPlayerProposalByPlayerOeuvre($playerOeuvre)
    {
        $repository = $this->em->getRepository('CLICHESPlayerBundle:PlayerProposal');

        return $repository->findOneBy(['playerOeuvre' => $playerOeuvre]);
    }
}

<?php

namespace CLICHES\PlayerBundle\Service;

use CLICHES\PlayerBundle\Entity\ExcludeView;
use DATA\ImageBundle\Entity\View;
use Doctrine\ORM\EntityManager;

class excludeViewService
{
    protected $em;

    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }

    /**
     * @param $view View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function excludeView($view)
    {
        $exclude = new ExcludeView();
        $exclude->setView($view);
        $this->em->persist($exclude);
        $this->em->flush();
    }

    /**
     * @param $excludeView ExcludeView
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function includeView($excludeView)
    {
        $this->em->remove($excludeView);
        $this->em->flush();
    }

    /**
     * @param $view View
     * @return bool
     */
    public function isExcluded($view)
    {
        if($this->em->getRepository('CLICHESPlayerBundle:ExcludeView')->findOneBy(['view' => $view]) != null) {
            return true;
        } else {
            return false;
        }
    }
}

<?php

namespace CLICHES\PlayerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PlayerProposalLoadChoiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach($options['attr']['listChoices'] as $field => $choices) {
            $builder
                ->add($field,   ChoiceType::class,   array(
                    'choices' => $choices,
                    'label' => lcfirst($field),
                    'multiple' => false,
                    'expanded' => true,
                    'required' => false,
                    'empty_data' => false,
                    'mapped' => false))
            ;
        }
    }
        
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CLICHES\PlayerBundle\Entity\PlayerProposal'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cliches_player_playerproposal_loadchoice_type';
    }
}

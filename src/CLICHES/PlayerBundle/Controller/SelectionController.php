<?php

namespace CLICHES\PlayerBundle\Controller;

use CLICHES\PlayerBundle\Entity\PlayerSession;
use CLICHES\PlayerBundle\Service\playerSelectionService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use Symfony\Component\Routing\Annotation\Route;

class SelectionController extends Controller
{
    /**
     * Méthode sélectionnant l'oeuvre à afficher
     * @Route(
     *     "/{playerSession_id}",
     *     name="cliches_player_selection_selection",
     *     requirements={
     *          "playerSession_id"="\d+"
     *     },
     *     options={
     *          "expose"=true,
     *          "utf8"=true
     *     }
     * )
     * @param $playerSession_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws
     */
    public function selectionAction($playerSession_id)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var PlayerSession $playerSession */
        $playerSession = $em->getRepository('CLICHESPlayerBundle:PlayerSession')->findOneBy(['id' => $playerSession_id]);
        if ($playerSession === null) {throw $this->createNotFoundException('Session : [id='.$playerSession_id.'] inexistante.');}

        if(!empty($playerSession->getDateEnd()) AND date_diff(new \DateTime(), $playerSession->getDateEnd())->format('%h') > 0) {
            $this->get('session')->getFlashBag()->add('notice', 'Oups... Cette session a expiré ...' );
            return $this->redirectToRoute('cliches_player_end_end', array('playerSession_id' => $playerSession->getId()));
        }
        
        $passedPlayerOeuvres = $em->getRepository('CLICHESPlayerBundle:PlayerOeuvre')->findBy(['playerSession' => $playerSession]);

        /** @var playerSelectionService $playerSelectionService */
        $playerSelectionService = $this->get('cliches_player.player_selection');

        if($playerSession->getTeaching() != null) {
            $array_result = $playerSelectionService->getRandViewForTeaching($playerSession->getTeaching(), $passedPlayerOeuvres);
        } elseif($playerSession->getPrivatePlayer() != null) {
            $array_result = $playerSelectionService->getRandViewForPrivatePlayer($playerSession->getPrivatePlayer(), $passedPlayerOeuvres);
        }

        if($array_result[0] == 'no_enough_entities' OR $array_result[0] == 'no_entities') {
            $this->get('session')->getFlashBag()->add('notice', 'Oups... Il n\'a pas assez d\'oeuvres pour continuer.' );
            return $this->redirectToRoute('cliches_player_end_end', array('playerSession_id' => $playerSession->getId()));
        } elseif($array_result[0] == 'view') {
            $playerOeuvre = new PlayerOeuvre;
            $playerOeuvre->setPlayerSession($playerSession);
            $playerOeuvre->setView($array_result[1]);
            $em->persist($playerOeuvre);

            $playerSession->setDateEnd(new \DateTime());
            $em->persist($playerSession);

            $em->flush();

            return $this->redirectToRoute('cliches_player_proposal_proposal', ['playerOeuvre_id' => $playerOeuvre->getId()]);
        }
    }
}

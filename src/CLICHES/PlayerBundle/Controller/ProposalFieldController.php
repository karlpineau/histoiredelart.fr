<?php

namespace CLICHES\PlayerBundle\Controller;

use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use CLICHES\PlayerBundle\Entity\PlayerProposal;
use CLICHES\PlayerBundle\Entity\PlayerProposalField;
use CLICHES\PlayerBundle\Form\PlayerProposalLoadFieldType;
use DATA\ImageBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProposalFieldController extends Controller
{
    /**
     * @Route(
     *     "/proposition/champ/{playerOeuvre_id}",
     *     name="cliches_player_proposalfield_proposalfield",
     *     requirements={
     *          "playerOeuvre_id"="\d+"
     *     },
     *     options={
     *          "utf8"=true
     *     }
     * )
     * @param int $playerOeuvre_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function proposalFieldAction($playerOeuvre_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryPlayerOeuvre = $em->getRepository('CLICHESPlayerBundle:PlayerOeuvre');
        /** @var PlayerOeuvre $playerOeuvre */
        $playerOeuvre = $repositoryPlayerOeuvre->findOneBy(['id' => $playerOeuvre_id]);
        if ($playerOeuvre === null) {throw $this->createNotFoundException('Session : [id='.$playerOeuvre_id.'] inexistante.');}

        $playerProposal = new PlayerProposal();
        $playerProposal->setPlayerOeuvre($playerOeuvre);

        $repositoryImage = $em->getRepository('DATAImageBundle:Image');
        /** @var Image $image */
        $image = $repositoryImage->findOneBy(['view' => $playerProposal->getPlayerOeuvre()->getView()]);

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->addMeta('property', 'og:title', 'Clichés!')
            ->addMeta('property', 'og:description', 'Aidez-moi à reconnaitre ce cliché !')
            ->addMeta('property', 'og:image', $this->get('assets.packages')->getUrl('uploads/gallery/'.$image->getMediumFileName()))
        ;

        //Si il existe déjà un playerProposal pour ce playerOeuvre (autrement dit on a déjà soumis un resultat), on bloque l'affichage du formulaire et on redirige vers le résultat déjà soumis
        $playerProposalCheck = $em->getRepository('CLICHESPlayerBundle:PlayerProposal')->findOneBy(['playerOeuvre' => $playerOeuvre]);
        if($playerProposalCheck != null) {
            return $this->redirect($this->generateUrl('cliches_player_result_result', ['playerProposal_id' => $playerProposalCheck->getId()]));
        }

        $form = $this->createForm(PlayerProposalLoadFieldType::class, $playerProposal);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var PlayerProposalField[] $playerProposalFields */
            $playerProposalFields = $playerProposal->getPlayerProposalFields();
            foreach ($playerProposalFields as $playerProposalField) {
                $playerProposalField->setPlayerProposal($playerProposal);
                $em->persist($playerProposalField);
            }

            $em->persist($playerProposal);
            $em->flush();

            return $this->redirect($this->generateUrl('cliches_player_result_result', ['playerProposal_id' => $playerProposal->getId()]));
        }

        return $this->render('CLICHESPlayerBundle:Proposal:proposal.html.twig', array(
            'proposalType' => 'field',
            'form' => $form->createView(),
            'playerOeuvre' => $playerOeuvre,
            'playerProposal' => $playerProposal,
            'image' => $image
        ));
    }

    /**
     * @Route(
     *     "/partie/proposition/champ/ajax/{idImg}",
     *     name="cliches_player_proposalfield_getFieldsAjax",
     *     options={
     *          "expose"=true,
     *          "utf8"=true
     *     }
     * )
     * @param int $idImg
     * @param Request $request
     * @return Response
     */
    public function getPropertiesAjaxAction($idImg, Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            return new Response($this->get('cliches_player.player_proposal_field')->foundProperties($idImg));
        }
    }
}

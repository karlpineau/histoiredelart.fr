<?php

namespace CLICHES\PlayerBundle\Controller;

use CLICHES\PlayerBundle\Entity\PlayerEndViews;
use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use CLICHES\PlayerBundle\Entity\PlayerSession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class EndController extends Controller
{
    /**
     * @Route(
     *     "/fin/{playerSession_id}",
     *     name="cliches_player_end_end",
     *     requirements={
     *          "playerSession_id"="\d+"
     *     },
     *     options={
     *          "utf8"=true
     *     }
     * )
     * @param $playerSession_id int
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function endAction($playerSession_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryPlayerSession = $em->getRepository('CLICHESPlayerBundle:PlayerSession');
        /** @var PlayerSession $playerSession */
        $playerSession = $repositoryPlayerSession->findOneBy(['id' => $playerSession_id]);
        
        if ($playerSession === null) {
            throw $this->createNotFoundException('Session : [id='.$playerSession_id.'] inexistante.');
        }
        $playerSession->setDateEnd(new \DateTime("now"));
        $em->persist($playerSession);
        
        $em->flush();

        $repositoryPlayerOeuvre = $em->getRepository('CLICHESPlayerBundle:PlayerOeuvre');
        $playerOeuvres = $repositoryPlayerOeuvre->findBy(['playerSession' => $playerSession]);

        $playerOeuvresCollection = array();
        /**
         * @var int $key
         * @var PlayerOeuvre $playerOeuvre
         */
        foreach($playerOeuvres as $key => $playerOeuvre) {
            $repositoryPlayerProposal = $em->getRepository('CLICHESPlayerBundle:PlayerProposal');
            $playerProposal = $repositoryPlayerProposal->findOneBy(['playerOeuvre' => $playerOeuvre]);

            if($playerOeuvre->getPlayerSession()->getProposalType() == 'modeField') {
                $servicePlayerResult = $this->container->get('cliches_player.player_result');
                if ($playerProposal != null AND $servicePlayerResult->testFilled($playerProposal) == true) {
                    $playerOeuvresCollection[$key] = ['playerOeuvre' => $playerOeuvre, 'playerProposal' => $servicePlayerResult->foundResults($playerProposal->getId(), false)];
                } else {
                    $playerOeuvresCollection[$key] = ['playerOeuvre' => $playerOeuvre, 'playerProposal' => null];
                }
            } elseif($playerOeuvre->getPlayerSession()->getProposalType() == 'modeChoice') {
                $playerOeuvresCollection[$key] = ['playerOeuvre' => $playerOeuvre, 'playerProposal' => $playerProposal];
            } elseif($playerOeuvre->getPlayerSession()->getProposalType() == 'modeTest') {
                $playerOeuvresCollection[$key] = ['playerOeuvre' => $playerOeuvre, 'playerProposal' => $playerProposal];
            }
        }


        return $this->render('CLICHESPlayerBundle:End:endGame.html.twig', ['playerOeuvres' => $playerOeuvresCollection]);
    }
}

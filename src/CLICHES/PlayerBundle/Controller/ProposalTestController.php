<?php

namespace CLICHES\PlayerBundle\Controller;

use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use DATA\ImageBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProposalTestController extends Controller
{
    /**
     * @Route(
     *     "/proposition/examen/{playerOeuvre_id}",
     *     name="cliches_player_proposaltest_proposaltest",
     *     requirements={
     *          "playerOeuvre_id"="\d+"
     *     },
     *     options={
     *          "utf8"=true
     *     }
     * )
     * @param int $playerOeuvre_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function proposalTestAction($playerOeuvre_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryPlayerOeuvre = $em->getRepository('CLICHESPlayerBundle:PlayerOeuvre');
        /** @var PlayerOeuvre $playerOeuvre */
        $playerOeuvre = $repositoryPlayerOeuvre->findOneBy(['id' => $playerOeuvre_id]);
        if ($playerOeuvre === null) {throw $this->createNotFoundException('Session : [id='.$playerOeuvre_id.'] inexistante.');}

        $repositoryImage = $em->getRepository('DATAImageBundle:Image');
        /** @var Image $image */
        $image = $repositoryImage->findOneBy(['view' => $playerOeuvre->getView()]);

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->addMeta('property', 'og:title', 'Clichés!')
            ->addMeta('property', 'og:description', 'Aidez-moi à reconnaitre ce cliché !')
            ->addMeta('property', 'og:image', $this->get('assets.packages')->getUrl('uploads/gallery/'.$image->getMediumFileName()))
        ;

        return $this->render('CLICHESPlayerBundle:Proposal:visualisation.html.twig', array(
            'playerSession' => $playerOeuvre->getPlayerSession(),
            'image' => $image,
            'view' => $playerOeuvre->getView()
        ));
    }
}

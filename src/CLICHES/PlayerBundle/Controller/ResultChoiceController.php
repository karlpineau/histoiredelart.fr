<?php

namespace CLICHES\PlayerBundle\Controller;

use CLICHES\PlayerBundle\Entity\PlayerProposal;
use CLICHES\PlayerBundle\Entity\PlayerProposalChoice;
use DATA\DataBundle\Entity\EntityProperty;
use DATA\DataBundle\Service\entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ResultChoiceController extends Controller
{
    /**
     * @Route(
     *     "/resultat/choix/{playerProposal_id}",
     *     name="cliches_player_resultchoice_result",
     *     requirements={
     *          "playerProposal_id"="\d+"
     *     },
     *     options={
     *          "utf8"=true
     *     }
     * )
     * @param int $playerProposal_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resultAction($playerProposal_id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var PlayerProposal $playerProposal */
        $playerProposal = $em->getRepository('CLICHESPlayerBundle:PlayerProposal')->findOneBy(['id' => $playerProposal_id]);
        if ($playerProposal === null) { throw $this->createNotFoundException('Session : [id='.$playerProposal_id.'] inexistante.'); }

        /** @var \DATA\DataBundle\Entity\Entity $entity */
        $entity = $playerProposal->getPlayerOeuvre()->getView()->getEntity();

        /** @var entity $entityService */
        $entityService = $this->container->get('data_data.entity');

        $image = $em->getRepository('DATAImageBundle:Image')->findOneBy(['view' => $playerProposal->getPlayerOeuvre()->getView()]);
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->addMeta('property', 'og:title', 'Clichés!')
            ->addMeta('property', 'og:description', 'Aidez-moi à reconnaitre ce cliché !')
            ->addMeta('property', 'og:image', $this->get('assets.packages')->getUrl('uploads/gallery/'.$image->getMediumFileName()))
        ;

        $playerProposalChoices = $em->getRepository('CLICHESPlayerBundle:PlayerProposalChoice')->findBy(['playerProposal' => $playerProposal]);

        // Sélection des données à afficher :
        $sources = $entityService->getSources($playerProposal->getPlayerOeuvre()->getView()->getEntity());
        /** @var EntityProperty[] $properties */
        if($entity->getEnrichmentStatus() == 1) {
            $properties = $entityService->getProperties($entity, "wd", true);
        } else {
            $properties = $entityService->getProperties($entity, "hda", true);
        }

        /** @var PlayerProposalChoice $playerProposalChoice */
        foreach($playerProposalChoices as $playerProposalChoice) {
            if($playerProposalChoice->getPlayerProposalChoiceValueSelected() == null or $playerProposalChoice->getPlayerProposalChoiceValueSelected()->getIsTrue() == false) {
                $playerProposalChoice->setCorrectAnswer(false);
            } elseif($playerProposalChoice->getPlayerProposalChoiceValueSelected()->getIsTrue() == true) {
                $playerProposalChoice->setCorrectAnswer(true);
            }
        }
        $em->flush();

        return $this->render('CLICHESPlayerBundle:Result:result.html.twig', array(
            'isSuggest' => false,
            'isVote' => false,
            'isError' => false,
            'playerProposal' => $playerProposal,
            'playerProposalChoices' => $playerProposalChoices,
            'entity' => $entity,
            'sources' => $sources,
            'properties' => $properties
        ));
    }
}

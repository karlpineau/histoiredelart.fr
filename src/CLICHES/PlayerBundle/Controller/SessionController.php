<?php

namespace CLICHES\PlayerBundle\Controller;

use CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer;
use DATA\TeachingBundle\Entity\Teaching;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CLICHES\PlayerBundle\Entity\PlayerSession;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SessionController extends Controller
{
    /**
     * @Route(
     *     "/chargement/{type}/{id}/{mode}",
     *     name="cliches_player_session_load",
     *     requirements={
     *          "teaching_id"="\d+",
     *          "mode"="\S{0,255}"
     *     },
     *     options={
     *          "utf8"=true
     *     }
     * )
     * @param string $type
     * @param int $id
     * @param string $mode
     * @throws
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loadAction($type, $id, $mode, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $teaching = null;
        $privatePlayer = null;

        if($type == "teaching") {
            $repositoryTeaching = $em->getRepository('DATATeachingBundle:Teaching');
            /** @var Teaching $teaching */
            $teaching = $repositoryTeaching->findOneBy(['id' => $id]);
            if ($teaching === null) {throw $this->createNotFoundException('Enseignement : [id='.$id.'] inexistant.');}
        } elseif($type == "privatePlayer") {
            $repositoryPrivatePlayer = $em->getRepository('CLICHESPersonalPlaceBundle:PrivatePlayer');
            /** @var PrivatePlayer $privatePlayer */
            $privatePlayer = $repositoryPrivatePlayer->findOneBy(['id' => $id]);
            if ($privatePlayer === null) {throw $this->createNotFoundException('Collection : [id='.$id.'] inexistante.');}
        }

        $session = new PlayerSession;
        if($this->getUser() != null) {$session->setCreateUser($this->getUser());}
        $session->setIpPlayerUser($request->getClientIp());
        $session->setHTTPUSERAGENT($_SERVER['HTTP_USER_AGENT']);
        $session->setProposalType($mode);
        $session->setContext('webapp');
        if($mode == 'modeTest') {$session->setSimpleSession(true);} else {$session->setSimpleSession(false);}

        if($teaching != null) {
            $session->setTeaching($teaching);
        } else if($privatePlayer != null) {
            $session->setPrivatePlayer($privatePlayer);
        }
        $em->persist($session);
        $em->flush();

        return $this->redirect($this->generateUrl('cliches_player_selection_selection', ['playerSession_id' => $session->getId()]));
    }

    /**
     * @Route(
     *     "/force/fin/{session_id}",
     *     name="cliches_player_session_forcedEnd",
     *     requirements={
     *          "session_id"="\d+"
     *     },
     *     options={
     *          "utf8"=true
     *     }
     * )
     * @param int $session_id
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response
     * @throws
     */
    public function forcedEndAction($session_id, Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            $em = $this->getDoctrine()->getManager();
            $repositorySource = $em->getRepository('CLICHESPlayerBundle:PlayerSession');
            /** @var PlayerSession $session */
            $session = $repositorySource->findOneBy(['id' => $session_id]);

            if ($session !== null AND empty($session->getDateEnd())) {
                $session->setDateEnd(new \DateTime());
                $em->persist($session);
                $em->flush();

                $response = new Response(json_encode("ok"));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }


        }
    }
}

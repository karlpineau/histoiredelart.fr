<?php

namespace CLICHES\PlayerBundle\Controller;

use CLICHES\PlayerBundle\Entity\PlayerProposal;
use CLICHES\PlayerBundle\Service\playerResultService;
use DATA\DataBundle\Entity\EntityProperty;
use DATA\DataBundle\Entity\Source;
use DATA\DataBundle\Service\entity;
use DATA\PublicBundle\Entity\Reporting;
use DATA\PublicBundle\Form\ReportingType;
use DATA\TeachingBundle\Entity\TeachingTest;
use DATA\TeachingBundle\Entity\TeachingTestVote;
use DATA\TeachingBundle\Form\TeachingTestVoteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ResultFieldController extends Controller
{
    /**
     * Méthode d'affichage des résultats une fois le formulaire rempli
     * @Route(
     *     "/resultat/champ/{playerProposal_id}/{validation}",
     *     name="cliches_player_resultfield_result",
     *     requirements={
     *          "playerProposal_id"="\d+",
     *          "validation"="\S{0,255}"
     *     },
     *     options={
     *          "utf8"=true
     *     }
     * )
     * @param int $playerProposal_id
     * @param Request $request
     * @param null $validation
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function resultAction($playerProposal_id, Request $request, $validation=null)
    {
        $em = $this->getDoctrine()->getManager();

        $repositoryPlayerProposal = $em->getRepository('CLICHESPlayerBundle:PlayerProposal');
        /** @var PlayerProposal $playerProposal */
        $playerProposal = $repositoryPlayerProposal->findOneBy(['id' => $playerProposal_id]);
        if ($playerProposal === null) { throw $this->createNotFoundException('Session : [id='.$playerProposal_id.'] inexistante.'); }

        /** @var \DATA\DataBundle\Entity\Entity $entity */
        $entity = $playerProposal->getPlayerOeuvre()->getView()->getEntity();

        /** @var entity $entityService */
        $entityService = $this->container->get('data_data.entity');

        // Sélection des données à afficher :
        $sources = $entityService->getSources($playerProposal->getPlayerOeuvre()->getView()->getEntity());
        /** @var EntityProperty[] $properties */
        if($entity->getEnrichmentStatus() == 1) {
            $properties = $entityService->getProperties($entity, "wd", true);
        } else {
            $properties = $entityService->getProperties($entity, "hda", true);
        }

        $repositoryImage = $em->getRepository('DATAImageBundle:Image');
        $image = $repositoryImage->findOneBy(['view' => $playerProposal->getPlayerOeuvre()->getView()]);
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->addMeta('property', 'og:title', 'Clichés!')
            ->addMeta('property', 'og:description', 'Aidez-moi à reconnaitre ce cliché !')
            ->addMeta('property', 'og:image', $this->get('assets.packages')->getUrl('uploads/gallery/'.$image->getMediumFileName()))
        ;

        // Gestion des votes :
        if($this->getUser() != null and $this->get('data_teaching.teaching_test_vote')->checkVote($playerProposal->getPlayerOeuvre()->getPlayerSession()->getTeaching(),
               $playerProposal->getPlayerOeuvre()->getView(),
               $this->getUser()) == null) {
            // SI l'utilisateur n'a pas exprimé de vote sur ce couple vue / matière
            $voteBoolean = true;
        } elseif($this->getUser() == null) {
            // SI l'utilisateur est inconnu
            $voteBoolean = true;
        } else {
            // SINON
            $voteBoolean = false;
        }

        if($voteBoolean == true and $playerProposal->getPlayerOeuvre()->getPlayerSession()->getTeaching() != null) {
            $teaching = $playerProposal->getPlayerOeuvre()->getPlayerSession()->getTeaching();

            //Définition du TeachingTest
            if($em->getRepository('DATATeachingBundle:TeachingTest')->findOneBy(array('teaching' => $teaching, 'view' => $playerProposal->getPlayerOeuvre()->getView())) == null) {
                $teachingTest = new TeachingTest();
                $teachingTest->setTeaching($teaching);
                $teachingTest->setView($playerProposal->getPlayerOeuvre()->getView());
                $em->persist($teachingTest);
                $em->flush();
            } else {
                $teachingTest = $em->getRepository('DATATeachingBundle:TeachingTest')->findOneBy(array('teaching' => $teaching, 'view' => $playerProposal->getPlayerOeuvre()->getView()));
            }

            // Lancement TeachingTestVote
            $teachingTestVote = new TeachingTestVote();
            $teachingTestVote->setTeachingTest($teachingTest);
            $formVote = $this->createForm(TeachingTestVoteType::class, $teachingTestVote);
        }

        // Gestion des reportings :
        $reporting = new Reporting();
        $reporting->setCreateUser($this->getUser());
        $reporting->setTraitement(false);
        $reporting->setEntity($entity);
        $formReporting = $this->createForm(ReportingType::class, $reporting);

        return $this->render('CLICHESPlayerBundle:Result:result.html.twig', array(
            'isVote' => false,
            //'formVote' => $formVote->createView(),
            //'teachingTest' => $teachingTest,
            'formReporting' => $formReporting->createView(),
            'playerProposal' => $playerProposal,
            'entity' => $entity,
            'sources' => $sources,
            'properties' => $properties
        ));
    }

    /**
     * Fonction retournant les champs sous format JSON
     * @Route(
     *     "/partie/resultats/champ/{playerProposal_id}",
     *     name="cliches_player_resultfield_results",
     *     options={
     *          "expose"=true,
     *          "utf8"=true
     *     }
     * )
     * @param int $playerProposal_id
     * @param Request $request
     * @return Response
     */
    public function resultsAction($playerProposal_id, Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            return new Response($this->get('cliches_player.player_result')->foundResults($playerProposal_id));
        }
    }
}

<?php

namespace CLICHES\PlayerBundle\Controller;

use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use CLICHES\PlayerBundle\Entity\PlayerProposal;
use CLICHES\PlayerBundle\Entity\PlayerProposalChoice;
use CLICHES\PlayerBundle\Entity\PlayerProposalChoiceValue;
use CLICHES\PlayerBundle\Form\PlayerProposalLoadChoiceType;
use CLICHES\PlayerBundle\Service\playerProposalChoiceService;
use DATA\DataBundle\Service\entity;
use DATA\DataBundle\Service\property;
use DATA\ImageBundle\Entity\Image;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProposalChoiceController extends Controller
{
    public function shuffleAssoc($list) {
        if (!is_array($list)) return $list;

        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $list[$key];
        }
        return $random;
    }

    /**
     * @Route(
     *     "/proposition/choix/{playerOeuvre_id}",
     *     name="cliches_player_proposalchoice_proposalchoice",
     *     requirements={
     *          "playerOeuvre_id"="\d+"
     *     },
     *     options={
     *          "utf8"=true
     *     }
     * )
     * @param int $playerOeuvre_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function proposalChoiceAction($playerOeuvre_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var PlayerOeuvre $playerOeuvre */
        $playerOeuvre = $em->getRepository('CLICHESPlayerBundle:PlayerOeuvre')->findOneBy(['id' => $playerOeuvre_id]);
        if ($playerOeuvre === null) {throw $this->createNotFoundException('Session : [id='.$playerOeuvre_id.'] inexistante.');}

        $image = $em->getRepository('DATAImageBundle:Image')->findOneBy(['view' => $playerOeuvre->getView()]);
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->addMeta('property', 'og:title', 'Clichés!')
            ->addMeta('property', 'og:description', 'Aidez-moi à reconnaitre ce cliché !')
            ->addMeta('property', 'og:image', $this->get('assets.packages')->getUrl('uploads/gallery/'.$image->getMediumFileName()))
        ;

        $repositoryPlayerProposal = $em->getRepository('CLICHESPlayerBundle:PlayerProposal');
        if($repositoryPlayerProposal->findOneBy(['playerOeuvre' => $playerOeuvre]) == null) {
            return $this->generateNewPlayerProposalChoices($playerOeuvre, $request);
        } else {
            /** @var PlayerProposal $playerProposal */
            $playerProposal = $repositoryPlayerProposal->findOneBy(['playerOeuvre' => $playerOeuvre]);

            $listChoices = array();

            /** @var PlayerProposalChoice $playerProposalChoice */
            foreach($em->getRepository('CLICHESPlayerBundle:PlayerProposalChoice')->findBy(['playerProposal' =>$playerProposal]) as $playerProposalChoice) {
                $arrayValues = [];
                /** @var PlayerProposalChoiceValue $choiceValue */
                foreach($em->getRepository('CLICHESPlayerBundle:PlayerProposalChoiceValue')->findBy(['playerProposalChoice' => $playerProposalChoice]) as $choiceValue) {
                    $arrayValues[$choiceValue->getId()] = $choiceValue->getValue();
                }
                $arrayValues = $this->shuffleAssoc($arrayValues);
                $listChoices[$playerProposalChoice->getField()] = $arrayValues;
            }

            $form = $this->createForm(PlayerProposalLoadChoiceType::class, $playerProposal, ['attr' => ['listChoices' => $listChoices]]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                return $this->validationResults($playerProposal, $form);
            } else {
                return $this->reloadPlayerProposalChoice($playerProposal, $form);
            }
        }
    }

    /**
     * COMPATIBILITE : 3.0
     * FONCTION : Génère un lot de questions à choix multiples pour un PlayerProposal
     *
     * @param PlayerOeuvre $playerOeuvre
     * @param Request $request
     * @return Response
     */
    public function generateNewPlayerProposalChoices($playerOeuvre, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var entity $entityService */
        $entityService = $this->get('data_data.entity');
        /** @var property $propertyService */
        $propertyService = $this->get('data_data.property');
        /** @var playerProposalChoiceService $playerProposalChcoiceService */
        $playerProposalChcoiceService = $this->get('cliches_player.player_proposal_choice');
        /** @var Image $image */
        $image = $em->getRepository('DATAImageBundle:Image')->findOneBy(['view' => $playerOeuvre->getView()]);
        /** @var \DATA\DataBundle\Entity\Entity $entity */
        $entity = $entityService->getByView($playerOeuvre->getView());
        /** @var LoggerInterface $logger */
        $logger = $this->get('logger');

        $playerProposal = new PlayerProposal();
        $playerProposal->setPlayerOeuvre($playerOeuvre);

        $listChoices = array();
        foreach($propertyService->getProperties() as $propertyName => $propertyInformation) {
            $question = $playerProposalChcoiceService->getValues($playerOeuvre, $propertyName, 3);
            if($question != false) {
                $listChoices[$propertyName] = $question;
            }
        }

        $em->persist($playerProposal);
        $listChoicesRebuilt = array();
        foreach($listChoices as $field => $choiceContainer) {
            $playerProposalChoice = new PlayerProposalChoice();
            $playerProposalChoice->setPlayerProposal($playerProposal);
            $playerProposalChoice->setField($field);
            $em->persist($playerProposalChoice);
            $listChoicesRebuiltContent = array();
            $fieldChoice = '';

            foreach($choiceContainer as $choice) {
                $playerProposalChoiceValue = new PlayerProposalChoiceValue();
                $playerProposalChoiceValue->setEntity($choice['entity']);
                $playerProposalChoiceValue->setField($field);
                $playerProposalChoiceValue->setValue($choice['value']);
                $playerProposalChoiceValue->setIsTrue($choice['quizz']);
                $playerProposalChoiceValue->setPlayerProposalChoice($playerProposalChoice);
                $em->persist($playerProposalChoiceValue);
                $em->flush();

                $listChoicesRebuiltContent[$playerProposalChoiceValue->getId()] = $choice['value'];
                $fieldChoice = $field;
            }
            $listChoicesRebuiltContent = $this->shuffleAssoc($listChoicesRebuiltContent);
            $listChoicesRebuilt[$fieldChoice] = $listChoicesRebuiltContent;
        }


        $form = $this->createForm(PlayerProposalLoadChoiceType::class, $playerProposal, ['attr' => ['listChoices' => $listChoicesRebuilt]]);
        $form->handleRequest($request);

        return $this->render('CLICHESPlayerBundle:Proposal:proposal.html.twig', array(
            'proposalType' => 'choice',
            'form' => $form->createView(),
            'playerOeuvre' => $playerOeuvre,
            'playerProposal' => $playerProposal,
            'image' => $image
        ));
    }

    /**
     * @param PlayerProposal $playerProposal
     * @param FormInterface $form
     * @return Response
     */
    public function reloadPlayerProposalChoice($playerProposal, $form)
    {
        $em = $this->getDoctrine()->getManager();
        $playerOeuvre = $playerProposal->getPlayerOeuvre();
        $image = $em->getRepository('DATAImageBundle:Image')->findOneBy(['view' => $playerOeuvre->getView()]);

        return $this->render('CLICHESPlayerBundle:Proposal:proposal.html.twig', array(
            'proposalType' => 'choice',
            'form' => $form->createView(),
            'playerOeuvre' => $playerOeuvre,
            'playerProposal' => $playerProposal,
            'image' => $image
        ));
    }

    /**
     * @param PlayerProposal $playerProposal
     * @param $form
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function validationResults($playerProposal, $form)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var PlayerProposalChoice $playerProposalChoice */
        foreach($em->getRepository('CLICHESPlayerBundle:PlayerProposalChoice')->findBy(['playerProposal' => $playerProposal]) as $playerProposalChoice) {
            /** @var PlayerProposalChoiceValue $playerProposalChoiceValue */
            $playerProposalChoiceValue = $em->getRepository('CLICHESPlayerBundle:PlayerProposalChoiceValue')->findOneBy(['id' => $form->get($playerProposalChoice->getField())->getData()]);
            $playerProposalChoice->setPlayerProposalChoiceValueSelected($playerProposalChoiceValue);
            $em->persist($playerProposalChoice);
        }
        $em->persist($playerProposal);
        $em->flush();

        return $this->redirect($this->generateUrl('cliches_player_result_result', ['playerProposal_id' => $playerProposal->getId()]));
    }

    /**
     * @Route(
     *     "/partie/proposition/choix/ajax/champ/{field}",
     *     name="cliches_player_proposalchoice_getFieldsAjax",
     *     options={
     *          "expose"=true,
     *          "utf8"=true
     *     }
     * )
     * @param $field string
     * @param Request $request
     * @return Response
     */
    public function getFieldsAjaxAction($field, Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            return new Response(json_encode($this->get('cliches_player.player_result')->getLabel($field)));
        }

    }
}

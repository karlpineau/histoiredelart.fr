<?php

namespace CLICHES\PlayerBundle\Controller;

use CLICHES\PlayerBundle\Entity\PlayerProposal;
use CLICHES\PlayerBundle\Form\PlayerSuggestEnrichedType;
use DATA\TeachingBundle\Entity\TeachingTest;
use DATA\TeachingBundle\Entity\TeachingTestVote;
use DATA\TeachingBundle\Form\TeachingTestVoteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CLICHES\PlayerBundle\Entity\PlayerSuggest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ResultController extends Controller
{
    /**
     * Méthode d'affichage des résultats une fois le formulaire rempli
     * @Route(
     *     "/resultat/{playerProposal_id}",
     *     name="cliches_player_result_result",
     *     requirements={
     *          "playerProposal_id"="\d+"
     *     },
     *     options={
     *          "utf8"=true
     *     }
     * )
     * @param int $playerProposal_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function resultAction($playerProposal_id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var PlayerProposal $playerProposal */
        $playerProposal = $em->getRepository('CLICHESPlayerBundle:PlayerProposal')->findOneBy(['id' => $playerProposal_id]);
        if ($playerProposal === null) { throw $this->createNotFoundException('Session : [id='.$playerProposal_id.'] inexistante.'); }

        switch ($playerProposal->getPlayerOeuvre()->getPlayerSession()->getProposalType()) {
            case 'modeField':
                return $this->redirectToRoute('cliches_player_resultfield_result', array('playerProposal_id' => $playerProposal->getId()));
                break;
            case 'modeChoice':
                return $this->redirectToRoute('cliches_player_resultchoice_result', array('playerProposal_id' => $playerProposal->getId()));
                break;
        }
    }
}

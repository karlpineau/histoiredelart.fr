<?php

namespace CLICHES\PlayerBundle\Controller;

use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ProposalController extends Controller
{
    /**
     * @Route(
     *     "/proposal/{playerOeuvre_id}",
     *     name="cliches_player_proposal_proposal",
     *     requirements={
     *          "playerOeuvre_id"="\d+"
     *     },
     *     options={
     *          "utf8"=true
     *     }
     * )
     * @param int $playerOeuvre_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function proposalAction($playerOeuvre_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryPlayerOeuvre = $em->getRepository('CLICHESPlayerBundle:PlayerOeuvre');
        /** @var PlayerOeuvre $playerOeuvre */
        $playerOeuvre = $repositoryPlayerOeuvre->findOneBy(['id' => $playerOeuvre_id]);
        
        if ($playerOeuvre === null) {throw $this->createNotFoundException('Session : [id='.$playerOeuvre_id.'] inexistante.');}

        switch ($playerOeuvre->getPlayerSession()->getProposalType()) {
            case 'modeTest':
                return $this->redirectToRoute('cliches_player_proposaltest_proposaltest', ['playerOeuvre_id' => $playerOeuvre->getId()]);
                break;
            case 'modeField':
                return $this->redirectToRoute('cliches_player_proposalfield_proposalfield', ['playerOeuvre_id' => $playerOeuvre->getId()]);
                break;
            case 'modeChoice':
                return $this->redirectToRoute('cliches_player_proposalchoice_proposalchoice', ['playerOeuvre_id' => $playerOeuvre->getId()]);
                break;
        }

        throw $this->createNotFoundException($playerOeuvre->getPlayerSession()->getProposalType());

    }
}

<?php

namespace CLICHES\PersonalPlaceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrivatePlayerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',                  TextType::class,            array('required' => true))
            ->add('publicPrivatePlayer',    CheckboxType::class,        array(
                'required' => false,
                'label' => 'Autoriser Clichés! à réutiliser cette collection',
                'attr' => ['class' => 'custom-control-input'],
                'label_attr' => ['class' => 'custom-control-label'],
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cliches_personalplacebundle_privateplayer';
    }
}

<?php

namespace CLICHES\PersonalPlaceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrivatePlayerEntityType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('entity',         EntityType::class,      array(
                'class' => 'DATADataBundle:Entity',
                'choice_label' => 'id',
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CLICHES\PersonalPlaceBundle\Entity\PrivatePlayerEntity'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cliches_personalplacebundle_privateplayerentity';
    }
}

<?php

namespace CLICHES\PersonalPlaceBundle\Form;

use CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddToPrivatePlayerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('privatePlayers',   EntityType::class,   array(
                'class' => 'CLICHESPersonalPlaceBundle:PrivatePlayer',
                'choice_label' => function($privatePlayer) use($options) {
                    /** @var PrivatePlayer $privatePlayer */
                    return $privatePlayer->getTitle();
                },
                'query_builder' => function (EntityRepository $er) use($options) {
                    return $er->createQueryBuilder('t')
                        ->where('t.createUser = :user')
                        ->setParameters(array('user' => $options['user']))
                        ->orderBy('t.title', 'ASC');
                },
                'choice_attr' => function($privatePlayer, $key, $value) use($options) {
                    $disabled = false;
                    /** @var PrivatePlayer $privatePlayer */
                    foreach ($privatePlayer->getViews() as $view) {
                        if($view == $options['view']) {
                            $disabled = true;
                        }
                    }

                    return $disabled ? ['disabled' => 'disabled'] : [];
                },
                'required' => true,
                'placeholder' => 'Sélectionnez une collection ...',
                'mapped' => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(array('user', 'view', 'privateplayer_service'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cliches_personalplacebundle_privateplayerentity';
    }
}

<?php

namespace CLICHES\PersonalPlaceBundle\Service;

use CAS\UserBundle\Entity\User;
use CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer;
use CLICHES\PlayerBundle\Entity\PlayerSession;
use DATA\ImageBundle\Entity\View;
use DATA\TeachingBundle\Entity\Teaching;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

class privatePlayerService
{
    protected $em;
    protected $logger;

    public function __construct(EntityManager $EntityManager, LoggerInterface $logger)
    {
        $this->em = $EntityManager;
        $this->logger = $logger;
    }

    /**
     * @param View $view
     * @param User $user
     * @return array
     */
    public function getByView($view, $user) {
        $selectedPrivatePlayers = array();
        $privatePlayers = $this->em->getRepository('CLICHESPersonalPlaceBundle:PrivatePlayer')->findBy(['createUser' => $user]);

        /** @var PrivatePlayer $privatePlayer */
        foreach ($privatePlayers as $privatePlayer) {
            foreach ($privatePlayer->getViews() as $viewP) {
                if($viewP == $view) {
                    $selectedPrivatePlayers[] = $privatePlayer;
                }
            }
        }

        return $selectedPrivatePlayers;
    }

    /**
     * @param PrivatePlayer $privatePlayer
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove($privatePlayer) {
        foreach ($privatePlayer->getViews() as $view) {
            $privatePlayer->removeView($view);
        }

        $this->em->remove($privatePlayer);
        $this->em->flush();
    }
}

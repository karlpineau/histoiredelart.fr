<?php

namespace CLICHES\PersonalPlaceBundle\Controller;

use CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer;
use CLICHES\PersonalPlaceBundle\Form\AddToPrivatePlayerType;
use CLICHES\PersonalPlaceBundle\Form\PrivatePlayerType;
use DATA\ImageBundle\Entity\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PrivatePlayerController extends Controller
{
    /**
     * @Route(
     *     "/collections",
     *     name="cliches_personalplace_privateplayer_index"
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $privatePlayers = $em->getRepository('CLICHESPersonalPlaceBundle:PrivatePlayer')->findBy(['createUser' => $user]);

        return $this->render('CLICHESPersonalPlaceBundle:PrivatePlayer:index.html.twig', ['privatePlayers' => $privatePlayers]);
    }

    /**
     * @Route(
     *     "/collections/{id}",
     *     name="cliches_personalplace_privateplayer_view",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $privatePlayer = $em->getRepository('CLICHESPersonalPlaceBundle:PrivatePlayer')->findOneBy(['id' => $id, 'createUser' => $user]);
        if($privatePlayer === null) {throw $this->createNotFoundException('Cet identifiant ('.$id.') n\'est pas défini');}

        return $this->render('CLICHESPersonalPlaceBundle:PrivatePlayer:view.html.twig', ['privatePlayer' => $privatePlayer]);
    }

    /**
     * @Route(
     *     "/collections/nouveau",
     *     name="cliches_personalplace_privateplayer_register"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $privatePlayer = new PrivatePlayer();
        $privatePlayer->setIpCreateUser($request->getClientIp());
        if($this->getUser() != null) {$privatePlayer->setCreateUser($user);}

        $form = $this->createForm(PrivatePlayerType::class, $privatePlayer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $privatePlayer->setCountPlayer(0);
            $privatePlayer->setPublicId(uniqid().rand(0,1000));
            $em->persist($privatePlayer);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, votre collection a bien été générée.' );
            return $this->redirect($this->generateUrl('cliches_personalplace_privateplayer_view', ['id' => $privatePlayer->getId()]));
        }

        return $this->render('CLICHESPersonalPlaceBundle:PrivatePlayer:Register/register.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route(
     *     "/collections/{id}/edition",
     *     name="cliches_personalplace_privateplayer_edit",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $privatePlayer = $em->getRepository('CLICHESPersonalPlaceBundle:PrivatePlayer')->findOneBy(['id' => $id, 'createUser' => $user]);
        if($privatePlayer === null) {throw $this->createNotFoundException('Cet identifiant ('.$id.') n\'est pas défini');}

        $form = $this->createForm(PrivatePlayerType::class, $privatePlayer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($privatePlayer);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, votre collection a bien été éditée.' );
            return $this->redirect($this->generateUrl('cliches_personalplace_privateplayer_view', ['id' => $privatePlayer->getId()]));
        }

        return $this->render('CLICHESPersonalPlaceBundle:PrivatePlayer:Edit/edit.html.twig', [
            'form' => $form->createView(),
            'privatePlayer' => $privatePlayer
        ]);
    }

    /**
     * @Route(
     *     "/collections/addToPrivatePlayer/{view_id}",
     *     name="cliches_personalplace_privateplayer_addToPrivatePlace",
     *     requirements={
     *          "view_id"="\d+"
     *     }
     * )
     * @param int $view_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addToPrivatePlayerAction($view_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        /** @var View $view */
        $view = $em->getRepository("DATAImageBundle:View")->find($view_id);
        if($view === null) {throw $this->createNotFoundException('Cet identifiant ('.$view_id.') n\'est pas défini');}

        $form = $this->createForm(AddToPrivatePlayerType::class, null, array('user' => $user, 'view' => $view, 'privateplayer_service' => $this->get('cliches_personalplace.private_player')));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var PrivatePlayer $privatePlayer */
            $privatePlayer = $form->get('privatePlayers')->getData();
            if($privatePlayer->getCreateUser() == $user) {
                $privatePlayer->addView($view);
                $em->flush();

                $this->get('session')->getFlashBag()->add('notice', 'Félicitations, votre collection a bien été éditée.' );
                return $this->redirectToRoute('data_public_entity_view', ['id' => $view->getEntity()->getId()]);
            }
            return $this->redirectToRoute('data_public_home_index');
        }

        return $this->render('CLICHESPersonalPlaceBundle:PrivatePlayer:AddToPrivatePlayer/register.html.twig', [
            'form' => $form->createView(),
            'view' => $view,
        ]);
    }

    /**
     * @Route(
     *     "/collections/removeView/{privatePlayer_id}/{view_id}",
     *     name="cliches_personalplace_privateplayer_removeview",
     *     requirements={
     *          "view_id"="\d+",
     *          "privatePlayer_id"="\d+"
     *     }
     * )
     * @param int $privatePlayer_id
     * @param int $view_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeViewAction($privatePlayer_id, $view_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        /** @var View $view */
        $view = $em->getRepository("DATAImageBundle:View")->find($view_id);
        if($view === null) {throw $this->createNotFoundException('Cet identifiant ('.$view_id.') n\'est pas défini');}

        /** @var PrivatePlayer $privatePlayer */
        $privatePlayer = $em->getRepository("CLICHESPersonalPlaceBundle:PrivatePlayer")->find($privatePlayer_id);
        if($privatePlayer === null) {throw $this->createNotFoundException('Cet identifiant ('.$privatePlayer_id.') n\'est pas défini');}

        if($privatePlayer->getCreateUser() == $user) {
            $privatePlayer->removeView($view);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Félicitations, votre collection a bien été éditée.' );
            return $this->redirect($request->headers->get('referer'));
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route(
     *     "/collections/remove/{id}",
     *     name="cliches_personalplace_privateplayer_remove",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        /** @var PrivatePlayer $privatePlayer */
        $privatePlayer = $em->getRepository("CLICHESPersonalPlaceBundle:PrivatePlayer")->find($id);
        if($privatePlayer === null) {throw $this->createNotFoundException('Cet identifiant ('.$id.') n\'est pas défini');}

        if($privatePlayer->getCreateUser() == $user) {
            $this->get('cliches_personalplace.private_player')->remove($privatePlayer);

            $this->get('session')->getFlashBag()->add('notice', 'Votre collection a bien été supprimée.');
            return $this->redirectToRoute('cliches_personalplace_privateplayer_index');
        }

        return $this->redirectToRoute('cliches_personalplace_privateplayer_view', ['id' => $id]);
    }
}

<?php

namespace CLICHES\PersonalPlaceBundle\Controller;

use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use CLICHES\PlayerBundle\Entity\PlayerSession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StatsController extends Controller
{
    /**
     * @Route(
     *     "/statistiques",
     *     name="cliches_personalplace_stats_stats"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function statsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryPlayerSession = $em->getRepository('CLICHESPlayerBundle:PlayerSession');
        $repositoryPlayerOeuvre = $em->getRepository('CLICHESPlayerBundle:PlayerOeuvre');
        $repositoryPlayerProposal = $em->getRepository('CLICHESPlayerBundle:PlayerProposal');

        $playerSessions = $repositoryPlayerSession->findBy(array('createUser' => $this->getUser()), array('createDate' => 'DESC'));
        $endPlayerSession = array();
        /** @var PlayerSession $playerSession */
        foreach($playerSessions as $playerSession) {
            $playerOeuvres = $repositoryPlayerOeuvre->findBy(['playerSession' => $playerSession]);

            $playerOeuvresCollection = array();
            /**
             * @var int $key
             * @var PlayerOeuvre $playerOeuvre
             */
            foreach($playerOeuvres as $key => $playerOeuvre) {
                $playerProposal = $repositoryPlayerProposal->findOneBy(['playerOeuvre' => $playerOeuvre]);

                $servicePlayerResult = $this->container->get('cliches_player.player_result');

                if($playerProposal != null AND $servicePlayerResult->testFilled($playerProposal) == true) {
                    $playerOeuvresCollection[$key] = ['playerOeuvre' => $playerOeuvre,
                        'playerProposal' => $servicePlayerResult->foundResults($playerProposal->getId(), false)];
                } else {
                    $playerOeuvresCollection[$key] = ['playerOeuvre' => $playerOeuvre,
                        'playerProposal' => null];
                }
            }

            $endPlayerSession[] =  ['playerSession' => $playerSession,
                                    'playerOeuvres' => $playerOeuvresCollection
                                    ];
        }

        $paginator  = $this->get('knp_paginator');
        $paginatorPlayerSessions = $paginator->paginate(
            $endPlayerSession,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('CLICHESPersonalPlaceBundle:Stats:stats.html.twig', ['playerSessions' => $paginatorPlayerSessions]);
    }
}

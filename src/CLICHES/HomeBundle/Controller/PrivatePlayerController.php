<?php

namespace CLICHES\HomeBundle\Controller;

use CLICHES\PersonalPlaceBundle\Entity\PrivatePlayer;
use CLICHES\PersonalPlaceBundle\Form\AddToPrivatePlayerType;
use CLICHES\PersonalPlaceBundle\Form\PrivatePlayerType;
use DATA\ImageBundle\Entity\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PrivatePlayerController extends Controller
{

    /**
     * @Route(
     *     "/collections/{publicId}",
     *     name="cliches_home_privateplayer_view",
     *     requirements={
     *          "id"="\d+"
     *     }
     * )
     * @param string $publicId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($publicId)
    {
        $em = $this->getDoctrine()->getManager();
        $privatePlayer = $em->getRepository('CLICHESPersonalPlaceBundle:PrivatePlayer')->findOneBy(['publicId' => $publicId]);
        if($privatePlayer === null) {throw $this->createNotFoundException('Cet identifiant ('.$publicId.') n\'est pas défini');}

        return $this->render('CLICHESHomeBundle:PrivatePlayer:view.html.twig', ['privatePlayer' => $privatePlayer]);
    }
}

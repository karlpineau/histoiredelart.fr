<?php

namespace CLICHES\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CLICHES\HomeBundle\Form\HomeLoadType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route(
     *     "/",
     *     name="cliches_home_home_index",
     *     options={
     *          "sitemap"={
     *              "priority"=1,
     *              "changefreq"="monthly"
     *          }
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        if(isset($_GET['sid']) AND !empty($_GET['sid'])) {
            return $this->render('CLICHESHomeBundle:Home:index.html.twig', ['sid' => $_GET['sid']]);
        } else {
            return $this->render('CLICHESHomeBundle:Home:index.html.twig');
        }
        
    }

    /**
     * @Route(
     *     "/accueil/partie",
     *     name="cliches_home_home_load"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loadAction(Request $request)
    {
        //avant ce service était dans le form pour une raison inconnue : $this->container->get('cliches_player.playersessionaction')
        $form = $this->createForm(HomeLoadType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirect($this->generateUrl('cliches_player_session_load', [
                'type' => 'teaching',
                'id' => $form->get('teaching')->getData()->getId(),
                'mode' => "modeField" /*$form->get('mode')->getData()*/
            ]));
        }
        return $this->render('CLICHESHomeBundle:Home:load_content.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route(
     *     "/découvrir",
     *     name="cliches_home_home_discover",
     *     options={
     *          "sitemap"={
     *              "priority"=0.6,
     *              "changefreq"="monthly"
     *          },
     *          "utf8"=true
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function discoverAction()
    {
        return $this->render('CLICHESHomeBundle:Home:discover.html.twig');
    }
}

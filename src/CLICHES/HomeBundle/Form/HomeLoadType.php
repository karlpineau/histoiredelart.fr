<?php

namespace CLICHES\HomeBundle\Form;

use DATA\DataBundle\Service\entity;
use DATA\TeachingBundle\Entity\Teaching;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HomeLoadType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teaching',   EntityType::class,   array(
                                                'class' => 'DATATeachingBundle:Teaching',
                                                'choice_label' => function($teaching) use($options) {
                                                    /** @var Teaching $teaching */
                                                    return $teaching->getName().' ('.$teaching->getViewsNumber().' oeuvres)';
                                                },
                                                'group_by' => function($teaching) {
                                                    /** @var Teaching $teaching */
                                                    return $teaching->getYear().'e année';
                                                },
                                                'query_builder' => function (EntityRepository $er) {
                                                    return $er->createQueryBuilder('t')
                                                              ->where('t.onLine = :true')
                                                              ->setParameters(array('true' => true))
                                                              ->orderBy('t.year', 'ASC')
                                                              ->orderBy('t.name', 'ASC');
                                                },
                                                'required' => true,
                                                'empty_data' => 'Sélectionnez une discipline ...',
                                                'mapped' => false
                                            ))
            /*->add('mode',       ChoiceType::class, array(
                                                'choices'  =>  array('Normal' => 'modeChoice', 'Élevé' => 'modeField'),
                                                'required' => true,
                                                'expanded' => true,
                                                'multiple' => false
                                            ))*/
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cliches_homebundle_home_load';
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }
}

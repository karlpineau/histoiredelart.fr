<?php

namespace CLICHES\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route(
     *     "/accueil",
     *     name="cliches_administration_home_index"
     * )
     * @return Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $sessions = $em->getRepository('CLICHESPlayerBundle:PlayerSession')->findBy([], ['createDate' => 'DESC'], 20);
        
        return $this->render('CLICHESAdministrationBundle:Home:index.html.twig', array(
                'sessions' => $sessions,
        ));
    }
}

<?php

namespace CLICHES\AdministrationBundle\Controller;

use CLICHES\PlayerBundle\Entity\PlayerOeuvre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlayerSessionViewController extends Controller
{
    /**
     * @Route(
     *     "/playersession/vue-globale",
     *     name="cliches_administration_playersession_index"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $repositorySession = $this->getDoctrine()->getManager()->getRepository('CLICHESPlayerBundle:PlayerSession');
        $sessions = $repositorySession->findBy([], ['dateBegin' => 'DESC']);

        $paginator  = $this->get('knp_paginator');
        $listSessions = $paginator->paginate(
            $sessions,
            $request->query->get('page', 1)/*page number*/,
            300/*limit per page*/
        );
        
        return $this->render('CLICHESAdministrationBundle:PlayerSessionView:index.html.twig', array(
                'sessions' => $listSessions
        ));
    }

    /**
     * @Route(
     *     "/playersession/vue/{session_id}",
     *     name="cliches_administration_playersession_view",
     *     requirements={
     *          "session_id"="\d+"
     *     }
     * )
     * @param $session_id int
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($session_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repositorySession = $em->getRepository('CLICHESPlayerBundle:PlayerSession');
        $session = $repositorySession->findOneBy(['id' => $session_id]);

        if ($session === null) { throw $this->createNotFoundException('Session : [id='.$session_id.'] inexistante.'); }

        $repositoryPlayerOeuvre = $em->getRepository('CLICHESPlayerBundle:PlayerOeuvre');
        $playerOeuvres = $repositoryPlayerOeuvre->findBy(['playerSession' => $session]);

        $playerOeuvresCollection = array();
        foreach($playerOeuvres as $key => $playerOeuvre) {
            /** @var PlayerOeuvre $playerOeuvre */
            $repositoryPlayerProposal = $em->getRepository('CLICHESPlayerBundle:PlayerProposal');
            $playerProposal = $repositoryPlayerProposal->findOneBy(['playerOeuvre' => $playerOeuvre]);

            if($playerOeuvre->getPlayerSession()->getProposalType() == 'modeField') {
                $servicePlayerResult = $this->container->get('cliches_player.player_result');
                if ($playerProposal != null AND $servicePlayerResult->testFilled($playerProposal) == true) {
                    $playerOeuvresCollection[$key] = ['playerOeuvre' => $playerOeuvre,
                        'playerProposal' => $servicePlayerResult->foundResults($playerProposal->getId(), false)];
                } else {
                    $playerOeuvresCollection[$key] = ['playerOeuvre' => $playerOeuvre,
                        'playerProposal' => null];
                }
            } elseif($playerOeuvre->getPlayerSession()->getProposalType() == 'modeChoice') {
                $playerOeuvresCollection[$key] = ['playerOeuvre' => $playerOeuvre,
                    'playerProposal' => $playerProposal];
            } elseif($playerOeuvre->getPlayerSession()->getProposalType() == 'modeTest') {
                $playerOeuvresCollection[$key] = ['playerOeuvre' => $playerOeuvre,
                    'playerProposal' => $playerProposal];
            }
        }
        
        return $this->render('CLICHESAdministrationBundle:PlayerSessionView:view.html.twig', array(
            'session' => $session,
            'playerOeuvres' => $playerOeuvresCollection
        ));
    }
}

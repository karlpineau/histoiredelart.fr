<?php

namespace CLICHES\AdministrationBundle\Controller;

use DATA\ImageBundle\Entity\View;
use DATA\TeachingBundle\Entity\Teaching;
use DATA\TeachingBundle\Entity\TeachingTest;
use DATA\TeachingBundle\Entity\TeachingTestVote;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VotesController extends Controller
{
    /**
     * @Route(
     *     "/votes/statistiques",
     *     name="cliches_administration_votes_statistics"
     * )
     * @return Response
     */
    public function statisticsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repositoryTeachingTestVote = $em->getRepository('DATATeachingBundle:TeachingTestVote');
        $teachingTestVotes = $repositoryTeachingTestVote->findBy(array(), array('createDate' => 'DESC'), 20);
        $countTeachingTestVotes = count($repositoryTeachingTestVote->findAll());
        $countTeachingTestVotesYes = count($repositoryTeachingTestVote->findBy(['vote' => true]));
        $countTeachingTestVotesNo = count($repositoryTeachingTestVote->findBy(['vote' => false]));

        /* -- Calcul du TOP20 -- */
        $arrayCountVotesPerTeachingTest = array();
        /** @var TeachingTest $teachingTest */
        foreach($em->getRepository('DATATeachingBundle:TeachingTest')->findAll() as $teachingTest) {
            $arrayCountVotesPerTeachingTest[$teachingTest->getId()] = count($repositoryTeachingTestVote->findBy(['teachingTest' => $teachingTest]));
        }
        arsort($arrayCountVotesPerTeachingTest);

        /* -- Calcul du classement par teaching -- */
        $arrayTeachingRate = array();
        /** @var Teaching $teaching */
        foreach($em->getRepository('DATATeachingBundle:Teaching')->findAll() as $teaching) {
            $numberVotes = 0;
            foreach($em->getRepository('DATATeachingBundle:TeachingTest')->findBy(['teaching' => $teaching]) as $teachingTest) {
                $numberVotes += count($em->getRepository('DATATeachingBundle:TeachingTestVote')->findBy(['teachingTest' => $teachingTest]));
            }
            $arrayTeachingRate[] = ['teaching' => $teaching, 'numberVote' => $numberVotes];
        }

        /* -- Calcul moyenne du nombre de vote / vues -- */
        $averageVoteView = count($em->getRepository('DATATeachingBundle:TeachingTestVote')->findAll())/count($em->getRepository('DATAImageBundle:View')->findAll());
        /* -- Calcul moyenne du nombre de vote / session Clichés -- */
        $averageVoteSession = count($em->getRepository('DATATeachingBundle:TeachingTestVote')->findAll())/count($em->getRepository('CLICHESPlayerBundle:PlayerSession')->findAll());


        return $this->render('CLICHESAdministrationBundle:Votes:statistics.html.twig', array(
            'teachingTestVotes' => $teachingTestVotes,
            'countTeachingTestVotes' => $countTeachingTestVotes,
            'top20' => $arrayCountVotesPerTeachingTest,
            'arrayTeachingRate' => $arrayTeachingRate,
            'countTeachingTestVotesYes' => $countTeachingTestVotesYes,
            'countTeachingTestVotesNo' => $countTeachingTestVotesNo,
            'averageVoteView' => $averageVoteView,
            'averageVoteSession' => $averageVoteSession
        ));
    }

    /**
     * @Route(
     *     "/votes/stats-xml-intime",
     *     name="cliches_administration_votes_statistics_intime"
     * )
     * @param Request $request
     * @return Response
     */
    public function getStatistiquesInTimeAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $repositorySession = $em->getRepository('DATATeachingBundle:TeachingTestVote');
            $nowBegin = date("Y-m-d 00:00:00");
            $nowEnd = date("Y-m-d 23:59:59");

            $return = array();
            for($i = 0 ; $i <= 15 ; $i++)
            {
                //$nowBegin->sub(new \DateInterval('P'.$i.'D'));
                //$nowEnd->sub(new \DateInterval('P'.$i.'D'));
                $begin  = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m")  , date("d")-$i, date("Y")));
                $end  = date("Y-m-d H:i:s", mktime(24, 59, 59, date("m")  , date("d")-$i, date("Y")));

                $players = $repositorySession->createQueryBuilder('p')
                    ->where('p.createDate BETWEEN :dayB AND :dayE')
                    ->setParameter('dayB', $begin)
                    ->setParameter('dayE', $end)
                    ->getQuery()->getResult();

                array_unshift($return, count($players));
            }

            return new Response(json_encode($return));
        }
    }
}

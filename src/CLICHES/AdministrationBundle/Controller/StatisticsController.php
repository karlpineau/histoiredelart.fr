<?php

namespace CLICHES\AdministrationBundle\Controller;

use CLICHES\PlayerBundle\Service\privatePlayerService;
use DATA\TeachingBundle\Entity\Teaching;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StatisticsController extends Controller
{
    /**
     * @Route(
     *     "/statistiques",
     *     name="cliches_administration_statistics_index"
     * )
     * @return Response
     */
    public function indexAction()
    {
        set_time_limit(0);
        $em = $this->getDoctrine()->getManager();
        $repositorySession = $em->getRepository('CLICHESPlayerBundle:PlayerSession');
        $nbSessions = count($repositorySession->findAll());
        $nbSimpleSessions = count($repositorySession->findBy(['simpleSession' => true]));

        /* -- Calcul du nombre de sessions pour aujourd'hui -- */
        $beginToday  = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
        $endToday  = date("Y-m-d H:i:s", mktime(24, 59, 59, date("m")  , date("d"), date("Y")));
        $playersToday = count($repositorySession->createQueryBuilder('p')
                ->where('p.createDate BETWEEN :dayB AND :dayE')
                ->setParameter('dayB', $beginToday)
                ->setParameter('dayE', $endToday)
                ->getQuery()->getResult());

        /* -- Calcul du nombre de sessions pour hier -- */
        $beginYesterday  = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
        $endYesterday = date("Y-m-d H:i:s", mktime(24, 59, 59, date("m")  , date("d")-1, date("Y")));
        $playersYesterday = count($repositorySession->createQueryBuilder('p')
            ->where('p.createDate BETWEEN :dayB AND :dayE')
            ->setParameter('dayB', $beginYesterday)
            ->setParameter('dayE', $endYesterday)
            ->getQuery()->getResult());

        return $this->render('CLICHESAdministrationBundle:Statistics:index.html.twig', array(
            'nbSessions' => $nbSessions,
            'nbSimpleSessions' => $nbSimpleSessions,
            'playersToday' => $playersToday,
            'playersYesterday' => $playersYesterday
        ));
    }

    /**
     * @Route(
     *     "/statistiques/enseignement",
     *     name="cliches_administration_statistics_statisticsbyteaching"
     * )
     * @return Response
     */
    public function statisticsByTeachingAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repositorySession = $em->getRepository('CLICHESPlayerBundle:PlayerSession');
        /** @var privatePlayerService $sessionService */
        $sessionService = $this->container->get('cliches_player.player_session');
        $repositoryTeachings = $em->getRepository('DATATeachingBundle:Teaching');
        /** @var Teaching[] $teachings */
        $teachings = $repositoryTeachings->findAll();

        $arrayTeachings = array();
        foreach($teachings as $teaching) {
            $nbSessionTeaching = count($repositorySession->findBy(['teaching' => $teaching]));
            $moyenneTempsTeaching = $sessionService->getAverageTime($teaching);
            $nbNonUserTeaching = $sessionService->getNumberNonUser($teaching);
            $averageSessionUserTeaching = $sessionService->getAverageUser($teaching);

            $arrayTeachings[] =
                ['name' => $teaching,
                    'sessions' => $nbSessionTeaching,
                    'moyenneTemps' => $moyenneTempsTeaching,
                    'nbNonUser' => $nbNonUserTeaching,
                    'averageSessionUser' => $averageSessionUserTeaching
                ];
        }

        return $this->render('CLICHESAdministrationBundle:Statistics:statisticsByTeaching.html.twig', array(
            'teachings' => $arrayTeachings
        ));
    }

    /**
     * @Route(
     *     "/statistiques/stats-xml-intime",
     *     name="cliches_administration_statistics_getstatistiques_intime",
     *     options={
     *          "expose"=true
     *     }
     * )
     * @param Request $request
     * @return Response
     */
    public function getStatistiquesInTimeAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $repositorySession = $em->getRepository('CLICHESPlayerBundle:PlayerSession');

            $return = array();
            for($i = 0 ; $i <= 30 ; $i++)
            {
                $begin  = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m")  , date("d")-$i, date("Y")));
                $end  = date("Y-m-d H:i:s", mktime(24, 59, 59, date("m")  , date("d")-$i, date("Y")));

                $players = $repositorySession->createQueryBuilder('p')
                    ->where('p.createDate BETWEEN :dayB AND :dayE')
                    ->setParameter('dayB', $begin)
                    ->setParameter('dayE', $end)
                    ->getQuery()->getResult();

                array_unshift($return, count($players));
            }

            return new Response(json_encode($return));
        }
    }

    /**
     * @Route(
     *     "/statistiques/stats-xml-nbcliches",
     *     name="cliches_administration_statistics_getstatistiques_nbcliches",
     *     options={
     *          "expose"=true
     *     }
     * )
     * @param Request $request
     * @return Response
     */
    public function getStatistiquesNbClichesAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $repositoryPlayerSession = $em->getRepository('CLICHESPlayerBundle:PlayerSession');
            $repositoryPlayerOeuvre = $em->getRepository('CLICHESPlayerBundle:PlayerOeuvre');
            $arrayTotal = array();

            foreach($repositoryPlayerSession->findAll() as $playerSession) {
                $playerOeuvres = $repositoryPlayerOeuvre->findBy(['playerSession' => $playerSession]);

                if(array_key_exists(count($playerOeuvres), $arrayTotal)) {
                    $arrayTotal[count($playerOeuvres)] = $arrayTotal[count($playerOeuvres)]+1;
                } else {
                    $arrayTotal[count($playerOeuvres)] = 1;
                }
            }
            ksort($arrayTotal);

            $arrayValue = array();
            $arrayNb = array();
            foreach($arrayTotal as $key => $nb) {
                $arrayNb[] = $key;
                $arrayValue[] = $nb;
            }

            return new Response(json_encode([$arrayNb, $arrayValue]));
        }
    }
}

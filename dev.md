histoiredelart.fr
==============

A Symfony project created on January 21, 2019, 9:07 am.

> Pour la gestion de Webpack Encore https://symfony.com/doc/3.4/frontend/encore/simple-example.html

    yarn encore production

> Pour Composer : -d memory_limit=-1


## Modèle de données


###Données Wikidata :

    {
        property: wd-P170
        value : 
        {
            "values": [
                {
                    "mainsnak": {
                        "value": "Q296955", 
                        "labels": {
                            "fr": {"language": "fr", "value": "peinture \u00e0 l'huile"}, 
                            "en": {"language": "en", "value": "oil paint"}}
                        }, 
                    "qualifiers": null
                }, 
                {
                    "mainsnak": {
                        "value": "Q4259259", 
                        "labels": {
                            "fr": {"language": "fr", "value": "toile"}, 
                            "en": {"language": "en", "value": "canvas"}
                        }
                    }, 
                    "qualifiers": {
                        "P518": [
                            {"hash": "741c2f726bfd42522b6ee31f850ec9ff7d4b7c41", "snaktype": "value", "property": "P518", "datatype": "wikibase-item", "datavalue": {"type": "wikibase-entityid", "value": {"id": "Q861259", "entity-type": "item", "numeric-id": 861259}}}
                        ]
                    }
                }
            ], 
            "labels": {
                "fr": {"language": "fr", "value": "mat\u00e9riau"}, 
                "en": {"language": "en", "value": "material used"}
            }
        }
    }
    
### Données internes alignées :
Ca veut dire que la valeur est une donnée en provenance de WIKIDATA
-> Peut être qu'il faut simplement importer de Wikidata le schéma validé ci-dessus dans ce cas
    
    {
        property: hda-P170
        value : Q180932
    }
    
### Données internes spécifiques :

    {
        property: hda-P170
        value: 
        {
            "values": [
                {
                    "mainsnak": {
                        "language": "fr", 
                        "value": "peinture \u00e0 l'huile"
                    }
                }, 
                {
                    "mainsnak": {
                        "language": "fr",
                        "value": "toile"
                    }
                }
            ], 
            "labels": {
                "fr": {"language": "fr", "value": "mat\u00e9riau"}
            }
        }
    }
var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/public/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .addEntry('app', './assets/js/app.js')
    .addEntry('prototype', './assets/js/prototype.js')
    .addEntry('js_cliches_administration_home', './assets/js/CLICHESAdministrationBundle/home.js')
    .addEntry('js_cliches_player_proposal', './assets/js/CLICHESPlayerBundle/proposal.js')
    .addEntry('js_cliches_player_proposalChoice', './assets/js/CLICHESPlayerBundle/proposalChoice.js')
    .addEntry('js_cliches_player_proposalField', './assets/js/CLICHESPlayerBundle/proposalField.js')
    .addEntry('js_cliches_player_result', './assets/js/CLICHESPlayerBundle/result.js')
    .addEntry('js_cliches_player_resultChoice', './assets/js/CLICHESPlayerBundle/resultChoice.js')
    .addEntry('js_cliches_player_resultField', './assets/js/CLICHESPlayerBundle/resultField.js')
    .addEntry('js_cliches_player_visualisation', './assets/js/CLICHESPlayerBundle/visualisation.js')
    .addEntry('js_data_administration_visualization', './assets/js/DATAAdministrationBundle/visualization.js')
    .addEntry('js_data_administration_testdate', './assets/js/DATAAdministrationBundle/testDate.js')
    .addEntry('js_data_data_editBuildingViews', './assets/js/DATADataBundle/editBuildingViews.js')
    .addEntry('js_data_data_editEntity', './assets/js/DATADataBundle/editEntity.js')
    .addEntry('js_data_data_editEntitySameAs', './assets/js/DATADataBundle/editEntitySameAs.js')
    .addEntry('js_data_data_editEntitySources', './assets/js/DATADataBundle/editEntitySources.js')
    .addEntry('js_data_data_editEntityViews', './assets/js/DATADataBundle/editEntityViews.js')
    .addEntry('js_data_data_editViews', './assets/js/DATADataBundle/editViews.js')
    .addEntry('js_data_data_view', './assets/js/DATADataBundle/view.js')
    .addEntry('js_data_import_import', './assets/js/DATAImportBundle/import.js')
    .addEntry('js_data_public_public', './assets/js/DATAPublicBundle/public.js')
    .addEntry('js_tb_personalPlace_addItems', './assets/js/TBPersonalPlaceBundle/addItems.js')
    .addEntry('js_tb_personalPlace_create', './assets/js/TBPersonalPlaceBundle/create.js')
    .addEntry('js_tb_personalPlace_editIcon', './assets/js/TBPersonalPlaceBundle/editIcon.js')
    .addEntry('js_tb_administration_addItems', './assets/js/TBAdministrationBundle/addItems.js')
    .addEntry('js_tb_administration_create', './assets/js/TBAdministrationBundle/create.js')
    .addEntry('js_tb_administration_editIcon', './assets/js/TBAdministrationBundle/editIcon.js')
    .addEntry('js_tb_player_index', './assets/js/TBPlayerBundle/index.js')
    .addEntry('js_tb_testedGame_addItems', './assets/js/TBTestedGameBundle/addItems.js')
    .addEntry('js_tb_testedGame_create', './assets/js/TBTestedGameBundle/create.js')
    .addEntry('js_tb_testedGame_editIcon', './assets/js/TBTestedGameBundle/editIcon.js')

    .addStyleEntry('css_global', './assets/css/global.scss')
    .addStyleEntry('css_main', './assets/css/main.css')
    .addStyleEntry('css_signin', './assets/css/signin.css')
    .addStyleEntry('css_fullHeightBlock', './assets/css/fullHeightBlock.css')
    .addStyleEntry('css_stickyFooter', './assets/css/stickyFooter.css')
    .addStyleEntry('css_tb_layout', './assets/css/TB/layout.css')
    .addStyleEntry('css_tb_player', './assets/css/TB/player.css')
    .addStyleEntry('css_tb_stickyFooterPlayer', './assets/css/TB/stickyFooterPlayer.css')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    .configureBabel(() => {}, {
        useBuiltIns: 'usage',
        corejs: 3
    })

    // enables Sass/SCSS support
    .enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes()

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()

    // uncomment if you use API Platform Admin (composer req api-admin)
    //.enableReactPreset()
    //.addEntry('admin', './assets/js/admin.js')
;

module.exports = Encore.getWebpackConfig();

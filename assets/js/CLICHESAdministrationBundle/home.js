import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
require('chart.js');

$(document).ready(function() {
    var sessionInTime = $("#sessionsInTime");
    var labels = [];
    function daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }
    var today = new Date();
    var yyyy = today.getFullYear();
    var month = today.getMonth()+1;
    var lastDay = today.getDate()-30;

    for(var i = 0 ; i <= 30 ; i++) {
        var current = lastDay+i;
        if(current > 0) {
            labels.push(current + '/' + month);
        } else {
            labels.push(daysInMonth(month-1, yyyy)-i);
        }
    }

    $.ajax({
        dataType: "json",
        url: Routing.generate('cliches_administration_statistics_getstatistiques_intime'),
        success: function(data){
            var sessionInTimeChart = new Chart(sessionInTime, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [
                        {
                            label: "Sessions du jour",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(75,192,192,0.4)",
                            borderColor: "rgba(75,192,192,1)",
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            pointBorderColor: "rgba(75,192,192,1)",
                            pointBackgroundColor: "#fff",
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: "rgba(75,192,192,1)",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            pointHoverBorderWidth: 2,
                            pointRadius: 1,
                            pointHitRadius: 10,
                            data: data,
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        },
        error: function(error){
            console.log(dump(error));
        }
    });
});


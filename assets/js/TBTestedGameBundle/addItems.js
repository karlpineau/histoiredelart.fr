$(document).ready(function() {
    function addPrototype(container, name, index) {
        $(container).append($(container.attr('data-prototype').replace(/__name__label__/g, name+' n°'+(index)+' :').replace(/__name__/g, index)));
    }

    function deleteTestedItem(index)
    {
        $('#delete_tested_game_add_items_testedItems_'+index).parent().remove();
        $('#tested_game_add_items_testedItems_'+index).parent().parent().remove();
    }

    function addTestedItem(container, index)
    {
        addPrototype(container, 'Image ', index);

        $('#tested_game_add_items_testedItems_'+index).addClass('row');
        $('#tested_game_add_items_testedItems_'+index).parent().addClass('col-md-11').css('padding-left', '0px');
        $('#tested_game_add_items_testedItems_'+index).wrap('<div/>').parent().addClass('card');

        /* LABEL */
        $('#tested_game_add_items_testedItems_'+index+'_itemLabel').parent().addClass('form-group col-md-12');
        $('#tested_game_add_items_testedItems_'+index+'_itemLabel').prev().addClass('control-label col-md-3');
        $('#tested_game_add_items_testedItems_'+index+'_itemLabel').addClass('form-control').wrap('<div></div>');
        $('#tested_game_add_items_testedItems_'+index+'_itemLabel').parent().addClass('col-md-9');

        /* ORDER */
        $('#tested_game_add_items_testedItems_'+index+'_itemOrder').parent().addClass('form-group col-md-12');
        $('#tested_game_add_items_testedItems_'+index+'_itemOrder').prev().addClass('control-label col-md-3');
        $('#tested_game_add_items_testedItems_'+index+'_itemOrder').addClass('form-control').wrap('<div></div>');
        $('#tested_game_add_items_testedItems_'+index+'_itemOrder').parent().addClass('col-md-9');
        $('#tested_game_add_items_testedItems_'+index+'_itemOrder').val(index);

        /* IMAGE */
        $('#tested_game_add_items_testedItems_'+index+'_dataImage').prev().addClass('sr-only');

        /* IMAGE : COPYRIGHT */
        $('#tested_game_add_items_testedItems_'+index+'_dataImage_copyright').parent().addClass('form-group col-md-12');
        $('#tested_game_add_items_testedItems_'+index+'_dataImage_copyright').prev().addClass('control-label col-md-3');
        $('#tested_game_add_items_testedItems_'+index+'_dataImage_copyright').addClass('form-control').wrap('<div></div>');
        $('#tested_game_add_items_testedItems_'+index+'_dataImage_copyright').parent().addClass('col-md-9');
        $('#tested_game_add_items_testedItems_'+index+'_dataImage_copyright').val('Inconnu');

        /* IMAGE : FILE IMAGE */
        //$('#tested_game_add_items_testedItems_'+index+'_dataImage_fileImage').parent().css('display', 'inline-block');
        $('#tested_game_add_items_testedItems_'+index+'_dataImage_fileImage').prev().addClass('sr-only');

        /* IMAGE : FILE IMAGE : FILE */
        $('#tested_game_add_items_testedItems_'+index+'_dataImage_fileImage_imageFile').parent().addClass('form-group col-md-12').css('margin-left', '0px');
        $('#tested_game_add_items_testedItems_'+index+'_dataImage_fileImage_imageFile').prev().addClass('sr-only');
        $('#tested_game_add_items_testedItems_'+index+'_dataImage_fileImage_imageFile_file').prev().addClass('sr-only');
        $('#tested_game_add_items_testedItems_'+index+'_dataImage_fileImage_imageFile_file').fileinput({
            uploadUrl: '/file-upload-batch/2',
            maxFilePreviewSize: 10240,
            language: 'fr',
            showUpload: false,
            allowedFileExtensions: ["jpg", "jpeg", "png", "gif"],
            maxImageWidth: 800,
            maxImageHeight: 800,
            minImageWidth: 300,
            minImageHeight: 300
        });

        $('#tested_game_add_items_testedItems_'+index).parent().parent().parent().append('' +
            '<div class="col-md-1 text-right">' +
            '   <button type="button" class="btn btn-danger" id="delete_tested_game_add_items_testedItems_'+index+'">' +
            '       <span class="fas fa-minus"></span>' +
            '   </button>' +
            '</div>');
        $('#delete_tested_game_add_items_testedItems_'+index).on('click', function() {deleteTestedItem(index);});
    }

    var container_testedItem = $('#tested_game_add_items_testedItems'),
        index = container_testedItem.children().length+1;
    container_testedItem.html('');
    addTestedItem(container_testedItem, index);
    var button = $('#buttonAddTestedItem').parent().html();
    $('#buttonAddTestedItem').remove();
    $('#tested_game_add_items_testedItems').after(button);
    $('#buttonAddTestedItem').addClass('btn-block btn-lg').html('<span class="fas fa-plus"></span> Ajouter une image');
    $('#buttonAddTestedItem').on('click', function() {
        ++index;
        addTestedItem(container_testedItem, index);
    });
});
const routes = require('../../web/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

Object.defineProperty(Object.prototype, "getTime", {
    value: function() {
        let timeS = this.time;
        let time = null;

        // Si date = +1503-00-00T00:00:00Z
        let timeSS = timeS.substr(1, 10);
        if(timeSS.split('-')[0] === "00" || timeSS.split('-')[1] === "00") {
            let arrayTimeSS = timeSS.split('-');
            if(arrayTimeSS[1] === "00") {
                arrayTimeSS[1] = "01";
            }
            if(arrayTimeSS[2] === "00") {
                arrayTimeSS[2] = "01";
            }

            time = new Date(arrayTimeSS.join('-'));
        } else {
            time = new Date(timeS.substr(1, timeS.length - 1));
        }

        let suffix = "";
        if(timeS.substr(0, 1) === "-") {
            suffix = " av. J.-C.";
        }

        let returnedValue = "";
        switch (this.precision) {
            case 0:
                returnedValue = "billion years";
                break;
            case 1:
                returnedValue = "hundred million years";
                break;
            case 2:
                returnedValue = "ten million years";
                break;
            case 3:
                returnedValue = "million years";
                break;
            case 4:
                returnedValue = "hundred thousand years";
                break;
            case 5:
                returnedValue = "ten thousand years";
                break;
            case 6:
                // Millénaire
                returnedValue = (parseInt(time.getFullYear().toString().substr(0, time.getFullYear().toString().length - 3))+1)+"e millénaire"+suffix;
                break;
            case 7:
                // Century
                returnedValue = (parseInt(time.getFullYear().toString().substr(0, time.getFullYear().toString().length - 2))+1)+"e siècle"+suffix;
                break;
            case 8:
                // Decade
                returnedValue = "années "+time.getFullYear().toString().substr(0, time.getFullYear().toString().length - 1)+"0"+suffix;
                break;
            case 9:
                // Year
                returnedValue = time.getFullYear()+suffix;
                break;
            case 10:
                returnedValue = time.getMonth().computeMonth()+" "+time.getFullYear()+suffix;
                break;
            case 11:
                returnedValue = time.getDate()+" "+time.getMonth().computeMonth()+" "+time.getFullYear()+suffix;
                break;
            case 12:
                returnedValue = time.getDate()+" "+time.getMonth().computeMonth()+" "+time.getFullYear()+" "+time.getHours()+suffix;
                break;
            case 13:
                returnedValue = time.getDate()+" "+time.getMonth().computeMonth()+" "+time.getFullYear()+" "+time.getHours()+":"+time.getMinutes()+suffix;
                break;
            case 14:
                returnedValue = time.getDate()+" "+time.getMonth().computeMonth()+" "+time.getFullYear()+" "+time.getHours()+":"+time.getMinutes()+":"+time.getSeconds()+suffix;
                break;
            default:
                returnedValue = time;
        }

        return returnedValue;
    },
    enumerable : false
});

Object.defineProperty(Object.prototype, "getUnit", {
    value: function() {
        switch (this) {
            case "http://www.wikidata.org/entity/Q174789":
                return "millimètre(s)";
            case "http://www.wikidata.org/entity/Q174728":
                return "centimètre(s)";
            case "http://www.wikidata.org/entity/Q11573":
                return "mètre(s)";
            case "http://www.wikidata.org/entity/Q11570":
                return "kilo(s)";
            case "http://www.wikidata.org/entity/Q191118":
                return "tonne(s)";
            default:
                return this;
        }
    },
    enumerable : false
});

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

Number.prototype.computeMonth = function() {
    switch (this) {
        case 0:
            return "janvier";
        case 1:
            return "janvier";
        case 2:
            return "février";
        case 3:
            return "mars";
        case 4:
            return "avril";
        case 5:
            return "mai";
        case 6:
            return "juin";
        case 7:
            return "juillet";
        case 8:
            return "août";
        case 9:
            return "septembre";
        case 10:
            return "octobre";
        case 11:
            return "novembre";
        case 12:
            return "décembre";
        default:
            return "inconnu";
    }
};
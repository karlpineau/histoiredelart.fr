/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */


// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
require('bootstrap');

const routes = require('../../web/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

import '../css/global.scss';

$(function () {
    $('.main-hide-div').hide();

    $('#UserTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $(document).on('click','[class*="main-JS-delete"]', function(){
        if(!confirm('Etes-vous sûr de vouloir effectuer cette action ?')) return false;
    });

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
require('bootstrap-fileinput');

$(document).ready(function() {
    $('#helpModal').on('shown.bs.modal', function () {});

    /* Hide du contenu formulaire à l'initialisation */
    $('#contentEntity_2').hide();
    $('#divPictureDescription').hide();

    function addPrototype(container, name, index) {
        container.append($(container.attr('data-prototype').replace(/__name__label__/g, name + ' :').replace(/__name__/g, index)));
    }

    function generateFields(arrayJson, container_fields) {
        for(var i = 0 ; i < arrayJson.length ; i++) {
            addPrototype(container_fields, arrayJson[i]['label'], i);

            $('#entity_import_fields_'+i).prev().remove();
            $('#entity_import_fields_'+i+' > div:first-child').addClass('row form-group');
            $('#entity_import_fields_'+i+' > div:first-child > label').wrap('<div class="col-sm-3 control-label"></div>');
            $('#entity_import_fields_'+i+' > div:first-child > div > label').text(arrayJson[i]['label']);
            $('#entity_import_fields_'+i+' > div:first-child > input').wrap('<div class="col-sm-9"></div>');
            $('#entity_import_fields_'+i+' > div:first-child > div > input').addClass('form-control');
            if(arrayJson[i]['field'] == 'sujet' || arrayJson[i]['field'] == 'name') {
                $('#entity_import_fields_'+i+' > div:first-child > div > input').attr('required', 'required');
            }

            $('#entity_import_fields_'+i+' > div:last-child').addClass('d-none');
            $('#entity_import_fields_'+i+' > div:last-child > label').remove();
            $('#entity_import_fields_'+i+' > div:last-child > input').val(arrayJson[i]['field']);

        }
    }

    function loadEntityForm() {
        $('button[id^="buttonLoad"]').on('click', function() {
            if($(this).attr('id') == 'buttonLoadArtwork') {var type = "artwork";}
            else if($(this).attr('id') == 'buttonLoadBuilding') {var type = "building";}
            $('#entity_import_type').val(type);

            var container_fields = $('#entity_import_fields');
            container_fields.prev().hide();

            $.ajax({
                dataType: "json",
                url: Routing.generate('data_import_import_entityField', {type: type}),
                success: function(data){
                    generateFields(data, container_fields);
                    $('#contentEntity_1').hide();
                    $('#contentEntity_2').show();
                    $('#ajax-loader').remove();

                    /* -- AFFICHAGE DU FORMULAIRE DES SOURCES -- */
                    var container_source = $('#entity_import_sources');
                        container_source.prev().hide();
                    var index = 0;
                    $('#buttonAddSource').on('click', function() { ++index; addSource(container_source, index, true);});
                    /* -- Fin des sources -- */
                },
                error: function(error){
                    console.log(error);
                }
            });
        });
    }

    function deleteSource(index)
    {
        $('#entity_import_sources_'+index).parent().remove();
    }

    function addSource(container, index, addProto)
    {
        if(addProto === true) {addPrototype(container, 'Source ', index);}

        $('#entity_import_sources_'+index).parent().addClass('col-md-12');
        $('#entity_import_sources_'+index).addClass('row');
        $('#entity_import_sources_'+index).prev().hide();

        $('#entity_import_sources_'+index+'_title').parent().addClass('form-group col-md-6');
        $('#entity_import_sources_'+index+'_title').prev().addClass('control-label col-md-3');
        $('#entity_import_sources_'+index+'_title').addClass('form-control').wrap('<div></div>');
        $('#entity_import_sources_'+index+'_title').parent().addClass('col-md-9');

        $('#entity_import_sources_'+index+'_url').parent().addClass('form-group col-md-6');
        $('#entity_import_sources_'+index+'_url').prev().addClass('control-label col-md-3');
        $('#entity_import_sources_'+index+'_url').addClass('form-control').wrap('<div></div>');
        $('#entity_import_sources_'+index+'_url').parent().addClass('col-md-9');

        $('#entity_import_sources_'+index+'_authorityControl').parent().addClass('d-none'); //addClass('form-group col-md-4')
        $('#entity_import_sources_'+index+'_authorityControl').prev().addClass('control-label col-md-8');
        $('#entity_import_sources_'+index+'_authorityControl').addClass('form-control').wrap('<div></div>');
        $('#entity_import_sources_'+index+'_authorityControl').parent().addClass('col-md-4');


        $('#entity_import_sources_'+index).append('' +
            '<div class="col-md-1 text-right">' +
            '   <button type="button" class="btn btn-danger" id="delete_entity_import_sources_'+index+'">' +
            '       <span class="glyphicon glyphicon-remove"></span>' +
            '   </button>' +
            '</div>');
        $('#delete_entity_import_sources_'+index).on('click', function() {deleteSource(index);});

    }

    $('#entity_import_fields').html('');
    $('#entity_import_sources').html('');
    $('#entity_import_image_fileImage').prev().addClass('sr-only');
    $('#entity_import_image_fileImage_imageFile_file').prev().hide();
    $('#entity_import_image_fileImage_imageFile').prev().hide();
    $("#entity_import_image_fileImage_imageFile_file").fileinput({
        uploadUrl: '/file-upload-batch/2',
        maxFilePreviewSize: 10240,
        language: 'fr',
        showUpload: false
    });

    $('.fileinput-remove').on('click', function() {
        $('#divPictureDescription').hide();
        $('#entity_import_image_view_isPlan').attr('checked', false);
        $('#entity_import_image_view_vue').val('');
        $('#entity_import_image_view_title').val('');
        $('#entity_import_image_view_iconography').val('');
        $('#entity_import_image_view_location').val('');
        $('#entity_import_image_copyright').val('Inconnu');
    });

    $('#entity_import_image_fileImage_imageFile_file').on('change', function() {
        $('#divPictureDescription').show();
    });

    loadEntityForm();

    $('button[id="buttonBackTo1"]').on('click', function() {
        $('#contentEntity_2').hide();
        $('#contentEntity_1').show();
        $('#entity_import_fields').html('');
        $('#entity_import_sources').html('');
        //loadEntityForm();
    });
});
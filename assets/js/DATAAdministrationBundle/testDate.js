const routes = require('../../../web/js/fos_js_routes.json');
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

$(document).ready(function() {
    function viewResults(data) {
        let trueResult = '';
        let propertyContent = JSON.parse(data);
        if(propertyContent.type === "time") {
            for(let valueK in propertyContent.values) {
                let value = propertyContent.values[valueK];
                if(valueK > 0) { trueResult += ", ";}
                if(value.mainsnak !== undefined && value.mainsnak.time !== undefined ) {
                    trueResult += value.mainsnak.getTime();
                } else {
                    trueResult += "(Valeur non définie)";
                }
            }
        }
        return trueResult;
    }

    $('.dateJS').each(function( index ) {
        console.log( index + ": " + $( this ) );
        console.log( index + ": " + $( this ).text());

        $(this).text(viewResults($( this ).text()));
    });
});
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
let ace = require('brace');
require('brace/mode/json');
require('brace/theme/monokai');

$(document).ready(function() {
    function addPrototype(container, name, index) {
        $(container).append($(container.attr('data-prototype').replace(/__name__label__/g, name+' n°'+(index+1)+' :').replace(/__name__/g, index)));
    }

    function deleteEntityProperty(index)
    {
        $('#entity_properties_'+index).parent().remove();
    }

    function addEntityProperty(container, index, addProto)
    {
        if(addProto === true) { addPrototype(container, 'entityProperty ', index); }

        $('#entity_properties_'+index).prev().addClass('d-none');
        $('#entity_properties_'+index).parent().addClass('mt-4');
        $('#entity_properties_'+index).addClass('row');

        $('#entity_properties_'+index+'_property').parent().addClass('form-group col-10 row');
        $('#entity_properties_'+index+'_property').prev().addClass('col-3 text-right');
        $('#entity_properties_'+index+'_property').addClass('form-control col-9');

        $('#entity_properties_'+index).append('' +
            '<div class="col-2 text-right">' +
            '   <button type="button" class="btn btn-danger" id="delete_entity_properties_'+index+'">' +
            '       <i class="fas fa-minus"></i>' +
            '   </button>' +
            '</div>');
        $('#delete_entity_properties_'+index).on('click', function() {deleteEntityProperty(index);});

        $('#entity_properties_'+index+'_value').parent().addClass('d-none');

        $('#entity_properties_'+index).append('' +
            '<div class="form-group col-10 row">' +
            '   <div class="col-3 text-right">Valeur</div>' +
            '   <div class="col-9" style="height: 200px;" id="json-editor-'+index+'"></div>' +
            '</div>');

        let editor = ace.edit('json-editor-'+index);
        editor.getSession().setMode('ace/mode/json');
        editor.setTheme('ace/theme/monokai');

        if($('#entity_properties_'+index+'_value').val() !== undefined && $('#entity_properties_'+index+'_value').val() !== "") {
            editor.setValue($('#entity_properties_'+index+'_value').val());
        } else {
            editor.setValue(
                [
                    '{',
                    '   "values": [{',
                    '       "mainsnak": "<< A REMPLIR >>"',
                    '   }],',
                    '   "labels": {',
                    '       "fr": {"language":"fr", "value":"<< A REMPLIR >>"}',
                    '   },',
                    '   "type": "string"',
                    '}'
                ].join('\n')
            );
        }
        editor.clearSelection();

        editor.getSession().on('change', function() {
            console.log(editor.getValue());
            $('#entity_properties_'+index+'_value').val(editor.getValue());
            console.log($('#entity_properties_'+index+'_value').val());
        });

    }

    let container_entityProperty = $('#entity_properties');
    container_entityProperty.prev().hide();

    let oldContent = container_entityProperty.html(); container_entityProperty.html('');

    container_entityProperty.append('' +
        '<div class="text-right">' +
        '   <button type="button" id="buttonAddEntityProperty" class="btn btn-primary"><i class="fas fa-plus"></i></button>' +
        '</div>');


    container_entityProperty.append(oldContent);

    let indexI = $('div[id^="entity_properties_"]').length;
    for(let i = 0 ; i < indexI ; i++) {
        addEntityProperty(container_entityProperty, i, false);
    }

    if(indexI === 0) {
        addEntityProperty(container_entityProperty, indexI, true);
    }

    $('#buttonAddEntityProperty').on('click', function() {
        ++indexI;
        addEntityProperty(container_entityProperty, indexI, true);
    });

});
$(document).ready(function() {
    function addPrototype(container, name, index) {
        $(container).append($(container.attr('data-prototype').replace(/__name__label__/g, name+' n°'+(index+1)+' :').replace(/__name__/g, index)));
    }

    function deleteSameAs(index)
    {
        $('#entity_same_as_sameAs_'+index).parent().remove();
    }

    function addSameAs(container, index, addProto)
    {
        if(addProto == true) {addPrototype(container, 'SameAs ', index);}

        $('#entity_same_as_sameAs_'+index).addClass('row');

        $('#entity_same_as_sameAs_'+index+'_url').parent().addClass('form-group col-md-11');
        $('#entity_same_as_sameAs_'+index+'_url').prev().addClass('control-label col-md-3');
        $('#entity_same_as_sameAs_'+index+'_url').addClass('form-control').wrap('<div></div>')
        $('#entity_same_as_sameAs_'+index+'_url').parent().addClass('col-md-9');


        $('#entity_same_as_sameAs_'+index).append('' +
            '<div class="col-md-1 text-right">' +
            '   <button type="button" class="btn btn-danger" id="delete_entity_same_as_sameAs_'+index+'">' +
            '       <i class="fas fa-minus"></i>' +
            '   </button>' +
            '</div>');
        $('#delete_entity_same_as_sameAs_'+index).on('click', function() {deleteSameAs(index);});

    }

    var container_sameAs = $('#entity_same_as_sameAs');
        container_sameAs.prev().hide();

    var oldContent = container_sameAs.html(); container_sameAs.html('');

        container_sameAs.append('' +
            '<div class="text-right">' +
            '   <button type="button" id="buttonAddSameAs" class="btn btn-primary"><i class="fas fa-plus"></i></button>' +
            '</div>');
        $('#buttonAddSameAs').on('click', function() {
            ++index;
            addSameAs(container_sameAs, index, true);
        });

        container_sameAs.append(oldContent);

        for(var i = 0 ; i < $('div[id^="entity_same_as_sameAs_"]').length ; i++) {
            addSameAs(container_sameAs, i, false);
        }

        if(i === 'undefined') { var index = 0;} else { var index = i;}

        if(index == 0) {
            addSameAs(container_sameAs, index, true);
        }
});
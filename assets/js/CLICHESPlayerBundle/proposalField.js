import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

$(document).ready(function() {
    function addPrototype(container, name, index) {
        container.append($(container.attr('data-prototype').replace(/__name__label__/g, name + ' :').replace(/__name__/g, index)));
    }

    function generateFields(arrayJson, container_fields) {
        for(var i = 0 ; i < arrayJson.length ; i++) {
            addPrototype(container_fields, arrayJson[i]['label'], i);

            $('#player_proposal_load_field_playerProposalFields_'+i).prev().remove();
            $('#player_proposal_load_field_playerProposalFields_'+i+' > div:first-child').addClass('form-group row mb-0 mb-sm-3');
            $('#player_proposal_load_field_playerProposalFields_'+i+' > div:first-child > label').text(arrayJson[i]['label']).addClass('col-12 col-sm-5 col-form-label text-left text-sm-right');
            $('#player_proposal_load_field_playerProposalFields_'+i+' > div:first-child > input').wrap('<div class="col-12 col-sm-7"></div>');
            $('#player_proposal_load_field_playerProposalFields_'+i+' > div:first-child > div > input').addClass('form-control');

            $('#player_proposal_load_field_playerProposalFields_'+i+' > div:last-child').addClass('d-none');
            $('#player_proposal_load_field_playerProposalFields_'+i+' > div:last-child > label').remove();
            $('#player_proposal_load_field_playerProposalFields_'+i+' > div:last-child > input').val(arrayJson[i]['field']);

        }
    }

    var container_fields = $('#player_proposal_load_field_playerProposalFields');
    container_fields.prev().hide();

    $.ajax({
        dataType: "json",
        url: Routing.generate('cliches_player_proposalfield_getFieldsAjax', {'idImg': $('#inputImageId').val()}),
        success: function(data){
            $('#ajax-loader').remove();
            generateFields(data, container_fields);
        },
        error: function(error){
            console.log(error);
        }
    });
});


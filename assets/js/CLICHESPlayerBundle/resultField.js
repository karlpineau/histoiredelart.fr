const routes = require('../../../web/js/fos_js_routes.json');
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

$(document).ready(function() {
    function viewResults(data) {
        for(let i = 0; i < data.length; i++) {
            let trueResult = '';
            let propertyContent = JSON.parse(data[i]['trueResult']);
            if(propertyContent.type === undefined) {
                // On est dans le cas d'un label
                for(let k in propertyContent) {
                    if(propertyContent[k].language === "fr") {
                        trueResult += propertyContent[k].value;
                    }
                }
            } else if(propertyContent.type === "entity" && propertyContent.values !== undefined) {
                for(let valueK in propertyContent.values) {
                    let value = propertyContent.values[valueK];
                    if(valueK > 0) { trueResult += ", ";}
                    if(value.mainsnak !== undefined) {
                        if(value.mainsnak.labels.fr !== undefined) {
                            trueResult += value.mainsnak.labels.fr.value;
                        } else if(value.mainsnak.labels.en !== undefined) {
                            trueResult += value.mainsnak.labels.en.value;
                        } else {
                            trueResult += "(Valeur non définie)";
                        }
                    }
                }
            } else if(propertyContent.type === "time") {
                for(let valueK in propertyContent.values) {
                    let value = propertyContent.values[valueK];
                    if(valueK > 0) { trueResult += ", ";}
                    if(value.mainsnak !== undefined && value.mainsnak.time !== undefined ) {
                        trueResult += value.mainsnak.getTime();
                    } else {
                        trueResult += "(Valeur non définie)";
                    }
                }
            } else if(propertyContent.type === "quantity") {
                for(let valueK in propertyContent.values) {
                    let value = propertyContent.values[valueK];
                    if(valueK > 0) { trueResult += ", ";}
                    if(value.mainsnak !== undefined && value.mainsnak.amount !== undefined ) {
                        trueResult += value.mainsnak.amount+" "+value.mainsnak.unit.getUnit();
                    } else {
                        trueResult += "(Valeur non définie)";
                    }
                }
            } else if(propertyContent.type === "monolingualtext") {
                for(let valueK in propertyContent.values) {
                    let value = propertyContent.values[valueK];
                    if(valueK > 0) { trueResult += ", ";}
                    if(value.mainsnak !== undefined && value.mainsnak.text !== undefined ) {
                        trueResult += value.mainsnak.text;
                    } else {
                        trueResult += "(Valeur non définie)";
                    }
                }
            } else if(propertyContent.type === "string") {
                for(let valueK in propertyContent.values) {
                    let value = propertyContent.values[valueK];
                    if(valueK > 0) { trueResult += ", ";}
                    if(value.mainsnak !== undefined ) {
                        trueResult += value.mainsnak;
                    } else {
                        trueResult += "(Valeur non définie)";
                    }
                }
            }

            let userValue = '<span class="text-info">Vous n\'avez rien proposé pour ce champ.</span>',
                classComputed = '',
                iconComputed = '<i class="fas fa-minus-circle text-info"></i>';

            if(data[i]['suggestResult'] !== null && data[i]['suggestResult'] !== '') {
                userValue = data[i]['suggestResult'];

                if(userValue === trueResult) {
                    classComputed = 'border-success';
                    iconComputed = '<i class="fas fa-check-circle text-success"></i>';
                } else {
                    classComputed = '';
                    iconComputed = '<i class="fas fa-times-circle text-danger"></i>';
                }
            }

            $('#divForResults').append('\n\
                <div class="card mt-3 '+ classComputed +'">\n\
                    <div class="card-body">\n\
                        <a data-toggle="collapse" href="#collapseCardField-'+i+'" role="button" aria-expanded="false" aria-controls="collapseCardField-'+i+'" title="Cliquez pour déplier" class="no-link">\n\
                            <div class="row">\n\
                                <dl class="col-11 row mb-0">\n\
                                    <dt class="col-12 col-sm-5 text-left text-sm-right"><strong>'+ data[i]['label']+'</strong></dt>\n\
                                    <dd class="col-12 col-sm-7 mb-0">'+ trueResult +'</dd>\n\
                                </dl>\n\
                                <div class="col-1 text-right">'+ iconComputed +'</div>\n\
                            </div>\n\
                        </a>\n\
                        <div class="row collapse" id="collapseCardField-'+i+'">\n\
                            <div class="col-12 d-block d-sm-none"><hr /></div>\n\
                            <dl class="col-11 row mb-0">\n\
                                <dt class="col-12 col-sm-5 text-left text-sm-right font-weight-normal">Votre proposition</dt>\n\
                                <dd class="col-12 col-sm-7 mb-0">'+ userValue +'</dd>\n\
                            </dl>\n\
                            <div id="info-'+i+'" class="col-12 text-muted" style="line-height: 1;"></div>\n\
                        </div>\n\
                    </div>\n\
                </div>'
            );

            if(classComputed !== '') { $('#info-'+i).append('<small>Ce résultat est calculé à partir d\'une simple comparaison des chaînes de caractères. Il est approximatif.</small>');}

        }
    }

    let idPlayerProposal = $('#playerproposal').val();
    $.ajax({
        dataType: "json",
        url: Routing.generate('cliches_player_resultfield_results', {'playerProposal_id': idPlayerProposal}),
        success: function(data){
            $('#ajax-loader').remove();
            viewResults(data);
        },
        error: function(error){
            console.log(error);
        }
    });

    $('[id^="teaching_test_vote_vote_"]').on('click', function() {
        $.ajax({
            dataType: "json",
            url: Routing.generate('data_public_teachingTest_vote', {'teachingTest_id': $('#idTeachingTest').val(), 'vote': $(this).val()}),
            success: function(data){
                console.log(data);

                $('form[name="teaching_test_vote"]').parent().append('' +
                    '<div class="alert alert-success" role="alert">\n' +
                    '  Merci d\'avoir contribué à Clichés!\n' +
                    '</div>');
                $('form[name="teaching_test_vote"]').remove();
            },
            error: function(error){
                console.log(error);
            }
        });
    });

    $('button[id="sendReport"]').on('click', function() {
        console.log("here");
        if($('input[id="reporting_reporting"]').val() !== '') {
            $.ajax({
                dataType: "json",
                url: Routing.generate('data_public_entity_report', {
                    'entity_id': $('#idEntity').val(),
                    'reportingText': $('input[id="reporting_reporting"]').val()
                }),
                success: function (data) {
                    console.log(data);

                    $('form[name="reporting"]').parent().append('' +
                        '<div class="alert alert-success" role="alert">\n' +
                        '  Merci d\'avoir contribué à Clichés!\n' +
                        '</div>');
                    $('form[name="reporting"]').remove();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });
});
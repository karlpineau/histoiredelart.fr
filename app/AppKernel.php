<?php

use Sensio\Bundle\BuzzBundle\SensioBuzzBundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\WebpackEncoreBundle\WebpackEncoreBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Jeremyjumeau\ParsedownBundle\JeremyjumeauParsedownBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new Vich\UploaderBundle\VichUploaderBundle(),
            new Sonata\SeoBundle\SonataSeoBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Symfony\Bundle\MakerBundle\MakerBundle(),
            new Norzechowicz\AceEditorBundle\NorzechowiczAceEditorBundle(),
            new Liip\ThemeBundle\LiipThemeBundle(),
            new API\DataBundle\APIDataBundle(),
            new CAS\AdministrationBundle\CASAdministrationBundle(),
            new CAS\UserBundle\CASUserBundle(),
            new CLICHES\AdministrationBundle\CLICHESAdministrationBundle(),
            new CLICHES\HomeBundle\CLICHESHomeBundle(),
            new CLICHES\PersonalPlaceBundle\CLICHESPersonalPlaceBundle(),
            new CLICHES\PlayerBundle\CLICHESPlayerBundle(),
            new DATA\AdministrationBundle\DATAAdministrationBundle(),
            new DATA\DataBundle\DATADataBundle(),
            new DATA\ImageBundle\DATAImageBundle(),
            new DATA\ImportBundle\DATAImportBundle(),
            new DATA\PersonalPlaceBundle\DATAPersonalPlaceBundle(),
            new DATA\PublicBundle\DATAPublicBundle(),
            new DATA\SearchBundle\DATASearchBundle(),
            new DATA\TeachingBundle\DATATeachingBundle(),
            new HOME\HomeBundle\HOMEHomeBundle(),
            new TB\AdministrationBundle\TBAdministrationBundle(),
            new TB\HomeBundle\TBHomeBundle(),
            new TB\ModelBundle\TBModelBundle(),
            new TB\PersonalPlaceBundle\TBPersonalPlaceBundle(),
            new TB\PlayerBundle\TBPlayerBundle(),
            new TB\TestedGameBundle\TBTestedGameBundle(),
            new DATA\MementoMoriBundle\DATAMementoMoriBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();

            if ('dev' === $this->getEnvironment()) {
                $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
                $bundles[] = new Symfony\Bundle\WebServerBundle\WebServerBundle();
            }
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(function (ContainerBuilder $container) {
            $container->setParameter('container.autowiring.strict_mode', true);
            $container->setParameter('container.dumper.inline_class_loader', true);

            $container->addObjectResource($this);
        });
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}

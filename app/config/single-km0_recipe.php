<?php get_header(); ?>

    <?php
    $args = array(
        'post_status'       =>  'private',
        'post_type'         =>  'km0_param',
        'posts_per_page'    =>  1,
    );
    $get_param = null;
    $get_param = new WP_Query();
    $get_param->query($args);
    if($get_param->have_posts()) : while($get_param->have_posts()) : $get_param->the_post();
        $spe_introduction_text = null;
        $spe_introduction_image = null;

        if(get_field('introduction_text')) {
            $spe_introduction_text = get_field('introduction_text');
        }
        if(get_field('introduction_image') != "") {
            $spe_introduction_image = get_field('introduction_image');
        }
    endwhile;
    endif;
    wp_reset_query();

    function convertToHoursMins($time) {
        if ($time < 1) {
            return "";
        }
        $hours = floor($time / 60);
        $minutes = $time % 60;

        if($minutes < 10) {
            $minutes = '0'.$minutes;
        }

        if($hours == 0) {
            return $minutes." minutes";
        } else {
            return $hours."h".$minutes;
        }
    }

    function schemaConvertToHoursMins($time) {
        if ($time < 1) {
            return "";
        }
        $hours = floor($time / 60);
        $minutes = $time % 60;

        if($minutes < 10) {
            $minutes = '0'.$minutes;
        }

        if($hours == 0) {
            return $minutes."M";
        } else {
            return $hours."H".$minutes."M";
        }
    }
    ?>

    <div class="container-fluid" itemscope itemtype="http://schema.org/Recipe">
        <?php while ( have_posts() ) : the_post(); $keywords = array(); ?>
            <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                <div class="row">
                    <div class="col-md-12 order-2 order-md-1">
                        <h1 class="mt-4" itemprop="name"><?php echo get_the_title(); ?></h1>

                        <div itemprop="description"><?php echo get_field('introduction_text'); ?></div>
                    </div>
                    <div class="col-md-8 offset-md-2 order-1 order-md-2 mt-4 mt-md-3">
                        <img itemprop="image" class="img-fluid" src="<?php echo get_field('contextual-data_image')["url"]; ?>" alt="<?php echo get_field('contextual-data_image')["title"]; ?>" />
                    </div>
                </div>

                <section class="row background-floor p-4 mt-4">
                    <div class="col-md-12">
                        <h2>À propos de cette recette</h2>
                    </div>
                    <div class="col-md-4">
                        <dl class="row">
                            <?php if(get_field('contextual-data_prep-time') != "" or get_field('contextual-data_prep-time-old') != "") { ?>
                            <dt class="col-4 text-right p-3"><i class="far fa-clock fa-2x" title="Temps de préparation"></i></dt>
                            <dd class="col-8 p-3 m-0">
                                <meta itemprop="prepTime" content="PT<?php schemaConvertToHoursMins(get_field('contextual-data_prep-time')); ?>" />
                                <?php
                                    if(get_field('contextual-data_prep-time') != "" && get_field('contextual-data_prep-time') != null) {
                                        echo convertToHoursMins(get_field('contextual-data_prep-time'));
                                    } elseif(get_field('contextual-data_prep-time-old') != "") {
                                        echo get_field('contextual-data_prep-time-old');
                                    }
                                ?>
                            </dd>
                            <?php } ?>

                            <?php if(get_field('contextual-data_waiting-time') != "") { ?>
                                <dt class="col-4 text-right p-3"><i class="fas fa-stopwatch fa-2x" title="Temps intermédiaire"></i></dt>
                                <dd class="col-8 p-3 m-0"><?php echo get_field('contextual-data_waiting-time'); ?></dd>
                            <?php } ?>

                            <?php if(get_field('contextual-data_cook-time') != "" or get_field('contextual-data_cook-time-old') != "") { ?>
                            <dt class="col-4 text-right p-3"><i class="fas fa-burn fa-2x" title="Temps de cuisson"></i></dt>
                            <dd class="col-8 p-3 m-0">
                                <meta itemprop="cookTime" content="PT<?php schemaConvertToHoursMins(get_field('contextual-data_cook-time')); ?>" />
                                <?php
                                    if(get_field('contextual-data_cook-time') != "" && get_field('contextual-data_cook-time') != null) {
                                        echo convertToHoursMins(get_field('contextual-data_cook-time'));
                                    } elseif(get_field('contextual-data_cook-time-old') != "") {
                                        echo get_field('contextual-data_cook-time-old');
                                    }
                                ?>
                            </dd>
                            <?php } ?>
                        </dl>
                    </div>
                    <div class="col-md-4">
                        <dl class="row">
                            <?php if(get_field('contextual-data_month') != null or get_field('contextual-data_season') != null) { ?>
                            <dt class="col-4 text-right p-3"><i class="fas fa-calendar-alt fa-2x" title="Saisons des produits"></i></dt>
                            <dd class="col-8 p-3 m-0"><?php 
                                foreach (get_field('contextual-data_month') as $key => $value) {
                                    $term = get_term($value, "km0_month");
                                    if($key > 0) {echo ", ";}
                                    echo "<a class=\"brown-link\" href=\"/mois/".$term->slug."\">".$term->name."</a>";
                                    $keywords[] = $term->name;
                                }

                                if(get_field('contextual-data_month') != null and get_field('contextual-data_season') != null) {
                                    echo ", ";
                                }

                                foreach (get_field('contextual-data_season') as $key => $value) {
                                    $term = get_term($value, "km0_season");
                                    if($key > 0) {echo ", ";}
                                    echo "<a class=\"brown-link\" href=\"/saison/".$term->slug."\">".$term->name."</a>";
                                    $keywords[] = $term->name;
                                }
                            ?></dd>
                            <?php } ?>

                            <?php if(get_field('contextual-data_category') != null) { ?>
                            <dt class="col-4 text-right p-3"><i class="fas fa-tags fa-2x" title="Catégories de recette"></i></dt>
                            <dd class="col-8 p-3 m-0"><?php 
                                foreach (get_field('contextual-data_category') as $key => $value) {
                                    $term = get_term($value, "km0_category_recipe");
                                    if($key > 0) {echo ", ";}
                                    echo "<a class=\"brown-link\" href=\"/categorie-recette/".$term->slug."\"><span itemprop=\"recipeCategory\">".$term->name."</span></a>";
                                    $keywords[] = $term->name;
                                }
                            ?></dd>
                            <?php } ?>

                            <?php if(get_field('contextual-data_cuisine') != null) { ?>
                            <dt class="col-4 text-right p-3"><i class="fas fa-globe-europe fa-2x" title="Types de cuisine"></i></dt>
                            <dd class="col-8 p-3 m-0"><?php 
                                foreach (get_field('contextual-data_cuisine') as $key => $value) {
                                    $term = get_term($value, "km0_cuisine");
                                    if($key > 0) {echo ", ";}
                                    echo "<a class=\"brown-link\" href=\"/cuisine/".$term->slug."\"><span itemprop=\"recipeCuisine\">".$term->name."</span></a>";
                                    $keywords[] = $term->name;
                                }
                            ?></dd>
                            <?php } ?>
                        </dl>
                    </div>
                    <div class="col-md-4">
                        <dl class="row">
                            <?php if(get_field('contextual-data_diet') != null) { ?>
                            <dt class="col-4 text-right p-3"><i class="fas fa-carrot fa-2x" title="Régimes spécifiques"></i></dt>
                            <dd class="col-8 p-3 m-0"><?php 
                                foreach (get_field('contextual-data_diet') as $key => $value) {
                                    $term = get_term($value, "km0_diet");
                                    if($key > 0) {echo ", ";}
                                    echo "<a class=\"brown-link\" href=\"/regime/".$term->slug."\">".$term->name."</a>";
                                    $keywords[] = $term->name;
                                }
                            ?></dd>
                            <?php } ?>

                            <?php if(get_field('contextual-data_yield') != "") { ?>
                                <dt class="col-4 text-right p-3"><i class="fas fa-users fa-2x" title="Nombre de personne"></i></dt>
                                <dd class="col-8 p-3 m-0" itemprop="recipeYield"><?php echo get_field('contextual-data_yield'); ?></dd>
                            <?php } ?>
                        </dl>
                    </div>                    
                </section>
                <section class="row bg-pattern p-4">
                    <div class="col-md-12">
                        <h2>Liste des ingrédients</h2>
                    </div>
                    <?php
                    $rows = get_field('ingredient-field');
                    if($rows)
                    { 
                        $array_ingredients = array();
                        $array_ingredients["undefined"] = array();
                        foreach($rows as $key => $row) { 
                            //echo "<pre>"; print_r($row); echo "</pre>"; 

                            if($row["ingredient-field_group"] != "") {
                                if (!array_key_exists($row["ingredient-field_group"], $array_ingredients)) {
                                    $array_ingredients[$row["ingredient-field_group"]] = array();
                                }

                                $array_ingredients[$row["ingredient-field_group"]][] = $row;
                            } else {
                                $array_ingredients["undefined"][] = $row;
                            }
                        } 

                        function ingredient_string($ingredientContainer) {
                            $ingredientTerm = get_term($ingredientContainer["ingredient-field_ingredient"], "km0_ingredient");
                            if($ingredientContainer["ingredient-field_plural"] == true) {
                                $ingredientDisplayName = get_field('ingredient_plural', 'km0_ingredient_'.$ingredientContainer["ingredient-field_ingredient"]);
                            } else {
                                $ingredientDisplayName = $ingredientTerm->name;
                            }
                            $string = "";
                            if($ingredientContainer["ingredient-field_position"] == 0) {
                                $string .= $ingredientContainer["ingredient-field_quantity"];

                                if($ingredientContainer["ingredient-field_determinant"] == true) {
                                    if($string != "") {$string .= " ";}
                                    $string .= "de";
                                }
                            }
                            if($string != "") {$string .= " ";}
                            $string .= $ingredientDisplayName;
                            if($ingredientContainer["ingredient-field_position"] == 1) {
                                if($string != "") {$string .= " ";}
                                $string .= $ingredientContainer["ingredient-field_quantity"];
                            }

                            return "<li><a href=\"/ingredient/".$ingredientTerm->slug."\"><span itemprop=\"recipeIngredient\">".$string."</span></a></li>";
                        }

                        if(count($array_ingredients) > 1) {
                            unset($array_ingredients["undefined"]);

                            foreach ($array_ingredients as $name => $group) {
                                echo "<div class=\"col-md-". 12/count($array_ingredients) ."\">";
                                    echo "<strong>".$name."</strong>";
                                    echo "<ul>";
                                    foreach ($group as $key => $ingredientContainer) {
                                        echo ingredient_string($ingredientContainer);
                                    }
                                    echo "</ul>";
                                echo "</div>";  
                            }
                        } else {
                            echo "<div class=\"col-md-12\">";
                                echo "<ul>";
                                foreach ($array_ingredients["undefined"] as $key => $ingredientContainer) {
                                    echo ingredient_string($ingredientContainer);
                                }
                                echo "</ul>";
                            echo "</div>";  
                        }


                    } ?>
                </section>

                <?php
                $rows = get_field('steps_step');
                if($rows)
                { ?>
                <div class="d-none" itemprop="recipeInstructions" itemscope itemtype="http://schema.org/ItemList">
                    <link itemprop="itemListOrder" href="http://schema.org/ItemListOrderDescending" />
                    <?php foreach($rows as $key => $row) { ?>
                        <div itemprop="itemListElement"><?php echo $row['steps_step_content']; ?></div>
                    <?php } ?>
                </div>
                <?php } ?>

                <div class="row">
                    <section class="col-md-12">
                        <?php
                        $rows = get_field('steps_step');
                        if($rows)
                        { ?>
                            <?php foreach($rows as $key => $row) { ?>
                                <article class="row mt-4 mb-4 mt-md-0 mb-md-0">
                                    <div class="col-12 col-md-5 order-2 order-md-1 mb-2 mb-md-0 pl-3 pl-md-4 pt-0 pt-md-4 pb-0 pb-md-4 step-img">
                                        <img class="img-fluid" src="<?php echo $row['steps_step_image']["sizes"]["large"]; ?>" alt="<?php echo get_field('steps_step_image')["title"]; ?>">
                                    </div>
                                    <div class="col-12 col-md-1 order-1 order-md-2 mt-2 mb-2 mt-md-0 mb-md-0 align-self-center step-number-container">
                                        <div class="text-center row step-number-container-second">
                                           <div class="col-md-12 align-self-center text-center montserrat w700"><?php echo $key+1; ?></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 order-3 mt-2 mb-2 mt-md-0 pr-0 pr-mb-4 pt-0 pt-md-4 pb-0 pb-md-4">
                                        <?php echo $row['steps_step_content']; ?>
                                    </div>
                                </article>
                            <?php } ?>
                        <?php } ?>
                    </section>
                </div>

                <section class="row bg-pattern p-4 d-print-none">
                    <aside class="col-12 col-md-6">
                        <?php
                        $conseils = get_field('conseil_conseil');
                        if($conseils)
                        {
                            foreach($conseils as $key => $conseil) { ?>
                            <article class="card card-shadow mb-4">
                                <div class="card-body row">
                                    <div class="col-12">
                                        <h3><?php echo $conseil['conseil_conseil_title']; ?></h3>
                                    </div>
                                    <div class="col-12 text-center">
                                        <?php if($conseil['conseil_conseil_image'] != "") { ?>
                                            <img class="img-fluid" src="<?php echo $conseil['conseil_conseil_image']["sizes"]["large"]; ?>" alt="<?php echo get_field('conseil_conseil_image')["title"]; ?>">
                                        <?php } ?>
                                    </div>
                                    <div class="col-12">
                                        <?php echo $conseil['conseil_conseil_text']; ?>
                                    </div>
                                </div>
                            </article>
                            <?php }
                        } ?>
                        <p>Une recette publiée par <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>" class="brown-link" itemprop="author"><?php echo get_the_author_meta( 'first_name' ); ?></a> le <?php echo get_the_date(); ?><meta itemprop="datePublished" content="<?php echo get_the_date("Y-m-d"); ?>">.</p>
                        <?php echo $spe_introduction_text; ?>
                        <div class="text-center mt-4">
                             <img src="<?php echo $spe_introduction_image["sizes"]["medium"]; ?>" alt="<?php echo $spe_introduction_image["title"]; ?>" class="img-fluid" />
                        </div>
                    </aside>

                    <aside class="col-12 col-md-6 mt-4 mt-md-0">
                        <div class="card card-shadow">
                            <div class="card-body">
                                <h2 class="card-title"><i class="fas fa-paper-plane"></i> La newsletter de Kilomètre-0</h2>
                                <p class="card-text">Vous manquez d'inspiration pour composer vos plats ? Abonnez-vous et recevez chaque jeudi des idées de recettes de saison.</p>
                                <?php echo do_shortcode("[mc4wp_form id=\"758\"]"); ?>
                                <h2 class="card-title mt-4"><i class="fas fa-share-alt"></i> Kilomètre-0 sur les réseaux sociaux</h2>
                                <div class="text-center">
                                    <a class="mr-4 brown-link" href="https://www.facebook.com/blogkilometre0" target="_blank"><i class="fab fa-facebook-square fa-5x"></i></a> 
                                    <a class="ml-4 brown-link" href="pinterest.com/blogkilometre0" target="_blank"><i class="fab fa-pinterest-square fa-5x"></i></a>
                                </div>
                            </div>
                        </div>
                    </aside>
                </section>

                <div class="d-none" itemprop="keywords"><?php echo implode(',', $keywords); ?></div>

                <div class="mt-4 mb-4 d-print-none">
                    <?php if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif; ?>
                </div>
                
                <div class="d-print-none"><?php echo do_shortcode("[related_posts_by_tax posts_per_page=\"40\" format=\"km0\"]"); ?></div>
            </article>
        <?php endwhile; // end of the loop. ?>
    </div>

<?php get_footer(); ?>
imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }
    - { resource: "@APIDataBundle/Resources/config/services.yml" }
    - { resource: "@CASAdministrationBundle/Resources/config/services.yml" }
    - { resource: "@CASUserBundle/Resources/config/services.yml" }
    - { resource: "@CLICHESAdministrationBundle/Resources/config/services.yml" }
    - { resource: "@CLICHESHomeBundle/Resources/config/services.yml" }
    - { resource: "@CLICHESPersonalPlaceBundle/Resources/config/services.yml" }
    - { resource: "@CLICHESPlayerBundle/Resources/config/services.yml" }
    - { resource: "@DATAAdministrationBundle/Resources/config/services.yml" }
    - { resource: "@DATADataBundle/Resources/config/services.yml" }
    - { resource: "@DATAImageBundle/Resources/config/services.yml" }
    - { resource: "@DATAImportBundle/Resources/config/services.yml" }
    - { resource: "@DATAPersonalPlaceBundle/Resources/config/services.yml" }
    - { resource: "@DATAPublicBundle/Resources/config/services.yml" }
    - { resource: "@DATASearchBundle/Resources/config/services.yml" }
    - { resource: "@DATATeachingBundle/Resources/config/services.yml" }
    - { resource: "@HOMEHomeBundle/Resources/config/services.yml" }
    - { resource: "@TBAdministrationBundle/Resources/config/services.yml" }
    - { resource: "@TBHomeBundle/Resources/config/services.yml" }
    - { resource: "@TBModelBundle/Resources/config/services.yml" }
    - { resource: "@TBPersonalPlaceBundle/Resources/config/services.yml" }
    - { resource: "@TBPlayerBundle/Resources/config/services.yml" }
    - { resource: "@TBTestedGameBundle/Resources/config/services.yml" }
    - { resource: "@DATAMementoMoriBundle/Resources/config/services.yml" }

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: fr
    jms_serializer.camel_case_naming_strategy.class: JMS\Serializer\Naming\IdenticalPropertyNamingStrategy

framework:
    #esi: ~
    #translator: { fallbacks: ['%locale%'] }
    secret: '%secret%'
    router:
        resource: '%kernel.project_dir%/app/config/routing.yml'
        strict_requirements: ~
    form: ~
    csrf_protection: ~
    validation: { enable_annotations: true }
    #serializer: { enable_annotations: true }
    default_locale: '%locale%'
    translator: { fallbacks: ['%locale%'] }
    trusted_hosts: ~
    session:
        # https://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id: session.handler.native_file
        save_path: '%kernel.project_dir%/var/sessions/%kernel.environment%'
    fragments: ~
    http_method_override: true
    assets: ~
    php_errors:
        log: true
    templating:
        engines: ['twig']

# Twig Configuration
twig:
    debug: '%kernel.debug%'
    strict_variables: '%kernel.debug%'
    globals:
        project_name: ""
        data_image_view_service: "@data_image.view"
        data_image_image_service: "@data_image.image"
        data_teaching_teaching_service: "@data_teaching.teaching"
        data_teaching_teachingtest_service: "@data_teaching.teaching_test"
        data_data_entity_service: "@data_data.entity"
        data_data_entity_routing_service: "@data_data.entity_routing"
        data_data_wikidata_service: "@data_data.wikidata"
        data_data_property_service: "@data_data.property"
        data_data_qualifiers_service: "@data_data.qualifiers"
        data_administration_statistics_service: "@data_administration.statistics"
        cliches_player_playersession_service: "@cliches_player.player_session"
        cliches_player_playeroeuvre_service: "@cliches_player.player_oeuvre"
        cliches_player_playerproposal_service: "@cliches_player.player_proposal"
        cliches_player_playerproposalfield_service: "@cliches_player.player_proposal_field"
        cliches_player_playerresult_service: "@cliches_player.player_result"
        cliches_player_playerexclude_service: "@cliches_player.player_exclude"
        cliches_personalplace_privateplayer_service: "@cliches_personalplace.private_player"
        tb_model_testeditem: "@tb_model.testeditem"
        tb_model_testeditemresult: "@tb_model.testeditemresult"
        tb_model_testeditemresultsession: "@tb_model.testeditemresultsession"
        tb_model_testedsession: "@tb_model.testedsession"
        cliches_mail: "cliches@histoiredelart.fr"
    form_themes: ['bootstrap_4_layout.html.twig']

# Doctrine Configuration
doctrine:
    dbal:
        driver: pdo_mysql
        host: '%database_host%'
        port: '%database_port%'
        dbname: '%database_name%'
        user: '%database_user%'
        password: '%database_password%'
        charset: UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: '%kernel.project_dir%/var/data/data.sqlite'
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #path: '%database_path%'

    orm:
        auto_generate_proxy_classes: '%kernel.debug%'
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true
        dql:
            string_functions:
                regexp: DoctrineExtensions\Query\Mysql\Regexp

# FOS\UserBundle configuration
fos_user:
    db_driver:              orm
    firewall_name:          main
    user_class:             CAS\UserBundle\Entity\User
    use_listener:           true
    use_username_form_type: false
    registration:
        form:
            type:  CAS\UserBundle\Form\Type\RegistrationFormType
            validation_groups: [CASRegistration]
        confirmation:
            enabled: true
    profile:
        form:
            type:  CAS\UserBundle\Form\Type\ProfileFormType
            validation_groups: [CASProfile]
    from_email:
        address:        cliches@karl-pineau.fr
        sender_name:    Clichés!

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    auth_mode: "%mailer_auth_mode%"
    host:      "%mailer_host%"
    port:      "%mailer_port%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    encryption: "%mailer_encryption%"
    spool: { type: memory }

sensio_framework_extra:
   router:
        annotations: false

stof_doctrine_extensions:
    default_locale: fr_FR
    orm:
        default:
            timestampable: true
            sluggable: true

vich_uploader:
    db_driver: orm
    mappings:
        product_image:
            uri_prefix:         web/uploads/gallery
            upload_destination: %kernel.root_dir%/../web/uploads/gallery
            namer:              vich_uploader.namer_uniqid
            inject_on_load:     false
            delete_on_update:   true
            delete_on_remove:   true

#Sonata SEO :
sonata_seo:
    encoding:         UTF-8
    page:
        metas:
            name:
                keywords:             "École du Louvre, Histoire, Histoire de l'Art, Archéologie, Muséologie"
                description:          "Testez vos connaissances en histoire de l'art et archéologie grâce à Clichés!"
                author:               "Clichés!"
                generator:            "Symfony2 (http://symfony.com/)"

            property:
                # Open Graph information
                # see http://developers.facebook.com/docs/opengraphprotocol/#types or http://ogp.me/
                'og:site_name':       "Clichés!"
                'og:description':     "Testez vos connaissances en histoire de l'art et archéologie grâce à Clichés!"
                'og:type':            "website"
                'og:url':             "https://beta.histoiredelart.fr"
                'og:title':           "Clichés!"
                'og:image':           "https://beta.histoiredelart.fr/web/images/logo-light.png"

                # Twitter Information
                'twitter:card':         "summary"
                'twitter:url':          "https://beta.histoiredelart.fr"
                'twitter:title':        "Clichés!"
                'twitter:description':  "Testez vos connaissances en histoire de l'art et archéologie grâce à Clichés!"
                'twitter:image':        "https://beta.histoiredelart.fr/web/images/logo-light.png"
                'twitter:site':         "@clichesart"
                'twitter:creator':      "@clichesart"


            http-equiv:
                'Content-Type':         text/html; charset=utf-8
                #'X-Ua-Compatible':      IE=EmulateIE7

            charset:
                UTF-8:    ''

        head:
            'xmlns':              http://www.w3.org/1999/xhtml
            'xmlns:og':           http://opengraphprotocol.org/schema/
            #'xmlns:fb':           "http://www.facebook.com/2008/fbml"

knp_paginator:
    page_range: 5                       # number of links showed in the pagination menu (e.g: you have 10 pages, a page_range of 3, on the 5th page you'll see links to page 4, 5, 6)
    default_options:
        page_name: page                 # page query parameter name
        sort_field_name: sort           # sort field query parameter name
        sort_direction_name: direction  # sort direction query parameter name
        distinct: true                  # ensure distinct results, useful when ORM queries are using GROUP BY statements
        filter_field_name: filterField  # filter field query parameter name
        filter_value_name: filterValue  # filter value query parameter name
    template:
        pagination: '@KnpPaginator/Pagination/twitter_bootstrap_v4_pagination.html.twig'     # sliding pagination controls template
        sortable: '@KnpPaginator/Pagination/sortable_link.html.twig' # sort link template
        filtration: '@KnpPaginator/Pagination/filtration.html.twig'  # filters template

webpack_encore:
    output_path: "%kernel.root_dir%/../public/build"

norzechowicz_ace_editor:
    base_path: "vendor/ace" # notice! this is starting from "your_project_root/web"!
    autoinclude: true
    debug: false # sources not minified, based on kernel.debug but it can force it
    noconflict: true # uses ace.require instead of require


fos_js_routing:
    routes_to_expose: [ 'data_public_teachingTest_vote' ]

nelmio_api_doc:
    documentation:
        host: histoiredelart.fr/api
        schemes: [http, https]
        info:
            title: histoiredelart.fr
            description: API documentation
            version: 1.0.0

liip_theme:
    themes: ['standard_theme', 'dark_theme']
    active_theme: 'standard_theme'